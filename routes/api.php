<?php

use Illuminate\Support\Facades\Route;

Route::group(['as' => 'api.',
		 'namespace' => 'Api',
         'prefix' => 'v1'
		],function() {


	Route::get('index', [\App\Http\Controllers\Api\MainController::class, 'index'])->name('index');
    Route::get('merchants/{id}', [\App\Http\Controllers\Api\MerchantController::class, 'show']);
	Route::get('merchants', [\App\Http\Controllers\Api\MerchantController::class, 'index']);
	

	Route::group(['prefix' => 'auth', 'as' => 'auth.', 'namespace' => 'Auth'],function(){
	    Route::post('login', [\App\Http\Controllers\Api\Auth\LoginController::class, 'authenticate'])->name('authenticate');
	});

	Route::group([], function(){
		Route::group(['prefix' => 'receipts','as' => 'transaction.'], function(){
            Route::post('/', [\App\Http\Controllers\Api\TransactionController::class, 'online'])->middleware(['api.exists:own_merchant']);
		    Route::post('pos', [\App\Http\Controllers\Api\TransactionController::class, 'pos'])->middleware(['api.exists:merchant', 'api.exists:teller', 'api.exists:terminal']);
		    Route::post('show', [\App\Http\Controllers\Api\TransactionController::class, 'show'])->middleware(['api.exists:bundle']);
            Route::get('generate/{code?}', [\App\Http\Controllers\Portal\MainController::class, 'generate'])->name('generate');
		});
	});

	Route::group([], function(){
		Route::group(['prefix' => 'generate', 'as' => 'generate.'],function(){
		    Route::post('online', [\App\Http\Controllers\Api\TransactionController::class, 'online'])->middleware(['api.exists:merchant']);
		    Route::post('pos', [\App\Http\Controllers\Api\TransactionController::class, 'pos'])->middleware(['api.exists:merchant', 'api.exists:teller', 'api.exists:terminal']);
		    Route::post('show', [\App\Http\Controllers\Api\TransactionController::class, 'show'])->middleware(['api.exists:bundle']);
		});

		Route::post('terminal', [\App\Http\Controllers\Api\MainController::class, 'terminal'])->name('terminal');
		Route::post('teller', [\App\Http\Controllers\Api\MainController::class, 'teller'])->name('teller');
		Route::post('merchant', [\App\Http\Controllers\Api\MainController::class, 'merchant'])->name('merchant');

		Route::group(['prefix' => 'teller','as' => 'teller.'],function(){
		    Route::post('create', [\App\Http\Controllers\Api\TellerController::class, 'store'])->middleware(['api.exists:merchant_by_id']);
		});
	});
});
