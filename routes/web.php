<?php

use Illuminate\Support\Facades\Route;

Route::group(['as' => 'portal.',
		 'namespace' => 'Portal',
		 'middleware' => ['web']
		],function() {

    
	Route::get('generate/{code?}', [\App\Http\Controllers\Portal\MainController::class, 'generate'])->name('generate');
	Route::get('send-eor/{code?}', [\App\Http\Controllers\Portal\MainController::class, 'send_eor'])->name('send_eor');
	Route::get('/', [\App\Http\Controllers\Portal\MainController::class, 'index'])->name('index');
	
});

Route::group(['prefix' => 'digipep','as' => 'admin.digipep.', 'namespace' => 'System'],function(){
	Route::any('success/{code}', [\App\Http\Controllers\System\DigipepController::class, 'success'])->name('success');
	Route::any('cancel/{code}', [\App\Http\Controllers\System\DigipepController::class, 'cancel'])->name('cancel');
	Route::any('failed/{code}', [\App\Http\Controllers\System\DigipepController::class, 'failed'])->name('failed');
});

Route::group(['as' => 'admin.',
		 'namespace' => 'System',
		 'prefix' => 'admin',
		 'middleware' => ['web']
		],function() {

    
    Route::get('get-division', [\App\Http\Controllers\System\MerchantController::class, 'get_division'])->name('get_division');
    Route::get('get-office', [\App\Http\Controllers\System\OfficeController::class, 'get_office'])->name('get_office');

	Route::group(['middleware' => 'system.guest'], function(){

        Route::get('register', [\App\Http\Controllers\System\AuthController::class, 'register'])->name('register');
        Route::post('register', [\App\Http\Controllers\System\AuthController::class, 'store']);

        Route::get('login', [\App\Http\Controllers\System\AuthController::class, 'admin_login'])->name('login');
        Route::post('login', [\App\Http\Controllers\System\AuthController::class, 'authenticate']);

        Route::get('forgot-password', [\App\Http\Controllers\System\AuthController::class, 'forgot_password'])->name('forgot_password');
        Route::post('forgot-password', [\App\Http\Controllers\System\AuthController::class, 'forgot_pw']);

        Route::get('reset-password/{token?}', [\App\Http\Controllers\System\AuthController::class, 'reset_password'])->name('reset_password');
        Route::post('reset-password/{token?}', [\App\Http\Controllers\System\AuthController::class, 'update_password']);

        Route::get('submit-documents/{code}', [\App\Http\Controllers\System\AuthController::class, 'submit_documents'])->name('submit_documents');
        Route::post('submit-documents/{code}', [\App\Http\Controllers\System\AuthController::class, 'store_documents']);

	});

    Route::get('generate/{code?}', [\App\Http\Controllers\System\MainController::class, 'generate'])->name('generate');
    Route::get('vertical', [\App\Http\Controllers\System\MainController::class, 'vertical'])->name('vertical');
    Route::get('horizontal', [\App\Http\Controllers\System\MainController::class, 'horizontal'])->name('horizontal');

	Route::group(['middleware' => ['system.auth', 'system.prevent-back-history']],function(){
		
        Route::get('/', [\App\Http\Controllers\System\MainController::class, 'index'])->name('index');
        Route::get('/logout', [\App\Http\Controllers\System\AuthController::class, 'logout'])->name('logout');

		Route::group(['as' => 'profile.'],function(){
            Route::get('/edit-password', [\App\Http\Controllers\System\ProfileController::class, 'edit_password'])->name('edit_password');
            Route::post('/edit-password', [\App\Http\Controllers\System\ProfileController::class, 'update_password']);
		});

		Route::group(['prefix' => 'registrants','as' => 'registrant.'],function(){
            Route::get('/', [\App\Http\Controllers\System\RegistrantController::class, 'index'])->name('index');
            Route::get('show/{id?}', [\App\Http\Controllers\System\RegistrantController::class, 'show'])->name('show');
            Route::any('activate/{id?}', [\App\Http\Controllers\System\RegistrantController::class, 'activate'])->name('activate');
            Route::any('decline/{id?}', [\App\Http\Controllers\System\RegistrantController::class, 'decline'])->name('decline');
            Route::get('update-status', [\App\Http\Controllers\System\RegistrantController::class, 'updateStatus'])->name('update_status');
		});

		Route::group(['prefix' => 'accounts','as' => 'account.'],function(){
            Route::get('/', [\App\Http\Controllers\System\AccountController::class, 'index'])->name('index');
            Route::get('show/{id?}', [\App\Http\Controllers\System\AccountController::class, 'show'])->name('show');
            Route::get('change-status/{id?}', [\App\Http\Controllers\System\AccountController::class, 'update_status'])->name('update_status');
		});

		Route::group(['prefix' => 'transactions','as' => 'transaction.'],function(){
            Route::get('/', [\App\Http\Controllers\System\TransactionController::class, 'index'])->name('index');
			Route::get('show/{code?}', [\App\Http\Controllers\System\TransactionController::class, 'show'])->name('show');
			Route::get('download/{code?}', [\App\Http\Controllers\System\TransactionController::class, 'download'])->name('download');
			Route::get('create', [\App\Http\Controllers\System\TransactionController::class, 'create'])->name('create');
			Route::post('create', [\App\Http\Controllers\System\TransactionController::class, 'store']);
		});

		Route::group(['prefix' => 'business','as' => 'business.'],function(){
			Route::get('/', [\App\Http\Controllers\System\BusinessController::class, 'index'])->name('index');
			Route::get('show/{code?}', [\App\Http\Controllers\System\BusinessController::class, 'show'])->name('show');
			Route::get('create', [\App\Http\Controllers\System\BusinessController::class, 'create'])->name('create');
			Route::post('create', [\App\Http\Controllers\System\BusinessController::class, 'store'])->name('store');
			Route::get('change-status/{code?}', [\App\Http\Controllers\System\BusinessController::class, 'update_status'])->name('update_status');
		});

		Route::group(['prefix' => 'ledgers','as' => 'ledger.'],function(){
			Route::get('/', [\App\Http\Controllers\System\LedgerController::class, 'index'])->name('index');
			Route::get('show/{code?}', [\App\Http\Controllers\System\LedgerController::class, 'show'])->name('show');

			Route::get('print/{code?}', [\App\Http\Controllers\System\LedgerController::class, 'print'])->name('print');
			Route::get('create', [\App\Http\Controllers\System\LedgerController::class, 'create'])->name('create');
			Route::post('create', [\App\Http\Controllers\System\LedgerController::class, 'store']);

			Route::get('online/{code?}', [\App\Http\Controllers\System\LedgerController::class, 'online'])->name('online');
			Route::get('booklet/{code?}', [\App\Http\Controllers\System\LedgerController::class, 'booklet'])->name('booklet');

			Route::get('edit/{merchant_code}/{code?}', [\App\Http\Controllers\System\LedgerController::class, 'edit'])->name('edit');
			Route::post('edit/{merchant_code}/{code?}', [\App\Http\Controllers\System\LedgerController::class, 'update'])->name('update');

			Route::get('export', [\App\Http\Controllers\System\LedgerController::class, 'export'])->name('export');
		});

		Route::group(['prefix' => 'offices','as' => 'office.'],function(){
			Route::get('/', [\App\Http\Controllers\System\OfficeController::class, 'index'])->name('index');
			Route::get('create', [\App\Http\Controllers\System\OfficeController::class, 'create'])->name('create');
			Route::post('create', [\App\Http\Controllers\System\OfficeController::class, 'store']);
			Route::get('edit/{id?}', [\App\Http\Controllers\System\OfficeController::class, 'edit'])->name('edit');
			Route::post('edit/{id?}', [\App\Http\Controllers\System\OfficeController::class, 'update']);
			Route::any('delete/{id?}', [\App\Http\Controllers\System\OfficeController::class, 'delete'])->name('delete');
		});

		Route::group(['prefix' => 'tellers','as' => 'teller.'],function(){
			Route::get('/', [\App\Http\Controllers\System\TellerController::class, 'index'])->name('index');
			Route::get('create', [\App\Http\Controllers\System\TellerController::class, 'create'])->name('create');
			Route::post('create', [\App\Http\Controllers\System\TellerController::class, 'store']);
			Route::get('edit/{id?}', [\App\Http\Controllers\System\TellerController::class, 'edit'])->name('edit');
			Route::post('edit/{id?}', [\App\Http\Controllers\System\TellerController::class, 'update']);
			Route::any('delete/{id?}', [\App\Http\Controllers\System\TellerController::class, 'delete'])->name('delete');
		});

		Route::group(['prefix' => 'merchants','as' => 'merchant.'],function(){
			Route::get('/', [\App\Http\Controllers\System\MerchantController::class, 'index'])->name('index');
			Route::get('create', [\App\Http\Controllers\System\MerchantController::class, 'create'])->name('create');
			Route::post('create', [\App\Http\Controllers\System\MerchantController::class, 'store']);
			Route::get('edit/{id?}', [\App\Http\Controllers\System\MerchantController::class, 'edit'])->name('edit');
			Route::post('edit/{id?}', [\App\Http\Controllers\System\MerchantController::class, 'update'])->name('update');
			Route::any('delete/{id?}', [\App\Http\Controllers\System\MerchantController::class, 'delete'])->name('delete');

			Route::get('edit-details/{code?}', [\App\Http\Controllers\System\MerchantController::class, 'edit_details'])->name('edit_details');
			Route::post('edit-details/{code?}', [\App\Http\Controllers\System\MerchantController::class, 'update_details'])->name('update_details');
		});

		Route::group(['prefix' => 'terminals','as' => 'terminal.'],function(){
			Route::get('/', [\App\Http\Controllers\System\TerminalController::class, 'index'])->name('index');
			Route::get('create', [\App\Http\Controllers\System\TerminalController::class, 'create'])->name('create');
			Route::post('create', [\App\Http\Controllers\System\TerminalController::class, 'store'])->name('store');
			Route::get('edit/{id?}', [\App\Http\Controllers\System\TerminalController::class, 'edit'])->name('edit');
			Route::post('edit/{id?}', [\App\Http\Controllers\System\TerminalController::class, 'update'])->name('update');
			Route::any('delete/{id?}', [\App\Http\Controllers\System\TerminalController::class, 'delete'])->name('delete');
		});

		Route::group(['prefix' => 'system-accounts','as' => 'user.'],function(){
			Route::get('/', [\App\Http\Controllers\System\UserController::class, 'index'])->name('index');
			Route::get('create', [\App\Http\Controllers\System\UserController::class, 'create'])->name('create');
			Route::post('create', [\App\Http\Controllers\System\UserController::class, 'store'])->name('store');
			Route::get('edit/{id?}', [\App\Http\Controllers\System\UserController::class, 'edit'])->name('edit');
			Route::post('edit/{id?}', [\App\Http\Controllers\System\UserController::class, 'update'])->name('update');
			Route::any('delete/{id?}', [\App\Http\Controllers\System\UserController::class, 'delete'])->name('delete');
			Route::get('update-status/{id?}', [\App\Http\Controllers\System\UserController::class, 'update_status'])->name('update_status');
		});

		Route::group(['prefix' => 'settings', 'as' => 'setting.'], function () {
			Route::get('/', [\App\Http\Controllers\System\SettingController::class, 'index'])->name('index');
			Route::get('create', [\App\Http\Controllers\System\SettingController::class, 'create'])->name('create');
			Route::post('create', [\App\Http\Controllers\System\SettingController::class, 'store'])->name('store');
		});
	});
});
