# Fix coding standards via https://github.com/squizlabs/PHP_CodeSniffer

Check for coding standards issues
```
phpcs app
```

Fix coding standards automatically
```
phpcbf app
```

# Installation

##