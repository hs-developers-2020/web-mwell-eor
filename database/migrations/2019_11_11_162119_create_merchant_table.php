<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('max_online')->nullable()->default(1000);
            $table->integer('max_booklet')->nullable()->default(5);
            $table->string('section_code')->nullable();
            $table->string('division_code')->nullable();
            $table->bigInteger('owner_user_id')->nullable();
            $table->string('status')->nullable()->default('active');
            $table->string('merchant_code')->nullable();
            $table->string('merchant_name', 255)->nullable();
            $table->string('submerchant_code')->nullable();
            $table->string('submerchant_name', 255)->nullable();
            $table->string('merchant_type')->nullable();
            $table->longText('address')->nullable();
            $table->string('tin')->nullable();
            $table->string('tel_no')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchants');
    }
}
