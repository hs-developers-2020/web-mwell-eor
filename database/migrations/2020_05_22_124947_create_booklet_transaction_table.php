<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookletTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booklet_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->nullable();
            $table->bigInteger('order_id')->nullable();
            $table->bigInteger('batch_id')->nullable();
            $table->string('merchant_code')->nullable();
            $table->string('merchant_name')->nullable();
            $table->string('or_code')->nullable();
            $table->string('series_code')->nullable();
            $table->boolean('status')->nullable()->default(0);
            $table->decimal('amount', 25, 2)->nullable();
            $table->string('tin')->nullable();
            $table->string('name', 255)->nullable();
            $table->date('transaction_date')->nullable();
            $table->date('date_expiry')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booklet_transactions');
    }
}
