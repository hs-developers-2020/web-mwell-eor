<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('partner_code')->nullable();
            $table->string('sku')->nullable();
            $table->string('product_name')->nullable();
            $table->integer('qty')->nullable();
            $table->decimal('price',10,2)->nullable();
            $table->decimal('sale_price',10,2)->nullable();
            $table->decimal('total',10,2)->nullable();
            $table->decimal('discount',10,2)->nullable();
            $table->decimal('gp',10,2)->nullable();
            $table->decimal('margin',5,2)->default("0");
            $table->date('sale_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
