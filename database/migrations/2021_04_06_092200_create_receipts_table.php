<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('bundle_id')->nullable();
            $table->integer('receipt_number');
            $table->integer('merchant_id')->nullable();
            $table->string('payment_reference_code')->nullable();
            $table->string('transaction_code')->nullable();

            $table->string('fname', 255)->nullable();
            $table->string('mname')->nullable();
            $table->string('lname')->nullable();
            $table->string('suffix')->nullable();
            $table->string('email', 255)->nullable();
            $table->longText('address')->nullable();
            $table->string('tin')->nullable();

            $table->decimal('dst_fee',25,2)->nullable();
            $table->decimal('convenience_fee',25,2)->nullable();

            $table->decimal('vat_sales',25,2)->nullable();
            $table->decimal('vat_amount',25,2)->nullable();
            $table->decimal('vat_percentage',25,2)->nullable();

            $table->integer('total_qty')->nullable();
            $table->decimal('sub_total',25,2)->nullable();
            $table->decimal('total',25,2)->nullable();
            $table->datetime('date_expiry')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipts');
    }
}
