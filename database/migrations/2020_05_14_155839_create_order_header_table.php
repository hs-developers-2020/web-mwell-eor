<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderHeaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_headers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->nullable();
            $table->string('merchant_code')->nullable();
            $table->string('reference_code')->nullable();
            $table->integer('eor_online_qty')->nullable();
            $table->integer('eor_booklet_qty')->nullable();
            $table->decimal('eor_online_price', 12, 2)->nullable();
            $table->integer('eor_online_total')->nullable();
            $table->decimal('eor_booklet_price', 12, 2)->nullable();
            $table->integer('eor_booklet_total')->nullable();
            $table->decimal('amount', 12, 2)->nullable();
            $table->decimal('convenience_fee', 12, 2)->nullable();
            $table->decimal('total_amount', 12, 2)->nullable();
            $table->string('status')->nullable()->default('pending');
            $table->string('payment_status')->nullable()->default('pending');
            $table->string('payment_reference')->nullable();
            $table->string('payment_type')->nullable();
            $table->string('payment_option')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('atp_no')->nullable();
            $table->string('printer_accreditation')->nullable();
            $table->string('layout')->nullable();
            $table->datetime('payment_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_headers');
    }
}
