<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionHeaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_headers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('bundle_id', 255)->nullable();
            $table->string('or_code', 255)->nullable();
            $table->string('type')->nullable();
            $table->string('year_month')->nullable();
            $table->string('merchant_code')->nullable();
            $table->string('submerchant_code')->nullable();
            $table->string('merchant_name')->nullable();
            $table->longText('merchant_address')->nullable();
            $table->string('tin')->nullable();
            $table->string('tel_no')->nullable();
            $table->string('office_name')->nullable();
            $table->string('office_code')->nullable();

            $table->string('terminal_code')->nullable();
            $table->string('teller_code')->nullable();
            $table->string('teller_name')->nullable();
            
            $table->decimal('amount', 25, 2)->nullable();
            $table->datetime('date_expiry')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_headers');
    }
}
