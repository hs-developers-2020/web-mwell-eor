<?php

use App\Laravel\Models\Section;
use Illuminate\Database\Seeder;

class SectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Section::truncate();
        Section::create(['code' => 'A', 'name' => 'Agriculture, Forestry and Fishing']);
        Section::create(['code' => 'B', 'name' => 'Mining and Quarrying']);
        Section::create(['code' => 'C', 'name' => 'Manufacturing']);
        Section::create(['code' => 'D', 'name' => 'Electricity, Gas, Steam and Air Conditioning Suuply']);
        Section::create(['code' => 'E', 'name' => 'Water Supply; Sewerage, Waste Management and Remediation Activities']);
        Section::create(['code' => 'F', 'name' => 'Construction']);
        Section::create(['code' => 'G', 'name' => 'Wholesale and Retail Trade; Repair of Motor Vehicles and Motocycles']);
        Section::create(['code' => 'H', 'name' => 'Transportation and storage']);
        Section::create(['code' => 'I', 'name' => 'Accomodation and Food Service Activities']);
        Section::create(['code' => 'J', 'name' => 'Information and Communication']);
        Section::create(['code' => 'K', 'name' => 'Financial and Insurance Activities']);
        Section::create(['code' => 'L', 'name' => 'Real Estate Activities']);
        Section::create(['code' => 'M', 'name' => 'Professional, Scientific and Technical Activities']);
        Section::create(['code' => 'N', 'name' => 'Administrative and Support Service Activities']);
        Section::create(['code' => 'O', 'name' => 'Public Administration and Defense; Compulsory Social Security']);
        Section::create(['code' => 'P', 'name' => 'Education']);
        Section::create(['code' => 'Q', 'name' => 'Human Health and Social Work Activities']);
        Section::create(['code' => 'R', 'name' => 'Arts, Entertainment and Recreation']);
        Section::create(['code' => 'S', 'name' => 'Other Service Activities']);
        Section::create(['code' => 'T', 'name' => 'Acivities od Households as Employers; Undifferentiated Goods-and Services-producing Activities of Households for Own Use']);
        Section::create(['code' => 'U', 'name' => 'Activities of Extra-territorial Organizations and Bodies']);
    }
}
