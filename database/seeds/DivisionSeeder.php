<?php

use App\Laravel\Models\Division;
use Illuminate\Database\Seeder;

class DivisionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Division::truncate();
        Division::create(['section_code' => 'A', 'code' => '01', 'name' => 'Crop and Animal Production, Hunting and Related Service Activities']);
        Division::create(['section_code' => 'A', 'code' => '02', 'name' => 'Forestry and Logging']);
        Division::create(['section_code' => 'A', 'code' => '03', 'name' => 'Fishing and Aquaculture']);
        Division::create(['section_code' => 'B', 'code' => '05', 'name' => 'Mining of Coal and Lignite']);
        Division::create(['section_code' => 'B', 'code' => '06', 'name' => 'Extraction of Crude Petroleum and Natural Gas']);
        Division::create(['section_code' => 'B', 'code' => '07', 'name' => 'Mining of Metal Ores']);
        Division::create(['section_code' => 'B', 'code' => '08', 'name' => 'Other Mining and Quarrying']);
        Division::create(['section_code' => 'B', 'code' => '09', 'name' => 'Mining Support Service Activities']);
        Division::create(['section_code' => 'C', 'code' => '10', 'name' => 'Manufacture of Food Products']);
        Division::create(['section_code' => 'C', 'code' => '11', 'name' => 'Manufacture of Beverages']);
        Division::create(['section_code' => 'C', 'code' => '12', 'name' => 'Manufacture of Tobacco Products']);
        Division::create(['section_code' => 'C', 'code' => '13', 'name' => 'Manufacture of Textiles']);
        Division::create(['section_code' => 'C', 'code' => '14', 'name' => 'Manufacture of Wearing Apparel']);
        Division::create(['section_code' => 'C', 'code' => '15', 'name' => 'Manufacture of Leather and Related Products']);
        Division::create(['section_code' => 'C', 'code' => '16', 'name' => 'Manufacture of Wood and of Products of Wood and Cork, Except Furniture; Manufacture of Articles of Bamboo, Cane, Rattan and the like; Manufacture of Articles of Straw and Plaiting Materials']);
        Division::create(['section_code' => 'C', 'code' => '17', 'name' => 'Manufacture of Paper and Paper Products']);
        Division::create(['section_code' => 'C', 'code' => '18', 'name' => 'Printing and Reproduction of Recorded Media']);
        Division::create(['section_code' => 'C', 'code' => '19', 'name' => 'Manufacture of Coke and Refined Petroleum Products']);
        Division::create(['section_code' => 'C', 'code' => '20', 'name' => 'Manufacture of Chemicals and Chemical Products']);
        Division::create(['section_code' => 'C', 'code' => '21', 'name' => 'Manufacture of Basic Pharmaceutical Products and Pharmaceutical Preparations']);
        Division::create(['section_code' => 'C', 'code' => '22', 'name' => 'Manufacture of Rubber and Plastic Products']);
        Division::create(['section_code' => 'C', 'code' => '23', 'name' => 'Manufacture of Other Non-Metallic Mineral Products']);
        Division::create(['section_code' => 'C', 'code' => '24', 'name' => 'Manufacture of Basic Metals']);
        Division::create(['section_code' => 'C', 'code' => '25', 'name' => 'Manufacture of Fabricated Metal Products, Except Machinery and Equipment']);
        Division::create(['section_code' => 'C', 'code' => '26', 'name' => 'Manufacture of Computer, Electronic and Optical Products']);
        Division::create(['section_code' => 'C', 'code' => '27', 'name' => 'Manufacture of Electrical Equipment']);
        Division::create(['section_code' => 'C', 'code' => '28', 'name' => 'Manufacture of Machinery and Equipment, N.e.c']);
        Division::create(['section_code' => 'C', 'code' => '29', 'name' => 'Manufacture of Motor Vehicles, Trailers and Semi-trailers']);
        Division::create(['section_code' => 'C', 'code' => '30', 'name' => 'Manufacture of Other Transport Equipment']);
        Division::create(['section_code' => 'C', 'code' => '31', 'name' => 'Manufacture of Furniture']);
        Division::create(['section_code' => 'C', 'code' => '32', 'name' => 'Other Manufacturing']);
        Division::create(['section_code' => 'C', 'code' => '33', 'name' => 'Repair and Installation of Machinery and Equipment']);
        Division::create(['section_code' => 'D', 'code' => '35', 'name' => 'Electricity, Gas, Steam and Air Conditioning Supply']);
        Division::create(['section_code' => 'E', 'code' => '36', 'name' => 'Water Collection, Treatment and Supply']);
        Division::create(['section_code' => 'E', 'code' => '37', 'name' => 'Sewerage']);
        Division::create(['section_code' => 'E', 'code' => '38', 'name' => 'Waste Collection, Treatment and Disposal Activities; Material Recovery']);
        Division::create(['section_code' => 'E', 'code' => '39', 'name' => 'Remediation Activities and Other Waste Management Services']);
        Division::create(['section_code' => 'F', 'code' => '41', 'name' => 'Construction of Buildings']);
        Division::create(['section_code' => 'F', 'code' => '42', 'name' => 'Civil Engineering']);
        Division::create(['section_code' => 'F', 'code' => '43', 'name' => 'Specialized Construction Activities']);
        Division::create(['section_code' => 'G', 'code' => '45', 'name' => 'Wholesale and Retail Trade and Repair of Motor Vehicles and Motorcycles']);
        Division::create(['section_code' => 'G', 'code' => '46', 'name' => 'Wholesale Trade, Except of Motor Vehicles and Motorcycles']);
        Division::create(['section_code' => 'G', 'code' => '47', 'name' => 'Retail Trade, Except of Motor Vehicles and Motorcycles']);
        Division::create(['section_code' => 'H', 'code' => '49', 'name' => 'Land Transport and Transport Via Pipelines']);
        Division::create(['section_code' => 'H', 'code' => '50', 'name' => 'Water Transport']);
        Division::create(['section_code' => 'H', 'code' => '51', 'name' => 'Air Transport']);
        Division::create(['section_code' => 'H', 'code' => '52', 'name' => 'Warehousing and Support Activities for Transportation']);
        Division::create(['section_code' => 'H', 'code' => '53', 'name' => 'Postal and Courier Activities']);
        Division::create(['section_code' => 'I', 'code' => '55', 'name' => 'Accomodation']);
        Division::create(['section_code' => 'I', 'code' => '56', 'name' => 'Food and Beverage Service Activities']);
        Division::create(['section_code' => 'J', 'code' => '58', 'name' => 'Publishing Activities']);
        Division::create(['section_code' => 'J', 'code' => '59', 'name' => 'Motion Picture, Video and Television Programme Production, Sound Recording and Music Publishing Activities']);
        Division::create(['section_code' => 'J', 'code' => '60', 'name' => 'Programming and Broadcasting Activities']);
        Division::create(['section_code' => 'J', 'code' => '61', 'name' => 'Telecommunications']);
        Division::create(['section_code' => 'J', 'code' => '62', 'name' => 'Computer Programming, Consultancy and Related Activities']);
        Division::create(['section_code' => 'J', 'code' => '63', 'name' => 'Information Service Activities']);
        Division::create(['section_code' => 'K', 'code' => '64', 'name' => 'Financial Service Activities, Except Insurance and Pension Funding']);
        Division::create(['section_code' => 'K', 'code' => '65', 'name' => 'Insurance, Reinsurance and Pension Funding, Except Compulsory Social Security']);
        Division::create(['section_code' => 'K', 'code' => '66', 'name' => 'Activities Auxillary to Financial Service and Insurance Activities']);
        Division::create(['section_code' => 'L', 'code' => '68', 'name' => 'Real Estate Activities']);
        Division::create(['section_code' => 'M', 'code' => '69', 'name' => 'Legal and Accounting Activites']);
        Division::create(['section_code' => 'M', 'code' => '70', 'name' => 'Activities of Head Offices; Management Consultancy Activities']);
        Division::create(['section_code' => 'M', 'code' => '71', 'name' => 'Architecture and Engineering Activities; Technical Testing and Analysis']);
        Division::create(['section_code' => 'M', 'code' => '72', 'name' => 'Scientific Research and Development']);
        Division::create(['section_code' => 'M', 'code' => '73', 'name' => 'Advertising and Market Research']);
        Division::create(['section_code' => 'M', 'code' => '74', 'name' => 'Other Professional, Scienific and Technical Activities']);
        Division::create(['section_code' => 'M', 'code' => '75', 'name' => 'Vetirinary Activities']);
        Division::create(['section_code' => 'N', 'code' => '77', 'name' => 'Rental and Leasing Activities']);
        Division::create(['section_code' => 'N', 'code' => '78', 'name' => 'Employment Activities']);
        Division::create(['section_code' => 'N', 'code' => '79', 'name' => 'Travel Agency, Tour Operator, Reservation Service and Related Activities']);
        Division::create(['section_code' => 'N', 'code' => '80', 'name' => 'Security and Investigation Activities']);
        Division::create(['section_code' => 'N', 'code' => '81', 'name' => 'Services to Buildings and Landscape Activities']);
        Division::create(['section_code' => 'N', 'code' => '82', 'name' => 'Office Administrative, Office Support and Other Business Support Activities']);
        Division::create(['section_code' => 'O', 'code' => '84', 'name' => 'Public Administration and Defense Compulsory Social Security']);
        Division::create(['section_code' => 'P', 'code' => '85', 'name' => 'Education']);
        Division::create(['section_code' => 'Q', 'code' => '86', 'name' => 'Human Health Activities']);
        Division::create(['section_code' => 'Q', 'code' => '87', 'name' => 'Residential Care Activities']);
        Division::create(['section_code' => 'Q', 'code' => '88', 'name' => 'Social Work Activities without Accomodation']);
        Division::create(['section_code' => 'R', 'code' => '90', 'name' => 'Creative, Arts and Entertainment Activities']);
        Division::create(['section_code' => 'R', 'code' => '91', 'name' => 'Libraries, Archives, Museums and ther Cultural Activities']);
        Division::create(['section_code' => 'R', 'code' => '92', 'name' => 'Gambling and Betting Activities']);
        Division::create(['section_code' => 'R', 'code' => '93', 'name' => 'Sports Activities and Amusement and Recreation Activities']);
        Division::create(['section_code' => 'S', 'code' => '94', 'name' => 'Activities of Membership Organizations']);
        Division::create(['section_code' => 'S', 'code' => '95', 'name' => 'Repair of Computers and Personal and Household Goods']);
        Division::create(['section_code' => 'S', 'code' => '96', 'name' => 'Other Personal Service Activities']);
        Division::create(['section_code' => 'T', 'code' => '97', 'name' => 'Activities of Households as Employers of Domestic Personnel']);
        Division::create(['section_code' => 'T', 'code' => '98', 'name' => 'Undifferentiated Goods-and-services-producing Activities of Private Households for Own Use']);
        Division::create(['section_code' => 'U', 'code' => '99', 'name' => 'Activities of Extra-territorial Organizations and Bodies']);
    }
}
