<?php

use App\Laravel\Models\User;
use Illuminate\Database\Seeder;

class AdminAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::firstOrNew([
            'name' => "Master Account", 
            'status' => 'active',
            'email' => "admin@hs.com", 
            'username' => "admin_hs",
            'type' => "super_user",
        ]);

        $user->password = bcrypt("admin");
        $user->save();

        $user = User::firstOrNew([
            'name' => "Master Account", 
            'status' => 'active',
            'email' => "admin@eor.com", 
            'username' => "admin_eor",
            'type' => "super_user",
        ]);

        $user->password = bcrypt("admin");
        $user->save();
    }
}
