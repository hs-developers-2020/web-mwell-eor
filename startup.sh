#!/bin/bash

cp /home/site/wwwroot/default /etc/nginx/sites-available/default
service nginx reload

# Go to app and migrate
cd /home/site/wwwroot && php artisan migrate 