<?php 

namespace App\Transformers;

use Input,Str;
use JWTAuth, Carbon, Helper;

use App\Models\Transactionbundle as Header;
use App\Models\TransactionDetail as Detail;
use App\Models\EorBundle as Bundle;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Transformers\MasterTransformer;

class TransactionTransformer extends TransformerAbstract{

	protected $availableIncludes = [
		'header'
    ];


	public function transform(Bundle $bundle) {

	    return [
	     	'id' => $bundle->id ?: 0,
	     	'bundle_id' => $bundle->bundle_id,
	     	'dst_fee' => $bundle->dst_fee ?: '',
	     	'convenience_fee' => $bundle->convenience_fee ?: '',
	     	'fname' => $bundle->fname ?: '',
	     	'lname' => $bundle->lname ?: '',
	     	'mname' => $bundle->mname ?: '',
	     	'suffix' => $bundle->suffix ?: '',
	     	'address' => $bundle->address ?: '',
	     	'name' => $bundle->name ?: '',
	     	'tin' => $bundle->tin ?: '',
	     	'total' => $bundle->total ?: 0,
	     	'display_total' => Helper::money_format($bundle->total),
	     	'subtotal' => $bundle->sub_total ?: 0,
	     	'display_subtotal' => Helper::money_format($bundle->sub_total),
	     	'vat_sales' => Helper::money_format($bundle->vat_sales) ?: "0",
	     	'vat_amount' => Helper::money_format($bundle->vat_amount) ?: "0",
	     	'vat_percentage' => $bundle->vat_percentage ?: 0,
	     	'payment_reference_code' => $bundle->payment_reference_code,
	     	'transaction_code' => $bundle->transaction_code,
	     	'bir_number' => "352-890-256-00000",
	     	'date_created' => [
				'date_db' => $bundle->date_db($bundle->created_at,env("MASTER_DB_DRIVER","mysql")),
				'month_year' => $bundle->month_year($bundle->created_at),
				'time_passed' => $bundle->time_passed($bundle->created_at),
				'timestamp' => $bundle->created_at
			],
			'date_expiry' => [
				'date_db' => $bundle->date_db($bundle->date_expiry,env("MASTER_DB_DRIVER","mysql")),
				'month_year' => $bundle->month_year($bundle->date_expiry),
				'time_passed' => $bundle->time_passed($bundle->date_expiry),
				'timestamp' => $bundle->date_expiry
			],
	     	'url' => route('portal.generate', $bundle->payment_reference_code),
	     ];
	}

	public function includeHeader(Bundle $bundle){
		return $this->collection($bundle->items ?: new Header, new TransactionHeaderTransformer);
	}
}