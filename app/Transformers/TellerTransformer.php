<?php 

namespace App\Transformers;

use Input,Str;
use JWTAuth, Carbon, Helper;

use App\Models\Teller;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Transformers\MasterTransformer;

class TellerTransformer extends TransformerAbstract{

	protected $availableIncludes = [
    ];


	public function transform(Teller $teller) {
	    return [
	     	'id' => $teller->id ?: 0,
	     	'code' => $teller->code,
	     	'office_code' => $teller->office_code ?: '',
	     	'merchant_id' => $teller->merchant_id ?: '',
	     	'fname' => $teller->fname ?: '',
	     	'mname' => $teller->mname ?: '',
	     	'lname' => $teller->lname ?: '',
	     	'pin_code' => $teller->pin_code ?: '',
	     ];
	}

}