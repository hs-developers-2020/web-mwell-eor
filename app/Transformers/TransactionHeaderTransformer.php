<?php 

namespace App\Transformers;

use Input,Str;
use JWTAuth, Carbon, Helper;

use App\Models\TransactionHeader as Header;
use App\Models\TransactionDetail as Detail;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Transformers\MasterTransformer;

class TransactionHeaderTransformer extends TransformerAbstract{

	protected $availableIncludes = [
		'items'
    ];


	public function transform(Header $header) {
	    return [
	     	'id' => $header->id ?: 0,
	     	'type' => $header->type,
	     	'bundle_id' => $header->bundle_id,
	     	'or' => $header->or_code ?: '',
	     	'merchant_code' => $header->merchant_code ?: '',
	     	'submerchant_code' => $header->submerchant_code ?: '',
	     	'merchant_name' => $header->merchant_name ?: '',
	     	'office_name' => $header->office_name ?: '',
	     	'office_code' => $header->office_code ?: '',
	     	'merchant_address' => $header->merchant_address ?: '',
	     	'tin' => $header->tin ?: '',
	     	'tel_no' => $header->tel_no ?: '',

	     	'terminal_code' => $header->terminal_code ?: '',
	     	'teller_code' => $header->teller_code ?: '',
	     	'teller_name' => $header->teller_name ?: '',
	     	'amount' => $header->amount ?: '',
	     	'date_created' => [
				'date_db' => $header->date_db($header->created_at,env("MASTER_DB_DRIVER","mysql")),
				'month_year' => $header->month_year($header->created_at),
				'time_passed' => $header->time_passed($header->created_at),
				'timestamp' => $header->created_at
			],
			'date_expiry' => [
				'date_db' => $header->date_db($header->date_expiry,env("MASTER_DB_DRIVER","mysql")),
				'month_year' => $header->month_year($header->date_expiry),
				'time_passed' => $header->time_passed($header->date_expiry),
				'timestamp' => $header->date_expiry 
			],
	     ];
	}

	public function includeItems(Header $header){
		return $this->collection($header->items ?: new Detail, new TransactionDetailTransformer);
	}
}