<?php 

namespace App\Transformers;

use Input,Str;
use JWTAuth, Carbon, Helper;

use App\Models\Merchant;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Transformers\MasterTransformer;

class MerchantTransformer extends TransformerAbstract{

	protected $availableIncludes = [
    ];


	public function transform(Merchant $merchant) {
	    return [
	     	'id' => $merchant->id ?: 0,
	     	'code' => $merchant->merchant_code,
	     	'name' => $merchant->merchant_name ?: '',
	     	'address' => $merchant->address ?: '',
	     	'tin' => $merchant->tin ?: '',
	     ];
	}

}