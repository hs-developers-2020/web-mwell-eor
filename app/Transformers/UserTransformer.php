<?php 

namespace App\Transformers;

use Input;
use JWTAuth, Carbon, Helper;
use App\Models\User;
use App\Models\Applicant;
Use App\Models\Payment;
use App\Models\PaymentTable;
use App\Models\Loan;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Transformers\MasterTransformer;

use Str;

class UserTransformer extends TransformerAbstract{

	protected $user,$auth;

	protected $availableIncludes = [
    ];

    public function __construct() {
    	$this->auth = Auth::user();
    }

	public function transform(User $user) {
	    return [
	     	'id' => $user->id ?:0,
	     	'email' => $user->email,
	     	'firstname' => $user->firstname,
	     	'lastname' => $user->lastname,
	     ];
	}
}