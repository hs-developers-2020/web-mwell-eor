<?php 

namespace App\Transformers;

use Input,Str;
use JWTAuth, Carbon, Helper;

use App\Models\TransactionHeader as Header;
use App\Models\TransactionDetail as Detail;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Transformers\MasterTransformer;

class TransactionDetailTransformer extends TransformerAbstract{

	protected $availableIncludes = [
    ];


	public function transform(Detail $detail) {
	    return [
	     	'id' => $detail->id ?: 0,
	     	'transaction_id' => $detail->transaction_id,
	     	'name' => $detail->name ?: '',
	     	'description' => $detail->description ?: '',
	     	'price' => $detail->price ?: '',
	     	'qty' => $detail->qty ?: '',
	     	'subtotal' => Helper::money_format($detail->price * $detail->qty),
	     	'date_created' => [
				'date_db' => $detail->date_db($detail->created_at,env("MASTER_DB_DRIVER","mysql")),
				'month_year' => $detail->month_year($detail->created_at),
				'time_passed' => $detail->time_passed($detail->created_at),
				'timestamp' => $detail->created_at
			],
	     ];
	}

}