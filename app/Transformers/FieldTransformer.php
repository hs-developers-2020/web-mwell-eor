<?php 

namespace App\Transformers;

use Input,Str;
use JWTAuth, Carbon, Helper;
use App\Models\CreditScore;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Transformers\MasterTransformer;

class FieldTransformer extends TransformerAbstract{

	protected $availableIncludes = [
		'options'
    ];


	public function transform(CreditScore $score) {

	    return [
	     	'id' => $score->id ?: 0,
	     	'field' => $score->field,
	     	'name' => Helper::get_field($score->field)
	     ];
	}

	public function includeOptions(CreditScore $score){
		if(Helper::get_options($score->field)->count()){
			return $this->collection(Helper::get_options($score->field), new OptionTransformer);
		} 

		return $this->collection(array(), new MasterTransformer);
	}
}