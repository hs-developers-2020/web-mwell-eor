<?php 

namespace App\Transformers;

use Input,Str;
use JWTAuth, Carbon, Helper;

use App\Models\Terminal;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Transformers\MasterTransformer;

class TerminalTransformer extends TransformerAbstract{

	protected $availableIncludes = [
    ];


	public function transform(Terminal $terminal) {
	    return [
	     	'id' => $terminal->id ?: 0,
	     	'office_code' => $terminal->office_code ?: '',
	     	'merchant_id' => $terminal->merchant_id ?: '',
	     	'terminal_number' => $terminal->terminal_number ?: '',
	     	'serial_code' => $terminal->serial_code ?: '',
	     	'device_name' => $terminal->device_name ?: '',
	     	'device_serial' => $terminal->device_serial ?: '',
	     ];
	}

}