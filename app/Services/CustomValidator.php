<?php 

namespace App\Services;

use Illuminate\Validation\Validator;
use App\Models\User as Account;
use App\Models\User;
use App\Models\Partner;
use App\Models\Teller;
use App\Models\Office;
use App\Models\Merchant;
use App\Models\Registrant;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class CustomValidator extends Validator
{

    public function validateUniqueUsername($attribute,$value,$parameters)
    {
        $username = Str::lower($value);
        $user_id = false;
        if($parameters) {
            $user_id = $parameters[0];
        }

        if($user_id) {
            $is_unique = Account::where('id', '<>', $user_id)->whereRaw("LOWER(username) = '{$username}'")->first();
        }else{
            $is_unique = Account::whereRaw("LOWER(username) = '{$username}'")->first();
        }

        return $is_unique ? false : true;
    }

    public function validateAlphaSpaces($attribute, $value, $parameters)
    {
        return preg_match('/^[\pL\s]+$/u', $value);
    }

    public function validateAlphaNumericDash($attribute, $value, $parameters)
    {
        return preg_match('/^[a-zA-Z0-9 -]*$/', $value);
    }

    public function validateUniqueEmail($attribute,$value,$parameters)
    {
        $account_id = (is_array($parameters) AND isset($parameters[0]) ) ? $parameters[0] : "";
        $account_type = (is_array($parameters) AND isset($parameters[1]) ) ? $parameters[1] : "user";

        $email = Str::lower($value);

        if($account_type =="partner") {
            $is_exist = Partner::whereRaw("LOWER(email) = '{$email}'")
                ->where('id', '<>', $account_id)
                ->first();
        }else{
            $is_exist = Account::whereRaw("LOWER(email) = '{$email}'")
                ->where('id', '<>', $account_id)
                ->first();
        }
        
        return $is_exist ? false: true;
    }

    public function validateUniquePartnerCode($attribute,$value,$parameters)
    {
        $account_id = (is_array($parameters) AND isset($parameters[0]) ) ? $parameters[0] : "";

        $code = Str::lower($value);

        $is_exist = Partner::whereRaw("LOWER(code) = '{$code}'")
            ->where('id', '<>', $account_id)
            ->first();
        
        return $is_exist ? false: true;
    }

    public function validateValidAccount($attribute, $value, $parameters)
    {
        $valid_accounts = ['mentor','mentee'];
        return in_array(Str::lower($value), $valid_accounts);
    }

    public function validateCurrentPassword($attribute, $value, $parameters)
    {
        $account_id = (is_array($parameters) AND isset($parameters[0]) ) ? $parameters[0] : "";
        $account_type = (is_array($parameters) AND isset($parameters[1]) ) ? $parameters[1] : "user";


        if($account_type == "partner") {
            $user_id = $parameters[0];
            $user = Partner::find($user_id);
            return Hash::check($value, $user->password);
        }else{
            $user_id = $parameters[0];
            $user = Account::find($user_id);
            return Hash::check($value, $user->password);
        }

        return false;
    }

    public function validateOldPassword($attribute, $value, $parameters)
    {
        
        if($parameters) {
            $user_id = $parameters[0];
            $user = User::find($user_id);
            return Hash::check($value, $user->password);
        }

        return false;
    }

    public function validatePasswordFormat($attribute,$value,$parameters)
    {
        return preg_match(("/^(?=.*)[A-Za-z\d!@#$%^&*()_+.<>]{6,20}$/"), $value);
    }

    public function validateUsernameFormat($attribute,$value,$parameters)
    {
        return preg_match(("/^(?=.*)[a-zA-Z0-9\d._+]{5,20}$/"), $value);
    }

    public function validateUniqueMerchant($attribute,$value,$parameters)
    {
        $code = Str::lower($value);
        $merchant_id = false;
        if($parameters) {
            $merchant_id = $parameters[0];
        }

        if($merchant_id) {
            $is_unique = Merchant::where('id', '<>', $merchant_id)->whereRaw("LOWER(merchant_code) = '{$code}'")->first();
        } else {
            $is_unique = Merchant::whereRaw("LOWER(merchant_code) = '{$code}'")->first();
        }

        return $is_unique ? false : true;
    }

    public function validateUniqueTeller($attribute,$value,$parameters)
    {
        $code = Str::lower($value);
        $teller_id = false;
        if($parameters) {
            $teller_id = $parameters[0];
        }

        if($teller_id) {
            $is_unique = Teller::where('id', '<>', $teller_id)->whereRaw("LOWER(code) = '{$code}'")->first();
        } else {
            $is_unique = Teller::whereRaw("LOWER(code) = '{$code}'")->first();
        }

        return $is_unique ? false : true;
    }

    public function validateUniqueOffice($attribute,$value,$parameters)
    {
        $code = Str::lower($value);
        $office_id = false;
        if($parameters) {
            $office_id = $parameters[0];
        }

        if($office_id) {
            $is_unique = Office::where('id', '<>', $office_id)->whereRaw("LOWER(code) = '{$code}'")->first();
        } else {
            $is_unique = Office::whereRaw("LOWER(code) = '{$code}'")->first();
        }

        return $is_unique ? false : true;
    }

    public function validatePendingApplication($attribute,$value,$parameters)
    {
        $email = Str::lower($value);

        return Registrant::where('email', $email)
            ->where('status', 'pending')
            ->count() ? false : true;
    }

    public function validateNotRegistered($attribute,$value,$parameters)
    {
        $email = Str::lower($value);

        return Registrant::where('email', $email)
            ->where('status', 'completed')
            ->count() ? false : true;
    }

    public function validateUniqueMerchantName($attribute,$value,$parameters)
    {
        $name = Str::lower($value);
        $merchant_id = (is_array($parameters) AND isset($parameters[0]) ) ? $parameters[0] : "";
        $name = addslashes($name);
        
        if($merchant_id) {
            $is_unique = Merchant::where('id', '<>', $merchant_id)->whereRaw("LOWER(merchant_name) = '{$name}'")->first();
        } else {
            $is_unique = Merchant::whereRaw("LOWER(merchant_name) = '{$name}'")->first();
        }

        return $is_unique ? false : true;
    }

    public function validateUniqueTin($attribute,$value,$parameters)
    {
        $merchant_id = (is_array($parameters) AND isset($parameters[0]) ) ? $parameters[0] : "";
        
        if($merchant_id) {
            $is_unique = Merchant::where('id', '<>', $merchant_id)->whereRaw("LOWER(tin) = '{$value}'")->first();
        } else {
            $is_unique = Merchant::whereRaw("LOWER(tin) = '{$value}'")->first();
        }

        return $is_unique ? false : true;
    }

    public function validateValidEmail($attribute,$value,$parameters)
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL);
    }
} 