<?php

namespace App\Nova\Dashboards;

use Laravel\Nova\Cards\Help;
use Laravel\Nova\Dashboards\Main as Dashboard;
use App\Nova\Metrics\TotalMerchants;
use App\Nova\Metrics\TotalReceipts;
use App\Nova\Metrics\TotalTerminals;


class Main extends Dashboard
{
    /**
     * Get the cards for the dashboard.
     *
     * @return array
     */
    public function cards()
    {
        return [
            // new Help,
            new TotalMerchants,
            new TotalReceipts,
            new TotalTerminals
        ];
    }
}
