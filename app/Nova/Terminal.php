<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\Gravatar;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Panel;
use Laravel\Nova\Fields\Country;
use Laravel\Nova\Fields\Place;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Http\Requests\NovaRequest;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Validation\Rule;
use Laravel\Nova\Fields\Date;
use App\Models\Merchant;

class Terminal extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Terminal::class;

    /**
     * Build an "index" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function indexQuery(NovaRequest $request, $query)
    {
        // return $query->where('type', User::SELLER_TYPE);
    }

    /**
     * Build a "detail" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function detailQuery(NovaRequest $request, $query)
    {
        // return $query->where('type', User::SELLER_TYPE);
    }

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    // public static $group = 'Users';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'merchant_id', 'terminal_number',
    ];

    // public static function authorizedToCreate(Request $request)
    // {
    //     return false;
    // }

    public function authorizedToDelete(Request $request)
    {
        return true;
    }

    // public function authorizedToUpdate(Request $request)
    // {
    //     return false;
    // }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Text::make('Terminal Number', 'terminal_number')
                ->rules('required', 'max:255'),
                // ->readonly(),

            Select::make('Merchant', 'merchant_id')
                ->options(Merchant::pluck('merchant_name', 'id')->toArray())
                ->rules('required')
                ->onlyOnForms(),

            Text::make('Merchant', function () {
                if ($this->merchant) {
                    return $this->merchant->merchant_name;
                }
            }),

            Text::make('Device Name', 'device_name')
                // ->sortable()
                ->rules('required', 'max:255'),

            Text::make('Device Serial', 'device_serial')
                ->rules('required', 'max:255'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [
            // (new \SLASH2NL\NovaBackButton\NovaBackButton())
            //     ->onlyOnDetail(),
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            // new \App\Nova\Filters\UserWithReferral,
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            // new \App\Nova\Actions\UserAccountVerify
        ];
    }

}
