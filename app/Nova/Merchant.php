<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\Gravatar;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Panel;
use Laravel\Nova\Fields\Country;
use Laravel\Nova\Fields\Place;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Http\Requests\NovaRequest;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Validation\Rule;
use Laravel\Nova\Fields\Date;

class Merchant extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Merchant::class;

    /**
     * Build an "index" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function indexQuery(NovaRequest $request, $query)
    {
        // return $query->where('type', User::SELLER_TYPE);
    }

    /**
     * Build a "detail" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function detailQuery(NovaRequest $request, $query)
    {
        // return $query->where('type', User::SELLER_TYPE);
    }

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    // public static $group = 'Users';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'merchant_code', 'merchant_name',
    ];

    /**
     * The relationships that should be eager loaded on index queries.
     *
     * @var array
     */
    // public static $with = [
    //     'verificationAddresses.province',
    //     'verificationAddresses.city',
    //     'verificationAddresses.barangay',
    // ];

    // public static function authorizedToCreate(Request $request)
    // {
    //     return false;
    // }

    public function authorizedToDelete(Request $request)
    {
        return true;
    }

    // public function authorizedToUpdate(Request $request)
    // {
    //     return false;
    // }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            // Image::make('Photo', 'profile_photo_path'),
            Text::make('Merchant Code', 'merchant_code')
                ->sortable()
                ->rules('required', 'max:255')
                ->creationRules('unique:merchants,merchant_code')
                ->updateRules('unique:merchants,merchant_code,{{resourceId}}'),

            Text::make('Merchant Name', 'merchant_name')
                ->sortable()
                ->rules('required', 'max:255')
                ->creationRules('unique:merchants,merchant_name')
                ->updateRules('unique:merchants,merchant_name,{{resourceId}}'),

            Text::make('Address')
                // ->sortable()
                ->rules('required', 'max:255'),

            Text::make('TIN', 'tin')
                // ->sortable()
                ->help('000-000-000-000')
                ->rules('required', 'max:255'),

            Text::make('Telephone Number', 'tel_no')
                // ->sortable()
                ->help('(XX) XXXX-XXXX / +63 9XX-XXX-XXX')
                ->rules('max:255'),

            Number::make('Max. Series', 'max_online')
                ->rules('required', 'min:1', 'max:255'),

            HasMany::make('Receipts', 'Receipts',  'App\Nova\Receipt')->nullable()->readonly(),


            // Number::make('Max. Booklet Series (x50)', 'max_booklet')
            //     // ->sortable()
            //     ->rules('required', 'max:255'),

            // Text::make('Email')
            //     ->sortable()
            //     ->rules('required', 'max:255'),

            // Boolean::make('Verified'),

            // Image::make('ID Photo 1', 'id_photo'),

            // Image::make('ID Photo 2', 'second_id_photo'),

            // Image::make('Selfie Photo', 'selfie_photo'),

            // new Panel('Address Information', $this->addressFields()),

            // Text::make('Referral Code', 'referral_code')
            //     ->sortable()
            //     ->rules(['nullable', Rule::unique('users')->ignore($this->id), 'max:32'])
            //     ->showOnDetail(),

            // Text::make('Referrals', function () {
            //     if ($this->referral_code == null) {
            //         return '';
            //     }
                
            //     $buyers = User::query()
            //         ->where('referral_code_used', $this->referral_code)
            //         ->where('type', 'buyer')
            //         ->count();

            //     $sellers = User::query()
            //         ->where('referral_code_used', $this->referral_code)
            //         ->where('type', 'seller')
            //         ->count();

            //     return $buyers . ' buyers , ' . $sellers . ' sellers ';
            // })->showOnDetail(),

            // HasMany::make('Products', 'Products',  'App\Nova\Product')->nullable()->readonly(),

            // HasMany::make('Orders', 'Orders',  'App\Nova\Order')->nullable()->readonly(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [
            // (new \SLASH2NL\NovaBackButton\NovaBackButton())
            //     ->onlyOnDetail(),
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            // new \App\Nova\Filters\UserWithReferral,
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            // new \App\Nova\Actions\UserAccountVerify
        ];
    }

}
