<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
// use Laravel\Nova\Filters\Filter;
use Laravel\Nova\Filters\BooleanFilter;
use App\Models\User;

class UserWithReferral extends BooleanFilter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    // public $component = 'select-filter';

    /**
     * The displayable name of the filter.
     *
     * @var string
     */
    public $name = 'Filter User';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        if (isset($value['referral']) and $value['referral'] != false) {
            $query->where('referrals_count', '>', 0);
        }

        if (isset($value['verification']) and $value['verification'] != false) {
            $query->whereNotNull('id_photo')
                ->whereNotNull('second_id_photo')
                ->whereNotNull('selfie_photo')
                ->where('verified', false);
        }

        if (isset($value['verified']) and $value['verified'] != false) {
            $query->where('verified', true);
        }
        
        return $query;
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        return [
            'With Referrals' => 'referral',
            'For Verification' => 'verification',
            'Verified' => 'verified',
        ];
    }
}
