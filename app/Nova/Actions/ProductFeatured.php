<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

class ProductFeatured extends Action
{
    use InteractsWithQueue, Queueable;

    /**
     * The displayable name of the action.
     *
     * @var string
     */
    public $name = 'Feature Product';

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        foreach ($models as $model) {
            $model->featured = ! $model->featured;
            $model->save();
        }

        return Action::message('Product(s) has been featured!');
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    // public function actions(Request $request)
    // {
    //     return [
    //         (new \App\Nova\Actions\UserAccountVerify)
    //             ->confirmText('Are you sure you want to activate this user?')
    //             ->confirmButtonText('Activate')
    //             ->cancelButtonText("Don't activate"),
    //     ];
    // }
}
