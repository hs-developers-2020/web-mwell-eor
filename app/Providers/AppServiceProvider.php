<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\CustomValidator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\URL;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        URL::forceScheme('https');

        Validator::resolver(
            function ($translator, $data, $rules, $messages) {
                return new CustomValidator($translator, $data, $rules, $messages);
            }
        );
    }
}
