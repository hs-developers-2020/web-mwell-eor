<?php

namespace App\Providers;

use App\Nova\Merchant;
use App\Nova\Transaction;
use Illuminate\Support\Facades\Gate;
use Laravel\Nova\Cards\Help;
use Illuminate\Http\Request;
use Laravel\Nova\Menu\Menu;
use Laravel\Nova\Menu\MenuItem;
use Laravel\Nova\Menu\MenuSection;
use App\Nova\Dashboards\Main;
use Laravel\Nova\Nova;
use Laravel\Nova\NovaApplicationServiceProvider;

class NovaServiceProvider extends NovaApplicationServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Nova::mainMenu(function (Request $request) {
        //     return [
                
        //         // MenuSection::dashboard(Main::class)->icon('chart-bar'),

        //         MenuItem::resource(Merchant::class),
        //         MenuItem::resource(Transaction::class),


        //         MenuSection::make('Transactions', [
        //             MenuItem::resource(Transaction::class),
        //             // MenuItem::resource(License::class),
        //         ])->icon('user')->collapsable(),

        //         // MenuSection::make('Content', [
        //         //     MenuItem::resource(Series::class),
        //         //     MenuItem::resource(Release::class),
        //         // ])->icon('document-text')->collapsable(),
        //     ];
        // });
        parent::boot();
    }

    /**
     * Register the Nova routes.
     *
     * @return void
     */
    protected function routes()
    {
        Nova::routes()
                ->withAuthenticationRoutes()
                ->withPasswordResetRoutes()
                ->register();
    }

    /**
     * Register the Nova gate.
     *
     * This gate determines who can access Nova in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
        Gate::define('viewNova', function ($user) {
            return in_array($user->email, [
                //
            ]);
        });
    }

    /**
     * Get the cards that should be displayed on the default Nova dashboard.
     *
     * @return array
     */
    protected function cards()
    {
        return [
            new Help,
        ];
    }

    /**
     * Get the extra dashboards that should be displayed on the Nova dashboard.
     *
     * @return array
     */
    protected function dashboards()
    {
        return [
            new Main,
        ];
    }

    /**
     * Get the tools that should be listed in the Nova sidebar.
     *
     * @return array
     */
    public function tools()
    {
        return [];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
