<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\DateFormatterTrait;

class TransactionHeader extends Model
{
    
    use SoftDeletes, DateFormatterTrait;
    
    /**
     * Enable soft delete in table
     *
     * @var boolean
     */
    protected $softDelete = true;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'transaction_headers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that created within the model.
     *
     * @var array
     */
    protected $appends = [];

    protected $casts = [
        'date_expiry' => 'date',
        'transaction_date' => 'date',
    ];

    public function items()
    {
        return $this->hasMany('App\Models\TransactionDetail', 'transaction_id', 'id');
    }

    public function bundle()
    {
        return $this->belongsTo('App\Models\EorBundle', 'bundle_id', 'bundle_id');
    }
}