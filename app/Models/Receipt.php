<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\DateFormatterTrait;

class Receipt extends Model
{
    
    use SoftDeletes, DateFormatterTrait;
    
    /**
     * Enable soft delete in table
     *
     * @var boolean
     */
    protected $softDelete = true;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'receipts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that created within the model.
     *
     * @var array
     */
    protected $appends = ['receipt_number'];

    public function items()
    {
        return $this->hasMany('App\Models\TransactionHeader', 'bundle_id', 'bundle_id');
    }

    public function getReceiptNumberAttribute($value)
    {
        return str_pad($value, 7, "0", STR_PAD_LEFT);
    }

    public function merchant()
    {
        return $this->belongsTo(Merchant::class);
    }

}