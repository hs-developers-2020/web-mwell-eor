<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Terminal extends Model
{
    
    use SoftDeletes;
    
    /**
     * Enable soft delete in table
     *
     * @var boolean
     */
    protected $softDelete = true;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'terminals';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that created within the model.
     *
     * @var array
     */
    protected $appends = [];

    public function merchant()
    {
        return $this->belongsTo('App\Models\Merchant', 'merchant_id', 'id');
    }

    public function office()
    {
        return $this->belongsTo('App\Models\Office', 'office_code', 'code');
    }
}