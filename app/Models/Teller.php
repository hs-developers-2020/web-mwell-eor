<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Teller extends Model
{
    
    use SoftDeletes;
    
    /**
     * Enable soft delete in table
     *
     * @var boolean
     */
    protected $softDelete = true;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tellers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that created within the model.
     *
     * @var array
     */
    protected $appends = [];

    public function getNameAttribute()
    {
        return "{$this->fname} {$this->lname}";
    }

    public function office()
    {
        return $this->belongsTo('App\Models\Office', 'office_code', 'code');
    }

    public function merchant()
    {
        return $this->belongsTo('App\Models\Merchant', 'merchant_id', 'id');
    }

    public function scopeKeyword($query,$keyword = null)
    {
        if($keyword) {
            return $query->whereRaw("LOWER(fname) LIKE '%{$keyword}%'")
                ->orWhereRaw("LOWER(lname) LIKE '%{$keyword}%'")
                ->orWhereRaw("LOWER(code) LIKE '%{$keyword}%'")
                ->orWhereRaw("CONCAT(fname, ' ', lname) LIKE '%{$keyword}%'");
        }
    }
}