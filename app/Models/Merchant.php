<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Merchant extends Model
{
    
    use SoftDeletes;
    
    /**
     * Enable soft delete in table
     *
     * @var boolean
     */
    protected $softDelete = true;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'merchants';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that created within the model.
     *
     * @var array
     */
    protected $appends = [];

    public function section()
    {
        return $this->belongsTo('App\Models\Section', 'section_code', 'code');
    }

    public function division()
    {
        return $this->belongsTo('App\Models\Division', 'division_code', 'code');
    }

    public function receipts()
    {
        return $this->hasMany(Receipt::class);
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\TransactionHeader', 'merchant_code', 'merchant_code');
    }

    public function booklet_transactions()
    {
        return $this->hasMany('App\Models\BookletTransaction', 'merchant_code', 'merchant_code');
    }

    public function getAvailableOrAttribute()
    {
        $total_or_series = Order::where('merchant_code', $this->merchant_code)->where('status', 'COMPLETED')->sum('eor_online_qty');
        $total_used = TransactionHeader::where('merchant_code', $this->merchant_code)->count();

        return $total_or_series - $total_used;
    }

    public function getAvailableBookletAttribute()
    {
        $total_or_series = Order::where('merchant_code', $this->merchant_code)->where('status', 'COMPLETED')->sum('eor_booklet_qty');
        $total_used = BookletTransaction::where('merchant_code', $this->merchant_code)->where('status', 1)->count();
        
        return $total_or_series - $total_used;
    }

    public function scopeKeyword($query,$keyword = null)
    {
        if($keyword) {
            return $query->whereRaw("LOWER(merchant_name) LIKE '%{$keyword}%'")
                ->orWhereRaw("LOWER(merchant_code) LIKE '%{$keyword}%'");
        }
    }
}