<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RegistrantLog extends Model
{
    
    use SoftDeletes;
    
    /**
     * Enable soft delete in table
     *
     * @var boolean
     */
    protected $softDelete = true;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'registration_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that created within the model.
     *
     * @var array
     */
    protected $appends = [];

    public function getNameAttribute()
    {
        return $this->firstname . ' ' . $this->lastname;
    }

    public function scopeKeyword($query,$keyword = null)
    {
        if($keyword) {
            return $query->whereRaw("LOWER(firstname) LIKE '%{$keyword}%'")
                ->orWhereRaw("LOWER(lastname) LIKE '%{$keyword}%'")
                ->orWhereRaw("LOWER(email) LIKE '%{$keyword}%'")
                ->orWhereRaw("CONCAT(firstname, ' ', lastname) LIKE '%{$keyword}%'");
        }
    }
}