<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;

use App\Transformers\TransformerManager;
use App\Transformers\UserTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use JWTAuth;

use Illuminate\Http\Request;

class LoginController extends Controller
{

    protected $response = array();

    public function __construct()
    {
        $this->response = array(
        "msg" => "Bad Request.",
        "status" => false,
        'status_code' => "BAD_REQUEST"
        );
        $this->response_code = 400;
        $this->transformer = new TransformerManager;
    }

    public function authenticate(Request $request)
    {
        $username = Str::lower(request('email'));
        $password = request('password');

        $field = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        $user = User::where($field, $username)->first();

        if(!$user) {
            $this->response['msg'] = "Invalid username/password.";
            $this->response['status_code'] = "LOGIN_FAILED";
            goto invalid_login;
        }

        if (Auth::attempt([$field => $username, 'password' => $password ])) {

            $user = Auth::user();

            $this->response['msg'] = "Welcome, {$user->name}!";
            $this->response['status'] = true;
            $this->response['status_code'] = "LOGIN_SUCCESS";
            // $this->response['token'] = JWTAuth::fromUser($user, ['did' => request('device_id')]);
            $this->response['first_login'] = false;
            $this->response['data'] = $this->transformer->transform($user, new UserTransformer, 'item');
            $this->response_code = 200;

            // event( new UserAction($user, ['login']) );

        } else {
            invalid_login:
            $this->response['msg'] = "Invalid username/password.";
            $this->response['status_code'] = "LOGIN_FAILED";
        }

        callback:
        return response()->json($this->response, $this->response_code);
    }

    public function logout(Request $request, $format = '')
    {
        $user = $request->user();
        // $user->last_activity = null;
        $user->save();
        $user_name = $user->name;

        // $device_id = request('device_id');
        // UserDevice::where('device_id', $device_id)->where('user_id', $user->id)->update(['is_login' => "0"]);

        JWTAuth::invalidate(JWTAuth::getToken());

        $this->response['msg'] = "See you again, {$user_name}!";
        $this->response['status'] = true;
        $this->response['status_code'] = "LOGOUT_SUCCESS";
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
        case 'json' :
            return response()->json($this->response, $this->response_code);
        break;
        case 'xml' :
            return response()->xml($this->response, $this->response_code);
            break;
        }
    }


}