<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\PasswordReset;
use App\Models\User;
use App\Http\Requests\Api\ResetPasswordRequest;
use App\Transformers\TransformerManager;
use Illuminate\Support\Str;


class ResetPasswordController extends Controller
{

    protected $data = array();

    public function __construct()
    {
        $this->response = array(
        "msg" => "Bad Request.",
        "status" => false,
        'status_code' => "BAD_REQUEST"
        );
        $this->response_code = 400;
        $this->transformer = new TransformerManager;
    }

    public function reset_password(ResetPasswordRequest $request, $format = '')
    {

        $token = Str::lower(request('validation_token'));
        $password_reset = PasswordReset::whereRaw("LOWER(token) = '{$token}'")->first();

        $user = User::where('email', $password_reset->email)->first();
        $user->password = bcrypt(request('password'));
        $user->save();

        PasswordReset::where('token', $token)->delete();

        // $this->response['msg'] = Helper::get_response_message("LOGIN_SUCCESS", ['name' => $user->name]);
        // $this->response['status'] = TRUE;
        // $this->response['status_code'] = "LOGIN_SUCCESS";
        // $this->response['token'] = JWTAuth::fromUser($user, ['did' => request('device_id')]);
        // $this->response['first_login'] = FALSE;
        // $this->response['data'] = $this->transformer->transform($user, new UserTransformer,'item');
        $this->response['msg'] = "New password has been stored successfully.";
        $this->response['status'] = true;
        $this->response['status_code'] = "SUCCESSFUL_RESET_PASSWORD";
        $this->response_code = 200;

        // event( new UserAction($user, ['login']) );

        callback:
        switch(Str::lower($format)){
        case 'json' :
            return response()->json($this->response, $this->response_code);
        break;
        case 'xml' :
            return response()->xml($this->response, $this->response_code);
            break;
        }
    }

}