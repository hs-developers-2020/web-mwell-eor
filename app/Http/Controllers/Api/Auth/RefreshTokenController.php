<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Support\Str;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\UserTransformer;
use App\Transformers\TransformerManager;

class RefreshTokenController extends Controller
{

    protected $response = array();

    public function __construct()
    {
        $this->response = array(
        "msg" => "Bad Request.",
        "status" => false,
        'status_code' => "BAD_REQUEST"
        );
        $this->response_code = 400;
        $this->transformer = new TransformerManager;
    }

    public function refresh(Request $request, $format = '')
    {

        $this->response['msg'] = "New session successfully created.";
        $this->response['status'] = true;
        $this->response['status_code'] = "NEW_TOKEN";
        $this->response['new_token'] = $request->new_token;
        $this->response['data'] = $this->transformer->transform($request->user, new UserTransformer, 'item');
        $this->response_code = 200;
        
        callback:
        switch(Str::lower($format)){
        case 'json' :
            return response()->json($this->response, $this->response_code);
        break;
        case 'xml' :
            return response()->xml($this->response, $this->response_code);
            break;
        }
    }
}