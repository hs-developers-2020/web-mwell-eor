<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;

use App\Http\Requests\Api\RegisterRequest;

use App\Transformers\UserTransformer;
use App\Transformers\TransformerManager;

use Illuminate\Support\Str;

class RegisterController extends Controller
{

    protected $response = array();

    public function __construct()
    {
        $this->response = array(
        "msg" => "Bad Request.",
        "status" => false,
        'status_code' => "BAD_REQUEST"
        );
        $this->response_code = 400;
        $this->transformer = new TransformerManager;

    }

    public function store(RegisterRequest $request, $format = '')
    {
        $user = new User;
        $user->username = Str::lower(request('username'));
        $user->password = bcrypt(request('password'));
        $user->type = 'user';

        $user->save();
        
        $this->response['msg'] = "Account successfully registered ";
        $this->response['status'] = true;
        $this->response['status_code'] = "REGISTER_SUCCESS";
        $this->response['data'] = $this->transformer->transform($user, new UserTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
        case 'json' :
            return response()->json($this->response, $this->response_code);
        break;
        case 'xml' :
            return response()->xml($this->response, $this->response_code);
            break;
        }
    }
}