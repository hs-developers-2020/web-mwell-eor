<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Models\PaymentTable;

use App\Transformers\TransformerManager;
use App\Transformers\CollectionTransformer;
use App\Transformers\PaymentTransformer;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;


// use App\Events\AuditTrailActivity;

use Illuminate\Http\Request;
// use App\Http\Requests\Api\PaymentRequest;

class CollectionController extends Controller
{

    protected $response = array();

    public function __construct()
    {
        $this->response = array(
        "msg" => "Bad Request.",
        "status" => false,
        'status_code' => "BAD_REQUEST"
        );
        $this->response_code = 400;
        $this->transformer = new TransformerManager;
    }

    public function for_collection(Request $request, $format="") 
    {
        $per_page = request('per_page', 10);
        $page = request('page', 1);
        $user = $request->user();

        $today = now()->format("Y-m-d");

        $this_week_start = Carbon::parse($today)->startOfWeek()->format("Y-m-d");
        $this_week_end = Carbon::parse($today)->endOfWeek()->format("Y-m-d");

        $this->response['msg'] = "List of For Collection";

        $transactions = PaymentTable::where('status', 'pending')
            ->whereIn(
                'transaction_id', function ($query) use ($user) {
                                                return $query->select('id')
                                                    ->where('account_officer_id', $user->id)
                                                    ->whereRaw('deleted_at IS NULL')
                                                    ->from('loan');
                }
            )
                                        ->whereRaw("DATE(payment_date) >= '{$this_week_start}'")
                                        ->whereRaw("DATE(payment_date) <= '{$this_week_end}'")
                                        ->whereRaw("DATE(payment_date) >= '{$today}'")
                                        ->whereIn(
                                            'transaction_id', function ($query) {
                                                $query->select('id')
                                                    ->where('status', 'ongoing')
                                                    ->from('loan');
                                            }
                                        )
                                        ->orderBy('payment_date', "DESC")
                                        ->paginate($per_page);

        $this->response['status'] = true;
        $this->response['status_code'] = "FOR_COLLECTION_LIST";
        $this->response['total_page'] = $transactions->lastPage();
        $this->response['current_page'] = $transactions->currentPage();
        $this->response['last_page'] = $transactions->lastPage();
        $this->response['has_morepages'] = $transactions->hasMorePages();
        $this->response['data'] = $this->transformer->transform($transactions, new CollectionTransformer, 'collection');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
        case 'json' :
            return response()->json($this->response, $this->response_code);
            break;
        case 'xml' :
            return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function past_due(Request $request, $format="")
    {
        $per_page = request('per_page', 10);
        $page = request('page', 1);
        $user = $request->user();

        $today = now()->format("Y-m-d");

        $this_week_start = Carbon::parse($today)->startOfWeek()->format("Y-m-d");
        $this_week_end = Carbon::parse($today)->endOfWeek()->format("Y-m-d");

        $this->response['msg'] = "List of Past Due Transactions";

        $transactions = PaymentTable::where('status', 'pending')
            ->whereIn(
                'transaction_id', function ($query) use ($user) {
                                                return $query->select('id')
                                                    ->where('account_officer_id', $user->id)
                                                    ->whereRaw('deleted_at IS NULL')
                                                    ->from('loan');
                }
            )
                                        ->whereRaw("DATE(payment_date) < '{$today}'")
                                        ->whereIn(
                                            'transaction_id', function ($query) {
                                                $query->select('id')
                                                    ->where('status', 'ongoing')
                                                    ->from('loan');
                                            }
                                        )
                                        ->orderBy('payment_date', "DESC")
                                        ->paginate($per_page);

        $this->response['status'] = true;
        $this->response['status_code'] = "PAST_DUE_LIST";
        $this->response['total_page'] = $transactions->lastPage();
        $this->response['current_page'] = $transactions->currentPage();
        $this->response['last_page'] = $transactions->lastPage();
        $this->response['has_morepages'] = $transactions->hasMorePages();
        $this->response['data'] = $this->transformer->transform($transactions, new CollectionTransformer, 'collection');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
        case 'json' :
            return response()->json($this->response, $this->response_code);
            break;
        case 'xml' :
            return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function collected(Request $request, $format="")
    {
        $per_page = request('per_page', 10);
        $page = request('page', 1);
        $user = $request->user();

        $today = now()->format("Y-m-d");

        $this_week_start = Carbon::parse($today)->startOfWeek()->format("Y-m-d");
        $this_week_end = Carbon::parse($today)->endOfWeek()->format("Y-m-d");

        $this->response['msg'] = "List of Collected";

        $transactions = Payment::keyword(request('keyword'))
            ->whereIn(
                'transaction_id', function ($query) use ($user) {
                                        return $query->select('id')
                                            ->where('account_officer_id', $user->id)
                                            ->whereRaw('deleted_at IS NULL')
                                            ->from('loan');
                }
            )
                                ->orderBy('created_at', "DESC")
                                ->paginate($per_page);

        $this->response['status'] = true;
        $this->response['status_code'] = "COLLECTED_LIST";
        $this->response['total_page'] = $transactions->lastPage();
        $this->response['current_page'] = $transactions->currentPage();
        $this->response['last_page'] = $transactions->lastPage();
        $this->response['has_morepages'] = $transactions->hasMorePages();
        $this->response['data'] = $this->transformer->transform($transactions, new PaymentTransformer, 'collection');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
        case 'json' :
            return response()->json($this->response, $this->response_code);
            break;
        case 'xml' :
            return response()->xml($this->response, $this->response_code);
            break;
        }
    }
}