<?php

namespace App\Http\Controllers\Api;

use App\Models\Teller;

use App\Transformers\TransformerManager;

// use App\Http\Requests\Api\PageRequest;
use App\Http\Requests\Api\TellerRequest;
use App\Http\Resources\TellerResource;

class TellerController extends Controller
{
    protected $response = array();

    public function __construct()
    {
        $this->response = array(
        "msg" => "Bad Request.",
        "status" => false,
        'status_code' => "BAD_REQUEST"
        );
            
        $this->response_code = 400;
        $this->transformer = new TransformerManager;
    }

    public function store(TellerRequest $request)
    {
        $merchant = request('merchant_data');

        $teller = new Teller;
        $teller->code = request('code');
        $teller->merchant_id = $merchant->id;
        $teller->fname = request('fname');
        $teller->mname = request('mname');
        $teller->lname = request('lname');
        $teller->pin_code = request('pin_code');
        $teller->save();

        return new TellerResource($teller);
    }
}