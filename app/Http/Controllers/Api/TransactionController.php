<?php

namespace App\Http\Controllers\Api;

use App\Models\TransactionHeader as Header;
use App\Models\TransactionDetail;
use App\Models\EorBundle as Bundle;
use App\Models\Merchant;
use App\Models\Receipt;

use App\Transformers\TransformerManager;
use App\Transformers\TransactionTransformer;

use App\Http\Requests\Api\PageRequest;
use App\Http\Requests\Api\TransactionRequest;
use App\Http\Requests\Api\PosRequest;

use App\Services\Helper;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

use App\Http\Resources\TransactionResource;
use App\Http\Resources\ReceiptResource;

class TransactionController extends Controller
{
    protected $response = array();

    public function __construct()
    {
        $this->response = array(
            "msg" => "Bad Request.",
            "status" => false,
            'status_code' => "BAD_REQUEST"
        );
        $this->response_code = 400;
        $this->transformer = new TransformerManager;
    }

    public function online(TransactionRequest $request)
    {
        $auth = $request->user();
        $sequence = Header::where('type', 'ONLINE')->count();
        $total_qty = 0;
        $totalAmount = 0;
        $total = Receipt::count();

        // We need to check if the merchant have enough OR credit
        // Get merchant
        $merchant = Merchant::where('merchant_code', request('merchant_code'))->first();

        $receiptCount = Receipt::where('merchant_id', $merchant->id)->count();

        // If merchant reach the max OR
        if ($receiptCount >= $merchant->max_online) {
            return response()->json(['message' => 'Insufficient EOR series.'], 406);
        }

        $receipt = Receipt::where('merchant_id', $merchant->id)->max('receipt_number');
        
        if ($receipt == null) {
            $receiptNumber = 0;
        } else {
            $receiptNumber = $receipt;
        }
           
        try {
            DB::beginTransaction();

            $bundle = new Receipt;
            $bundle->bundle_id = str_pad($receiptNumber + 1, 7, "0", STR_PAD_LEFT);
            $bundle->receipt_number = $receiptNumber + 1;
            $bundle->payment_reference_code = request('payment_reference_code');
            $bundle->transaction_code = request('transaction_code');

            $bundle->fname = request('fname');
            $bundle->lname = request('lname');
            $bundle->mname = request('mname');
            $bundle->suffix = request('suffix');
            $bundle->email = request('email');
            $bundle->address = request('address');
            $bundle->tin = request('tin');

            $bundle->dst_fee = request('dst_fee');
            $bundle->convenience_fee = request('convenience_fee');
            $bundle->date_expiry = now()->addYears(5);
            $bundle->save();

            if (! $merchant) {
                // abort(404);
                DB::rollBack();
            }

            $bundle->merchant_id = $merchant->id; 
            $bundle->save();

            $header = new Header;
            $header->bundle_id = $bundle->bundle_id;
            $header->type = "ONLINE";
            $header->merchant_code = request('merchant_code');
            $header->submerchant_code = request('submerchant_code');
            $header->merchant_name = $merchant->merchant_name;
            $header->merchant_address = $merchant->address;
            $header->tin = $merchant->tin;
            $header->tel_no = $merchant->tel_no;
            $header->or_code = $header->merchant_code . $header->submerchant_code . '-' . Helper::date_format(now(), 'y') . '-'. str_pad(++$sequence, 8, "0", STR_PAD_LEFT);
            $header->date_expiry = now()->addYears(5);
            $header->save();

            if(is_array(request('items'))) {
                foreach (request('items') as $key => $value) {
                    $detail = new TransactionDetail;
                    $detail->transaction_id = $header->id;
                    $detail->name = $value['name'];
                    $detail->description = $value['description'];
                    $detail->price = $value['price'];
                    $detail->qty = $value['qty'];
                    $detail->date_expiry = now()->addYears(5);
                    $detail->save();

                    $total_qty += $detail->qty;
                    $totalAmount += ($detail->price * $detail->qty);
                }
            }

            $bundle->total_qty = $total_qty;
            $bundle->sub_total = $totalAmount;
            $bundle->total = $totalAmount + $bundle->dst_fee + $bundle->convenience_fee;

            $bundle->vat_sales = $bundle->total / 1.12;
            $bundle->vat_percentage = 12;
            $bundle->vat_amount = $bundle->total - $bundle->vat_sales;
            $bundle->save();    

            $header->amount = $totalAmount;
            $header->save();

            DB::commit();

            return new ReceiptResource($bundle);

        }catch(\Exception $e){
            DB::rollBack();
            $this->response['status'] = false;
            $this->response['status_code'] = "SERVER_ERROR";
            $this->response['msg'] = "Server Error. Please try again. ".$e->getMessage();
            $this->response_code = 500;
            goto callback;
        }

        callback:
        return response()->json($this->response, $this->response_code);
    }

    public function pos(PosRequest $request, $format="") 
    {
        
    }

    public function show(PageRequest $request)
    {
        $bundle = request('bundle_data');

        $this->response['status'] = true;
        $this->response['status_code'] = "TRANSACTION_DETAILS";
        $this->response['msg'] = "Bundle Details.";
        $this->response['data'] = $this->transformer->transform($bundle, new TransactionTransformer, 'item');
        $this->response_code = 200;

        return response()->json($this->response, $this->response_code);
    }
}