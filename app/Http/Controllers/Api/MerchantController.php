<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\PageRequest;

use App\Models\Terminal;
use App\Models\Teller;
use App\Models\Merchant;
use App\Http\Resources\MerchantResource;

class MerchantController extends Controller
{
    protected $response = array();

    public function __construct()
    {

    }

    public function index(PageRequest $request)
    {
        return MerchantResource::collection(Merchant::all());
    }

    public function show(PageRequest $request, $id)
    {
        return new MerchantResource(Merchant::findOrFail($id));
    }
    
}