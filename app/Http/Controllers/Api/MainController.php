<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Transformers\TransformerManager;
use App\Transformers\TerminalTransformer;
use App\Transformers\TellerTransformer;
use App\Transformers\MerchantTransformer;

use App\Http\Requests\Api\PageRequest;

use App\Models\Terminal;
use App\Models\Teller;
use App\Models\Merchant;

class MainController extends Controller
{
    protected $response = array();

    public function __construct()
    {
        $this->response = [
            "msg" => "Bad Request.",
            "status" => false,
            'status_code' => "BAD_REQUEST"
        ];
        $this->response_code = 400;
        $this->transformer = new TransformerManager;
    }

    public function index(PageRequest $request)
    {
        $this->response['status'] = true;
        $this->response_code = 200;

        return response()->json($this->response, $this->response_code);
    }

    public function terminal(PageRequest $request)
    {
        $merchant_code = request('merchant_code');

        $terminals = Terminal::whereIn(
            'merchant_id', function ($query) use ($merchant_code) {
                                    return $query->select('id')
                                        ->whereRaw("LOWER(merchant_code) = '{$merchant_code}'")
                                        ->whereRaw('deleted_at IS NULL')
                                        ->from('merchant');
            }
        )->paginate(10);

        $this->response['total'] = $terminals->total();
        $this->response['has_morepage'] = $terminals->hasMorePages();
        $this->response['last_page'] = $terminals->lastPage();
        $this->response['current_page'] = $terminals->currentPage();
        $this->response['data'] = $this->transformer->transform($terminals, new TerminalTransformer, 'collection');
        $this->response_code = 200;

        return response()->json($this->response, $this->response_code);
    }

    public function teller(PageRequest $request)
    {
        $merchant_code = request('merchant_code', false);

        $tellers = Teller::where(
            function ($query) use ($merchant_code) {
                if($merchant_code) {
                    $merchant = Merchant::where('merchant_code', $merchant_code)->first();
                    if($merchant) {
                        $query->where('merchant_id', $merchant->id);
                    }
                }
            }
        )->paginate(10);

        $this->response['total'] = $tellers->total();
        $this->response['has_morepage'] = $tellers->hasMorePages();
        $this->response['last_page'] = $tellers->lastPage();
        $this->response['current_page'] = $tellers->currentPage();
        $this->response['data'] = $this->transformer->transform($tellers, new TellerTransformer, 'collection');
        $this->response_code = 200;

        return response()->json($this->response, $this->response_code);
    }

    public function merchant(PageRequest $request)
    {
        $merchants = Merchant::where('merchant_type', 'govt')->paginate(10);

        $this->response['total'] = $merchants->total();
        $this->response['has_morepage'] = $merchants->hasMorePages();
        $this->response['last_page'] = $merchants->lastPage();
        $this->response['current_page'] = $merchants->currentPage();
        $this->response['data'] = $this->transformer->transform($merchants, new MerchantTransformer, 'collection');
        $this->response_code = 200;

        return response()->json($this->response, $this->response_code);
    }
}