<?php 

namespace App\Http\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Models\Sale;
use App\Models\Partner;


/*
 * Requests used in this Controller
 */
use App\Http\Requests\Portal\SalesImportRequest;
use App\Http\Requests\Portal\SalesResetRequest;
use App\Http\Requests\Portal\PageRequest;
/*
 * App Classes used in this Controller
 */
use Str,Carbon,Helper,DB,Excel,Auth;

class ReportingController extends Controller
{
    
    protected $data,$import_process,$counter,$update_counter;
    
    public function __construct()
    {
        parent::__construct();
        array_merge($this->data, parent::get_data());
        $this->data['partners'] = ['' => "--All Partners--"]+Partner::orderBy('name', "ASC")->pluck('name', 'id')->toArray();

    }
    
    public function index(PageRequest $request)
    {
        $this->data['commission'] = 0;
        $this->data['partner_id'] = session()->get('is_admin', 'no') == "yes" ? request('partner_id', '') : Auth::guard('partner')->user()->id;

        $this->data['start_date'] = request('start_date', now()->format('m/d/Y'));
        $this->data['end_date'] = request('end_date', now()->format('m/d/Y'));

        $this->data['sales'] = Sale::where(
            function ($query) {
                if(session()->get('is_admin', 'no') == "no") {
                    $partner_code = Auth::guard('partner')->user()->code;
                    $query->where('partner_code', $partner_code);
                }else{
                    $partner = Partner::find($this->data['partner_id']);
                    if($partner) {
                        $query->where('partner_code', $partner->code);
                    }
                }
            }
        )
                                ->where('sale_date', '>=', Helper::date_db($this->data['start_date']))
                                ->where('sale_date', '<=', Helper::date_db($this->data['end_date']))
                                ->orderBy('created_at', 'DESC')->get();

        return view('portal.reporting.index', $this->data);
    }

    public function import()
    {
        return view('portal.reporting.import', $this->data);
    }

    public function import_data(SalesImportRequest $request)
    {
        $this->counter = 0;
        $this->update_counter = 0;
        $this->sale_date = Carbon::parse(request('sale_date'))->format("Y-m-d");
        Excel::load(
            $request->file('file'), function ($reader) {
                $results = $reader->all();
                if(Str::lower($results->getTitle()) != "sale list") {
                    $this->import_process = false;
                    return 0;
                }
                try{
                    DB::beginTransaction();
                    $results->each(
                        function ($row,$index) {
                            $is_duplicate = Sale::where('sale_date', $this->sale_date)
                            ->where('sku', Str::lower($row->sku.""))
                            ->first();

                            if($is_duplicate AND strlen(Str::lower($row->sku."")) > 0) {
                                $is_duplicate->qty = $row->qty;
                                $is_duplicate->price = $row->price;
                                $is_duplicate->sale_price = $row->sale_price;
                                $is_duplicate->total = $row->total;
                                $is_duplicate->discount = $row->discount;
                                $is_duplicate->gp = $row->gp;
                                $is_duplicate->save();
                                $this->update_counter++;
                            }else{

                                $sale = new Sale;
                                $sale->partner_code = Str::lower((string)$row->partner_code);

                                $partner = Partner::where('code', $sale->partner_code)->first();
                                $sale->margin = 0;

                                if($partner) {
                                    $sale->margin = $partner->margin;
                                }

                                $sale->product_name = Str::lower($row->product_name);
                                $sale->sale_date = $this->sale_date;
                                $sale->sku = Str::lower($row->sku."");
                                $sale->qty = $row->qty;
                                $sale->price = $row->price;
                                $sale->sale_price = $row->sale_price;
                                $sale->total = $row->total;
                                $sale->discount = $row->discount;
                                $sale->gp = $row->gp;
                                $sale->save();
                                $this->counter++;
                            }

                        }
                    );

                       $this->import_process = true;

                    DB::commit();
                }catch(\Exception $e){
                    DB::rollBack();
                    session()->flash('notification-status', "failed");
                    session()->flash('notification-msg', "Server Error. Please try again.".$e->getMessage());
                    return redirect()->back();
                }
            }
        );

        if($this->import_process === false) {
            session()->flash('notification-status', 'failed');
            session()->flash('notification-msg', "Invalid excel file uploaded.");
            return redirect()->route('portal.reporting.import');
        }

        session()->flash('notification-status', 'success');
        session()->flash('notification-msg', "{$this->counter} sales record have been imported and {$this->update_counter} sales record have updated.");
        return redirect()->route('portal.reporting.index');
    }

    public function reset()
    {
        return view('portal.reporting.reset', $this->data);
    }

    public function reset_data(SalesResetRequest $request)
    {
        $record = Sale::where('sale_date', Carbon::parse(request('sale_date'))->format("Y-m-d"));

        $counter  = $record->count();

        session()->flash('notification-status', 'success');
        session()->flash('notification-msg', "{$counter} has been deleted.");

        $record->delete();
        return redirect()->route('portal.reporting.index');

        // return view('portal.reporting.reset');
    }
}