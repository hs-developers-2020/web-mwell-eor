<?php 

namespace App\Http\Controllers\Portal;


/*
 * Requests used in this Controller
 */
use App\Http\Requests\Portal\PageRequest;

/*
 * App Classes used in this Controller
 */
use Illuminate\Support\Facades\Auth;


class AuthController extends Controller
{

    protected $data;
    
    public function login(PageRequest $request)
    {
        session()->put('admin_login', "no");
        return view('portal.auth.login');
    }

    public function admin_login(PageRequest $request)
    {
        session()->put('admin_login', "yes");
        return view('portal.auth.login');
    }

    public function authenticate(PageRequest $request)
    {
        $username = request('username');
        $password = request('password');
        $remember = request('remember_me', 1);

        $field = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        $guard = "partner";
        if(session()->get('admin_login', 'no') == "yes") {
            $guard = "admin";
        }

        $attempt = Auth::guard($guard)->attempt(['username' => $username, 'password' => $password], $remember);

        if($attempt) {
            session()->put('is_admin', 'no');

            if(session()->get('admin_login', 'no') == "yes") {
                session()->put('is_admin', 'yes');
            }

            $user = Auth::guard($guard)->user();
            if($user->status != "active") {
                Auth::guard($guard)->logout();
                session()->flash('notification-status', 'warning');
                session()->flash('notification-msg', 'Account is not yet active.');
                goto callback;
            }

            session()->forget('admin_login');

            session()->flash('notification-status', 'success');
            session()->flash('notification-msg', 'Login successfully.');

            return redirect()->route('portal.index');
        }

        session()->flash('notification-status', "failed");
        session()->flash('notification-msg', "Invalid account credentials.");

        callback:
        return redirect()->back();

    }

    public function logout()
    {
        $guard = "partner";
        $route = "portal.login";

        if(session()->get('is_admin', 'no') == "yes") {
            $guard = "admin";
            $route = "portal.admin_login";

        }

        Auth::guard($guard)->logout();

        session()->flash('notification-status', 'success');
        session()->flash('notification-msg', 'You are now signed off.');
        return redirect()->route($route);
    }
}