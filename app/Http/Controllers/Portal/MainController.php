<?php 

namespace App\Http\Controllers\Portal;

use App\Models\EorBundle;
use App\Models\Receipt;
use App\Services\Helper;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Barryvdh\DomPDF\Facade\Pdf;

class MainController extends Controller
{

    protected $data;
    
    public function __construct()
    {
        parent::__construct();
        array_merge($this->data, parent::get_data());
    }

    public function index()
    {
        return redirect('manager');
        return redirect()->route('admin.index');
    }

    public function generate($code = null)
    {
        $this->data['transaction'] = Receipt::query()
            ->where('payment_reference_code', Str::upper($code))
            ->orderBy('created_at', 'DESC')
            ->first();

        if(! $this->data['transaction']) {
            return view('generic.confirmation');
        }

        $this->data['flag'] = true;
        switch (count($this->data['transaction']->items)) {
        case 1:
            $this->data['flag'] = false;
        case 2:
            if($this->data['transaction']->dst_fee || $this->data['transaction']->dst_fee > 0) {
                $this->data['flag'] = false;
            }
            $this->data['index'] = 1;
            break;
        default:
            $this->data['flag'] = true;
            $this->data['index'] = 2;
            break;
        }

        $pdf = Pdf::loadView('pdf.receipt', $this->data)->setPaper('legal');
        return $pdf->download("{$code}.pdf");
    }

    public function send_eor()
    {
        //yesterday until 4pm 
        $start_date = Helper::date_format(now()->subDay(2), "Y-m-d")." 16:01";
        $end_date = Helper::date_format(now()->subDays(1), "Y-m-d")." 16:00";

        $transactions = EorBundle::where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date)->get();
        
        foreach ($transactions as $key => $value) {
            Log::info('INFO', array($value));
            if($value->email) {
                $this->data['transaction'] = $value;
                $this->data['flag'] = true;
                switch (count($this->data['transaction']->items)) {
                case 1:
                    $this->data['flag'] = false;
                case 2:
                    if($this->data['transaction']->dst_fee || $this->data['transaction']->dst_fee > 0) {
                              $this->data['flag'] = false;
                    }
                    $this->data['index'] = 1;
                    break;
                default:
                    $this->data['flag'] = true;
                    $this->data['index'] = 2;
                    break;
                }

                $pdf = PDF::loadView('pdf.receipt', $this->data);
                Mail::send(
                    'emails.eor', $this->data, function ($message) use ($pdf) {
                        $message->from(env('MAIL_ADDRESS', 'noreply@domain.com'));
                        $message->to($this->data['transaction']->email);
                        $message->subject("ELECTRONIC ONLINE RECEIPT");
                        $message->attachData(
                            $pdf->output(), "eor.pdf", [
                                           'mime' => 'application/pdf',
                                       ]
                        );
                    }
                );
            }
        }

        Log::info('Emails was successfully sent');
    }
}