<?php 

namespace App\Http\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Models\Partner as Account;

/*
 * Requests used in this Controller
 */
use App\Http\Requests\Portal\PartnerRequest;
use App\Http\Requests\Portal\PartnerPasswordRequest;
use App\Http\Requests\Portal\PartnerImportRequest;
use App\Http\Requests\Portal\PageRequest;
/*
 * App Classes used in this Controller
 */
use Str,Carbon,Helper,DB,Excel;

class PartnerController extends Controller
{

    protected $data,$import_process,$counter;
    public function __construct()
    {
        parent::__construct();
        array_merge($this->data, parent::get_data());
        $this->data['statuses'] = ['' => "--Choose Status--",'active' => "Active",'inactive' => "Inactive"];
    }
    
    public function index(PageRequest $request)
    {
        $per_page = request('per_page', 10);
        $this->data['accounts'] = Account::orderBy('created_at', "DESC")->get();
        return view('portal.partner.index', $this->data);
    }

    public function create()
    {
        return view('portal.partner.create', $this->data);
    }

    public function store(PartnerRequest $request)
    {
        try{
            DB::beginTransaction();
            $account = new Account;
            $account->name = request('name');
            $account->owner_name = request('owner_name');
            $account->code = Str::lower(request('code'));
            $account->username = Str::lower(request('username'));
            $account->email = Str::lower(request('email'));
            $account->password = bcrypt(request('password'));
            $account->status = request('status');
            $account->margin = (float)request('margin');

            $account->save();

            DB::commit();

            session()->flash('notification-status', "success");
            session()->flash('notification-msg', "New account has been created.");
            return redirect()->route('portal.partner.index');

        }catch(\Exception $e){
            DB::rollBack();
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Server Error. Please try again.".$e->getMessage());
            return redirect()->back();
        }

    }

    public function edit($partner_id = null)
    {
        $this->data['account'] = Account::find($partner_id);

        if(!$this->data['account']) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Account not found.");
            return redirect()->route('portal.partner.index');
        }
        return view('portal.partner.edit', $this->data);
    }

    public function update(PartnerRequest $request, $partner_id = null)
    {
        $account = Account::find($partner_id);

        if(!$account) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Account not found.");
            return redirect()->route('portal.partner.index');
        }
        try{
            DB::beginTransaction();
            $account->name = request('name');
            $account->owner_name = request('owner_name');
            $account->code = Str::lower(request('code'));
            $account->username = Str::lower(request('username'));
            $account->email = Str::lower(request('email'));
            $account->status = request('status');
            $account->margin = (float)request('margin');
            
            $account->save();

            DB::commit();

            session()->flash('notification-status', "success");
            session()->flash('notification-msg', "Account has been modified.");
            return redirect()->route('portal.partner.index');

        }catch(\Exception $e){
            DB::rollBack();
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Server Error. Please try again.".$e->getMessage());
            return redirect()->back();
        }

    }

    public function reset_password($partner_id = null)
    {
        $this->data['account'] = Account::find($partner_id);

        if(!$this->data['account']) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Account not found.");
            return redirect()->route('portal.partner.index');
        }
        return view('portal.partner.reset-password', $this->data);
    }

    public function update_reset_password(PartnerPasswordRequest $request,$partner_id)
    {

        $account = Account::find($partner_id);

        if(!$account) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Account not found.");
            return redirect()->route('portal.partner.index');
        }

        try{
            DB::beginTransaction();
            $account->password = bcrypt(request('password'));
            $account->save();

            DB::commit();

            session()->flash('notification-status', "success");
            session()->flash('notification-msg', "New password has been configured.");
            return redirect()->route('portal.partner.index');

        }catch(\Exception $e){
            DB::rollBack();
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Server Error. Please try again.".$e->getMessage());
            return redirect()->back();
        }

    }

    public function import()
    {
        return view('portal.partner.import', $this->data);
    }

    public function import_account(PartnerImportRequest $request)
    {
        $this->counter = 0;
        Excel::load(
            $request->file('file'), function ($reader) {
                $results = $reader->all();
                if(Str::lower($results->getTitle()) != "partner list") {
                    $this->import_process = false;
                    return 0;
                }
                try{
                    DB::beginTransaction();
                    $results->each(
                        function ($row,$index) {
                            if($index > 0 AND strlen($row->partner_name) > 0 AND strlen($row->email) > 0  AND strlen($row->username) > 0 AND strlen($row->code) > 0 ) {
                                $is_duplicate = Account::whereRaw("LOWER(email) = '{$row->email}'")
                                    ->orWhereRaw("LOWER(username) = '{$row->username}'")
                                    ->first();

                                if(!$is_duplicate) {
                                     $account = new Account;
                                     $account->username = Str::lower((string)$row->username);
                                     $account->email = Str::lower($row->email);
                                     $account->owner_name = $row->owner_name;
                                     $account->code = Str::lower((string)$row->code);
                                     $account->name = $row->partner_name;
                                     $account->password = bcrypt((string)$row->password);
                                     $account->status = $row->status == "active" ? 'active' : 'inactive';
                                     $account->save();
                                     $this->counter++;
                                }
                            }
                        }
                    );

                       $this->import_process = true;

                    DB::commit();
                }catch(\Exception $e){
                    DB::rollBack();
                    session()->flash('notification-status', "failed");
                    session()->flash('notification-msg', "Server Error. Please try again.".$e->getMessage());
                    return redirect()->back();
                }
            }
        );

        if($this->import_process === false) {
            session()->flash('notification-status', 'failed');
            session()->flash('notification-msg', "Invalid excel file uploaded.");
            return redirect()->route('portal.partner.import');
        }

        session()->flash('notification-status', 'success');
        session()->flash('notification-msg', "{$this->counter} new partners has been added.");
        return redirect()->route('portal.partner.index');
    }
}