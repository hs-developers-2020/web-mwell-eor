<?php 

namespace App\Http\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Models\User as Account;

/*
 * Requests used in this Controller
 */
use App\Http\Requests\Portal\AdministratorRequest;
use App\Http\Requests\Portal\AdministratorPasswordRequest;
use App\Http\Requests\Portal\PageRequest;

/*
 * App Classes used in this Controller
 */
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class AdministratorController extends Controller
{

    protected $data;
    public function __construct()
    {
        parent::__construct();
        array_merge($this->data, parent::get_data());
        $this->data['statuses'] = ['' => "--Choose Status--",'active' => "Active",'inactive' => "Inactive"];
    }
    
    public function index(PageRequest $request)
    {
        $per_page = request('per_page', 10);
        $this->data['accounts'] = Account::where('id', '<>', 1)->orderBy('created_at', "DESC")->paginate($per_page);
        return view('portal.administrator.index', $this->data);
    }

    public function create()
    {
        return view('portal.administrator.create', $this->data);
    }

    public function store(AdministratorRequest $request)
    {
        try{
            DB::beginTransaction();
            $account = new Account;
            $account->name = request('name');
            $account->username = Str::lower(request('username'));
            $account->email = Str::lower(request('email'));
            $account->password = bcrypt(request('password'));
            $account->status = request('status');
            $account->save();

            DB::commit();

            session()->flash('notification-status', "success");
            session()->flash('notification-msg', "New account has been created.");
            return redirect()->route('portal.administrator.index');

        }catch(\Exception $e){
            DB::rollBack();
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Server Error. Please try again.".$e->getMessage());
            return redirect()->back();
        }

    }

    public function edit($administrator_id = null)
    {
        $this->data['account'] = Account::find($administrator_id);

        if(!$this->data['account']) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Account not found.");
            return redirect()->route('portal.administrator.index');
        }
        return view('portal.administrator.edit', $this->data);
    }

    public function update(AdministratorRequest $request, $administrator_id = null)
    {
        $account = Account::find($administrator_id);

        if(!$account) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Account not found.");
            return redirect()->route('portal.administrator.index');
        }
        try{
            DB::beginTransaction();
            $account->name = request('name');
            $account->username = Str::lower(request('username'));
            $account->email = Str::lower(request('email'));
            $account->status = request('status');
            $account->save();

            DB::commit();

            session()->flash('notification-status', "success");
            session()->flash('notification-msg', "Account has been modified.");
            return redirect()->route('portal.administrator.index');

        }catch(\Exception $e){
            DB::rollBack();
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Server Error. Please try again.".$e->getMessage());
            return redirect()->back();
        }

    }

    public function reset_password($administrator_id = null)
    {
        $this->data['account'] = Account::find($administrator_id);

        if(!$this->data['account']) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Account not found.");
            return redirect()->route('portal.administrator.index');
        }
        return view('portal.administrator.reset-password', $this->data);
    }

    public function update_reset_password(AdministratorPasswordRequest $request,$administrator_id)
    {

        $account = Account::find($administrator_id);

        if(!$account) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Account not found.");
            return redirect()->route('portal.administrator.index');
        }

        try{
            DB::beginTransaction();
            $account->password = bcrypt(request('password'));
            $account->save();

            DB::commit();

            session()->flash('notification-status', "success");
            session()->flash('notification-msg', "New password has been configured.");
            return redirect()->route('portal.administrator.index');

        }catch(\Exception $e){
            DB::rollBack();
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Server Error. Please try again.".$e->getMessage());
            return redirect()->back();
        }

    }
}