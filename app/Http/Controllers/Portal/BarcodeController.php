<?php 

namespace App\Http\Controllers\Portal;

/*
 * Models used in this Controller
 */
use App\Models\Barcode;
use App\Models\Partner;

/*
 * Requests used in this Controller
 */
use App\Http\Requests\Portal\BarcodeRequest;
use App\Http\Requests\Portal\PageRequest;

/*
 * App Classes used in this Controller
 */
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Barryvdh\DomPDF\PDF;

class BarcodeController extends Controller
{

    protected $data;
    public function __construct()
    {
        parent::__construct();
        array_merge($this->data, parent::get_data());
        $this->data['partners'] = ['' => "--Choose Partner--"]+Partner::pluck('name', 'id')->toArray();
    }
    
    public function index(PageRequest $request)
    {
        $per_page = request('per_page', 10);
        $guard = session()->get('is_admin', 'no') == "yes" ? 'admin' : 'partner';
        $user = Auth::guard($guard)->user();
        $this->data['user'] = $user;
        $this->data['barcodes'] = Barcode::where(
            function ($query) use ($user) {
                if(session()->get('is_admin', 'no') == "no") {
                                                    return $query->where('partner_id', $user->id);
                }
            }
        )
                                        ->orderBy('created_at', "DESC")->paginate($per_page);
        return view('portal.barcode.index', $this->data);
    }

    public function create(PageRequest $request)
    {
        return view('portal.barcode.create', $this->data);
    }

    public function store(BarcodeRequest $request)
    {
        try{
            DB::beginTransaction();
            $barcode = new Barcode;
            if(session()->get('is_admin', 'no') == "yes") { $barcode->partner_id = request('partner_id');
            } else { $barcode->partner_id = Auth::guard('partner')->user()->id;
            }

            $barcode->code = Str::upper(request('code'));
            $barcode->product_name = Str::upper(request('name'));
            $barcode->qty = request('qty');
            if($barcode->qty%3 != 0) {
                $barcode->qty = $barcode->qty + (3-($barcode->qty%3)); 
            }
            $barcode->price = request('price');
            $barcode->save();

            DB::commit();

            session()->flash('notification-status', "success");
            session()->flash('notification-msg', "New set of barcode has been created.");
            return redirect()->route('portal.barcode.index');

        }catch(\Exception $e){
            DB::rollBack();
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Server Error. Please try again.".$e->getMessage());
            return redirect()->back();
        }

    }

    public function edit($barcode_id = null)
    {
        $guard = session()->get('is_admin', 'no') == "yes" ? 'admin' : 'partner';
        $user = Auth::guard($guard)->user();

        $this->data['barcode'] = Barcode::where(
            function ($query) use ($user) {
                if(session()->get('is_admin', 'no') == "no") {
                    return $query->where('partner_id', $user->id);
                }
            }
        )->find($barcode_id);

        if(!$this->data['barcode']) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Barcode record not found.");
            return redirect()->route('portal.barcode.index');
        }
        return view('portal.barcode.edit', $this->data);
    }

    public function update(BarcodeRequest $request, $barcode_id = null)
    {
        $guard = session()->get('is_admin', 'no') == "yes" ? 'admin' : 'partner';
        $user = Auth::guard($guard)->user();
        $barcode = Barcode::where(
            function ($query) use ($user) {
                if(session()->get('is_admin', 'no') == "no") {
                    return $query->where('partner_id', $user->id);
                }
            }
        )->find($barcode_id);

        if(!$barcode) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Barcode record not found.");
            return redirect()->route('portal.barcode.index');
        }
        try{
            DB::beginTransaction();
            if(session()->get('is_admin', 'no') == "yes") { $barcode->partner_id = request('partner_id');
            }
            
            $barcode->code = Str::upper(request('code'));
            $barcode->product_name = Str::upper(request('name'));
            $barcode->qty = request('qty');
            if($barcode->qty%3 != 0) {
                $barcode->qty = $barcode->qty + (3-($barcode->qty%3)); 
            }
            $barcode->price = request('price');
            $barcode->save();

            DB::commit();

            session()->flash('notification-status', "success");
            session()->flash('notification-msg', "Barcode record has been modified.");
            return redirect()->route('portal.barcode.index');

        }catch(\Exception $e){
            DB::rollBack();
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Server Error. Please try again.".$e->getMessage());
            return redirect()->back();
        }

    }

    public function download(PageRequest $request,$barcode_id = null)
    {
        $guard = session()->get('is_admin', 'no') == "yes" ? 'admin' : 'partner';
        $user = Auth::guard($guard)->user();
        $barcode = Barcode::where(
            function ($query) use ($user) {
                if(session()->get('is_admin', 'no') == "no") {
                    return $query->where('partner_id', $user->id);
                }
            }
        )->find($barcode_id);

        if(!$barcode) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Barcode record not found.");
            return redirect()->route('portal.barcode.index');
        }
        
        $this->data['barcode'] = $barcode;
        $pdf = PDF::loadView('pdf.barcode', $this->data);
        return $pdf->download("{$barcode->code}_BARCODE.pdf");
        // return view('portal.barcode.import',$this->data);
    }
}