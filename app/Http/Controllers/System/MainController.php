<?php 

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;

use App\Models\{Order, Merchant, Registrant};

use App\Http\Requests\System\PageRequest;

use PDF;

class MainController extends Controller
{

    protected $data;
    protected $guard = 'admin';
    
    public function __construct()
    {
        // parent::__construct();
        // array_merge($this->data, parent::get_data());
    }

    public function index(PageRequest $request)
    {
        $this->data['user'] = $request->user($this->guard);

        $this->data['transactions'] = Order::where(
            function ($query) {
                if(in_array($this->data['user']->type, ['user'])) {
                                                    $query->where('user_id', $this->data['user']->id);
                }
            }
        )->count();

        $this->data['merchants'] = Merchant::where(
            function ($query) {
                if(in_array($this->data['user']->type, ['user'])) {
                                                    $query->where('owner_user_id', $this->data['user']->id);
                }
            }
        )->count();

        $this->data['registrants'] = Registrant::where('status', 'pending')
            ->count();
                                                
        return view('system.index', $this->data);
    }

    public function generate($code = null)
    {
        $data = [];
        //if(strlen(trim($code)) == 0){ echo "Invalid record.";exit;}
        $pdf = PDF::loadView('pdf.receipt', $data);
        // $pdf->setPaper('A4', 'landscape');
        return $pdf->stream("receipt.pdf");
    }

    public function vertical($code = null)
    {
        $data = [];
        //if(strlen(trim($code)) == 0){ echo "Invalid record.";exit;}
        $pdf = PDF::loadView('pdf.vertical', $data);
        // $pdf->setPaper('A4', 'landscape');
        return $pdf->stream("vertical.pdf");
    }

    public function horizontal($code = null)
    {
        $data = [];
        //if(strlen(trim($code)) == 0){ echo "Invalid record.";exit;}
        $pdf = PDF::loadView('pdf.hori', $data);
        // $pdf->setPaper('legal', 'ob_list_handlers()cape');
        return $pdf->stream("horizontal.pdf");
    }
}