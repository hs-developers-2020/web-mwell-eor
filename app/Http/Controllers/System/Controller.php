<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller as BaseController;

use Illuminate\Routing\Route;
use Illuminate\Support\Carbon;

use App\Services\Helper;

class Controller extends BaseController
{

    protected $data;

    public function __construct()
    {
        // self::set_system_routes();
        // self::set_user_info();
        // self::set_date_today();
        // self::set_current_route();
        // self::set_marital_status();
        // self::set_months();
        // self::set_years();
        // self::set_days();
    }

    public function get_data()
    {
        $this->data['page_title'] = "";
        return $this->data;
    }


    public function set_current_route()
    {
        $this->data['current_route'] = Route::currentRouteName();
    }

    public function set_date_today()
    {
        $this->data['date_today'] = Helper::date_db(now());
    }

    public function set_system_routes()
    {

    }
}