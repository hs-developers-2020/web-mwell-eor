<?php 

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;

/*
 * Models used in this Controller
 */
use App\Models\Order;
use App\Models\Merchant;
use App\Models\BookletTransaction;

/*
 * Requests used in this Controller
 */
use App\Http\Requests\System\PageRequest;
use App\Http\Requests\System\TransactionRequest;

/*
 * App Classes used in this Controller
 */
use App\Services\Helper;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Barryvdh\DomPDF\PDF;
use Illuminate\Support\Facades\Http;

class TransactionController extends Controller
{

    protected $data;
    protected $guard = 'admin';
    public function __construct()
    {
        // parent::__construct();
        // array_merge($this->data, parent::get_data());
        $this->data['statuses'] = ['' => "--All Transactions--",'completed' => "Completed Transactions",'pending' => "Pending Transactions", 'cancelled' => "Cancelled Transactions", 'failed' => "Failed Transactions"];
    }
    
    public function index(PageRequest $request)
    {
        $per_page = request('per_page', 10);
        $this->data['user'] = $request->user($this->guard);

        $this->data['keyword'] = Str::lower(request('keyword'));
        $this->data['status'] = request('status');
        
        $first_record = Order::orderBy('created_at', 'ASC')->first();
        $start_date = request('start_date', now()->startOfMonth());
        if($first_record) {
            $start_date = request('start_date', $first_record->created_at->format("Y-m-d"));
        }

        $this->data['start_date'] = Carbon::parse($start_date)->format("Y-m-d");
        $this->data['end_date'] = $this->data['end_date'] = Carbon::parse(request('end_date', now()))->format("Y-m-d");

        $this->data['transactions'] = Order::where(DB::raw("DATE(created_at)"), '>=', $this->data['start_date'])
            ->where(DB::raw("DATE(created_at)"), '<=', $this->data['end_date'])
            ->where(
                function ($query) {
                    if(strlen($this->data['keyword']) > 0) {
                         return $query->whereRaw("LOWER(reference_code) LIKE '{$this->data['keyword']}%'")
                             ->orWhereRaw("LOWER(merchant_code) LIKE '{$this->data['keyword']}%'");

                    }
                }
            )
                                        ->where(
                                            function ($query) {
                                                if(strlen($this->data['status']) > 0) {
                                                     return  $query->where('status', $this->data['status']);
                                                }
                                            }
                                        )
                                        ->where(
                                            function ($query) {
                                                if(in_array($this->data['user']->type, ['user'])) {
                                                     $query->where('user_id', $this->data['user']->id);
                                                }
                                            }
                                        )
                                        ->orderBy('created_at', 'DESC')->paginate($per_page);

        return view('system.transaction.index', $this->data);
    }

    public function show(PageRequest $request, $code = null)
    {
        $this->data['user'] = $request->user($this->guard);

        $this->data['transaction'] = Order::where('reference_code', $code)->first();

        if(!$this->data['transaction']) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Transaction record not found.");
            return redirect()->route('admin.transaction.index');
        }
        return view('system.transaction.show', $this->data);
    }

    public function download(PageRequest $request, $code = null)
    {
        $this->data['user'] = $request->user($this->guard);

        $transaction = Order::where('reference_code', $code)->first();

        if(!$transaction) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Transaction record not found.");
            return redirect()->route('admin.transaction.index');
        }
         
        if($transaction->eor_booklet_qty > 0 AND $transaction->layout) {
            $_series = BookletTransaction::where('order_id', $transaction->id)->get();

            $this->data['transaction'] = $transaction;
            $this->data['series'] = $_series;

            if($transaction->layout == 'vertical') {
                $pdf = PDF::loadView('pdf.vertical', $this->data);
            }
            else {
                $pdf = PDF::loadView('pdf.horizontal', $this->data);
            }
            return $pdf->download("BOOKLET PRINT OUT - [{$transaction->reference_code}].pdf");
        } else {
            session()->flash('notification-status', "warning");
            session()->flash('notification-msg', "No file to download.");
            return redirect()->route('admin.transaction.index');
        }

        return redirect()->route('admin.transaction.index');
    }

    public function create(PageRequest $request)
    {
        $this->data['user'] = $request->user($this->guard);
        $this->data['business'] = ['' => "--Choose Business--"] + Merchant::where('owner_user_id', $this->data['user']->id)->where('status', 'active')->pluck('merchant_name', 'merchant_code')->toArray();

        $this->data['transaction'] = Order::where('user_id', $this->data['user']->id)->where('status', 'COMPLETED')->orderBy('created_at', 'DESC')->first() ?: new Order();
        return view('system.transaction.create', $this->data);
    }

    public function store(TransactionRequest $request)
    {
        $user = $request->user($this->guard);

        $batch_id = BookletTransaction::where('merchant_code', request('merchant_code'))->max('batch_id');
        
        if($batch_id) {
            $used = BookletTransaction::where('merchant_code', request('merchant_code'))->where('batch_id', $batch_id)->where('status', 1)->first();

            if(!$used AND request('eor_booklet_qty') > 0 ) {
                session()->flash('notification-status', "warning");
                session()->flash('notification-msg', "You cannot buy a series again if your last bought is still unused.");
                return redirect()->route('admin.transaction.index');
            }
        }

        try{
            DB::beginTransaction();
            
            $new_transaction = new Order;
            $new_transaction->user_id = $user->id;
            $new_transaction->merchant_code = request('merchant_code');
            $new_transaction->eor_online_qty = request('eor_online_qty');
            $new_transaction->eor_booklet_qty = request('eor_booklet_qty') * 50;
            $new_transaction->eor_online_price = 0.99;
            $new_transaction->eor_online_total = $new_transaction->eor_online_qty * $new_transaction->eor_online_price;
            $new_transaction->eor_booklet_price = 1;
            $new_transaction->eor_booklet_total = $new_transaction->eor_booklet_qty * $new_transaction->eor_booklet_price;
            $new_transaction->amount = $new_transaction->eor_booklet_total + $new_transaction->eor_online_total;
            $new_transaction->status = 'PENDING';
            $new_transaction->payment_status = 'PENDING';
            $new_transaction->layout = request('layout');
            $new_transaction->atp_no = request('atp_no');
            $new_transaction->printer_accreditation = request('printer_accreditation');
            $new_transaction->save();

            $new_transaction->reference_code = 'EOR' . Helper::date_format(now(), 'ym') . '-' . str_pad($new_transaction->id, 6, "0", STR_PAD_LEFT);
            $new_transaction->save();

            $request_body = Helper::digipep_transaction(
                [
                'title' => "eOR Transaction Payment",
                'trans_token' => $new_transaction->reference_code,
                'transaction_type' => "", 
                'amount' => $new_transaction->amount,
                'penalty_fee' => 0,
                'dst_fee' => 0,
                'particular_fee' => $new_transaction->amount,
                'success_url' => route('admin.digipep.success', [$new_transaction->reference_code]),
                'cancel_url' => route('admin.digipep.cancel', [$new_transaction->reference_code]),
                'return_url' => route('admin.transaction.index', [$new_transaction->reference_code]),
                'failed_url' => route('admin.digipep.failed', [$new_transaction->reference_code]),
                'first_name' => $user->firstname,
                'middle_name' => "",
                'last_name' => $user->lastname,
                'contact_number' => "",
                'email' => $user->email
                ]
            );  
            
               // Todo
               // Laravel Http here instead
            $response = Curl::to(env('DIGIPEP_CHECKOUT_URL'))
                ->withHeaders(
                    [
                    "X-token: ".env('DIGIPEP_TOKEN'),
                    "X-secret: ".env("DIGIPEP_SECRET")
                    ]
                )
                ->withData($request_body)
                ->asJson(true)
                ->returnResponseObject()
                ->post();    

            if($response->status == "200") {
                $content = $response->content;

                DB::commit();

                return redirect()->away($content['checkoutUrl']);

            }else{
                DB::rollBack();

                Log::alert("DIGIPEP Request System Error ($new_transaction->reference_code): ", array($response));

                session()->flash('notification-status', "failed");
                session()->flash('notification-msg', "There's an error while connecting to our online payment. Please try again.");
                return redirect()->back();
            }

        }catch(\Exception $e){
            DB::rollBack();
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Server Error. Please try again.".$e->getMessage());
            return redirect()->back();
        }

    }
}