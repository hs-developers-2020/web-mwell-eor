<?php 

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;

/*
 * Models used in this Controller
 */
use App\Models\Office;
use App\Models\Merchant;

/*
 * Requests used in this Controller
 */
use App\Http\Requests\System\OfficeRequest;

use App\Http\Requests\System\PageRequest;
/*
 * App Classes used in this Controller
 */
use Str,DB,Input;

class OfficeController extends Controller
{

    protected $data;
    protected $guard = 'admin';

    public function __construct()
    {
        // parent::__construct();
        // array_merge($this->data, parent::get_data());
        $this->data['merchants'] = ['' => '--Choose Merchant--'] + Merchant::pluck('merchant_name', 'id')->toArray();
    }
    
    public function index(PageRequest $request)
    {
        $per_page = request('per_page', 10);
        $this->data['user'] = $request->user($this->guard);

        $this->data['offices'] = Office::orderBy('created_at', "DESC")->paginate($per_page);
        return view('system.office.index', $this->data);
    }

    public function create(PageRequest $request)
    {
        $this->data['user'] = $request->user($this->guard);
        return view('system.office.create', $this->data);
    }

    public function store(OfficeRequest $request)
    {
        try{
            DB::beginTransaction();
            $office = new Office;

            $office->code = request('code');
            $office->name = Str::upper(request('name'));
            $office->address = request('address');
            $office->merchant_id = request('merchant_id');
            $office->save();

            DB::commit();

            session()->flash('notification-status', "success");
            session()->flash('notification-msg', "New office has been created.");
            return redirect()->route('admin.office.index');

        }catch(\Exception $e){
            DB::rollBack();
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Server Error. Please try again.".$e->getMessage());
            return redirect()->back();
        }

    }

    public function edit(PageRequest $request, $id = null)
    {
        $this->data['user'] = $request->user($this->guard);

        $this->data['office'] = Office::find($id);

        if(!$this->data['office']) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Office record not found.");
            return redirect()->route('admin.office.index');
        }
        return view('system.office.edit', $this->data);
    }

    public function update(OfficeRequest $request, $id = null)
    {
        $this->data['user'] = $request->user($this->guard);
        
        $office = Office::find($id);

        if(!$office) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Office record not found.");
            return redirect()->route('admin.office.index');
        }
        try{
            DB::beginTransaction();
            
            $office->code = request('code');
            $office->name = Str::upper(request('name'));
            $office->address = request('address');
            $office->merchant_id = request('merchant_id');
            $office->save();

            DB::commit();

            session()->flash('notification-status', "success");
            session()->flash('notification-msg', "Office record has been modified.");
            return redirect()->route('admin.office.index');

        }catch(\Exception $e){
            DB::rollBack();
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Server Error. Please try again.".$e->getMessage());
            return redirect()->back();
        }

    }

    public function get_office()
    {
        $offices = Office::where('merchant_id', request('merchant_id'))->get();

        $this->response['offices'] = $offices;
        return response()->json($this->response);
    }
}