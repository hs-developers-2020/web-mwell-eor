<?php 

namespace App\Http\Controllers\System;


use App\Http\Controllers\Controller;
/*
 * Models used in this Controller
 */
use App\Models\User;
use App\Models\UserDocument;
use App\Models\Merchant;

/*
 * Requests used in this Controller
 */
use App\Http\Requests\System\PageRequest;
/*
 * App Classes used in this Controller
 */

use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class AccountController extends Controller
{

    protected $data;
    protected $guard = 'admin';
    public function __construct()
    {
        
    }
    
    public function index(PageRequest $request)
    {
        $per_page = request('per_page', 10);
        $this->data['user'] = $request->user($this->guard);
        $this->data['keyword'] = Str::lower(request('keyword'));

        $first_record = User::orderBy('created_at', 'ASC')->first();
        $start_date = request('start_date', now()->startOfMonth());
        if($first_record) {
            $start_date = request('start_date', $first_record->created_at->format("Y-m-d"));
        }

        $this->data['start_date'] = Carbon::parse($start_date)->format("Y-m-d");
        $this->data['end_date'] = $this->data['end_date'] = Carbon::parse(request('end_date', now()))->format("Y-m-d");

        $this->data['accounts'] = User::keyword($this->data['keyword'])
            ->where('type', 'user')
            ->where(DB::raw("DATE(created_at)"), '>=', $this->data['start_date'])
            ->where(DB::raw("DATE(created_at)"), '<=', $this->data['end_date'])
            ->orderBy('created_at', "DESC")
            ->paginate($per_page);
                                        
        return view('system.account.index', $this->data);
    }

    public function show(PageRequest $request, $id = null)
    {
        $per_page = request('per_page', 10);
        $this->data['user'] = $request->user($this->guard);

        $this->data['account'] = User::find($id);
        $this->data['documents'] = UserDocument::where('user_id', $id)->get();
        $this->data['merchants'] = Merchant::where('owner_user_id', $this->data['account']->id)
            ->orderBy('created_at', "DESC")
            ->paginate($per_page);

        if(!$this->data['account']) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Registrant record not found.");
            return redirect()->route('admin.account.index');
        }
        return view('system.account.show', $this->data);
    }

    public function update_status(PageRequest $request, $id = null)
    {
        $user = User::find($id);

        if(!$user OR ($user AND $user->type == 'super_user')) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Record not found.");
            return redirect()->route('admin.account.index');
        }

        if($user->status == "inactive") {
            $user->status = "active";
            session()->flash('notification-msg', "Record was successfully activated.");
        } else {
            $user->status = "inactive";
            session()->flash('notification-msg', "Record was successfully deactivated.");
        }

        $user->save();

        session()->flash('notification-status', "success");
        return redirect()->route('admin.account.index');
    }
}