<?php 

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;

use App\Models\{Registrant, RegistrantLog};
use App\Models\RegistrantDocument;
use App\Models\{User, PasswordReset};

/*
 * Requests used in this Controller
 */
use App\Http\Requests\System\PageRequest;
use App\Http\Requests\System\{RegisterRequest, ResetPasswordRequest, ForgotPasswordRequest, DocumentRequest};

/*
 * App Classes used in this Controller
 */
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use ImageUploader, FileUploader, Str, Log, Mail, Carbon;
use Illuminate\Support\Facades\Crypt;


class AuthController extends Controller
{

    protected $data;
    protected $guard = 'admin';
    
    public function login(PageRequest $request)
    {
        session()->put('admin_login', "no");
        return view('system.auth.login');
    }

    public function submit_documents(PageRequest $request, $code = null)
    {    
        try{
            $id = Crypt::decryptString($code);

            $registrant = Registrant::find($id);

            if(!$registrant) {
                session()->flash('notification-status', 'warning');
                session()->flash('notification-msg', 'Invalid code.');
                return redirect()->route('admin.login');
            }

            if($registrant->is_activated == 1) {
                session()->flash('notification-status', 'warning');
                session()->flash('notification-msg', 'Account is already activated. No more action is needed.');
                return redirect()->route('admin.login');
            }

            return view('system.auth.document');
        }catch(\Exception $e){
            session()->flash('notification-status', 'warning');
            session()->flash('notification-msg', 'Code is invalid.');
            return redirect()->route('admin.login');    
        }
        
    }

    public function store_documents(DocumentRequest $request, $code = null)
    {
        if(!$request->hasFile('file')) {
            session()->flash('notification-status', 'warning');
            session()->flash('notification-msg', 'You must attach atleast one document.');

            return redirect()->back();
        }

        DB::beginTransaction();
        try{
            $id = Crypt::decryptString($code);

            $registrant = Registrant::find($id);

            if(!$registrant) {
                session()->flash('notification-status', 'warning');
                session()->flash('notification-msg', 'Invalid code.');
                return redirect()->route('admin.login');
            }

            $log = new RegistrantLog;
            $log->registrant_id = $registrant->id;
            $log->user_id = 0;
            $log->remarks = "{$registrant->name} submitted documents. Account request status is changed to pending.";
            $log->save();

            $registrant->status = 'pending';
            $registrant->remarks = null;
            $registrant->save();

            if($request->hasFile('file')) {
                foreach($request->file as $index => $file){
                    $mime_type = $file->getMimeType();
                    $file_type = "image";
                    $asset_type = "images";

                    if($mime_type == "application/pdf") {
                        $asset_type = "files";
                        $file_type = "file";
                    }

                    $document = new RegistrantDocument;
                    $document->registrant_id = $registrant->id;
                    if($file_type == "file") {
                        $attachment = FileUploader::upload($file, "uploads/{$registrant->id}/documents/{$asset_type}");
                    } else {
                        $attachment = ImageUploader::upload($file, "uploads/{$registrant->id}/documents/{$asset_type}");
                    }
                    $document->path = $attachment['path'];
                    $document->directory = $attachment['directory'];
                    $document->filename = $attachment['filename'];
                    $document->source = $attachment['source'];
                    $document->file_type = $file_type;
                    $document->save();
                }
            }

            RegistrantDocument::where('registrant_id', $registrant->id)->where('status', 'declined')->delete();

            DB::commit();

            session()->flash('notification-status', 'success');
            session()->flash('notification-msg', 'Application submitted. You will receive an email once your application has been approved.');

            return redirect()->route('admin.login');
        }catch(\Exception $e){
            DB::rollback();
            
            Log::info("ERROR: ", array($e));

            session()->flash('notification-status', 'failed');
            session()->flash('notification-msg', "Server Error: Code #{$e->getLine()}");

            return redirect()->route('admin.login');        
        }
    }

    public function forgot_password(PageRequest $request)
    {
        return view('system.auth.forgot-password');
    }

    public function forgot_pw(ForgotPasswordRequest $request)
    {
        $user = User::where('email', Str::lower(request('email')))->where('type', 'user')->first();

        if(!$user) {
            session()->flash('notification-status', 'warning');
            session()->flash('notification-msg', 'Email does not exist in our records.');
            return redirect()->route('admin.login');
        }

        $token = Str::random(6);

        PasswordReset::where('email', $user->email)->delete();

        $password_reset = new PasswordReset;
        $password_reset->email = $user->email;
        $password_reset->token = $token;
        $password_reset->created_at = now();
        $password_reset->save();

        $data = ['token' => $token];
        Mail::send(
            'emails.password', $data, function ($message) use ($user) {
                $message->from(env('MAIL_ADDRESS', 'noreply@domain.com'));
                $message->to($user->email);
                $message->subject('Forgot Password [EOR]');
            }
        );

        session()->flash('notification-status', 'success');
        session()->flash('notification-msg', 'Reset password request was successfully sent to your email.');
        return redirect()->route('admin.login');
    }

    public function reset_password($token = null)
    {
        $token = Str::lower($token);
        $password_reset = PasswordReset::whereRaw("LOWER(token) = '{$token}'")->first();

        if(!$password_reset) {
            session()->flash('notification-status', 'warning');
            session()->flash('notification-msg', 'Inavlid token.');
            return redirect()->route('admin.login');
        }    

        return view('system.auth.reset-password', $this->data);
    }

    public function update_password(ResetPasswordRequest $request, $token = null)
    {
        $token = Str::lower($token);

        $password_reset = PasswordReset::whereRaw("LOWER(token) = '{$token}'")->first();

        if(!$password_reset) {
            session()->flash('notification-status', 'warning');
            session()->flash('notification-msg', 'Inavlid token.');
            return redirect()->route('frontend.login');
        }    


        $user = User::where('email', $password_reset->email)->where('type', 'user')->first();
        $user->password = bcrypt(request('password'));
        $user->save();

        PasswordReset::where('token', $token)->delete();

        session()->flash('notification-status', "success");
        session()->flash('notification-msg', "New password successfully stored. Login to the platform using your updated credentials.");
        return redirect()->route('admin.login');
    }

    public function admin_login(PageRequest $request)
    {
        session()->put('admin_login', "yes");
        return view('system.auth.login');
    }

    public function authenticate(PageRequest $request)
    {
        $username = request('username');
        $password = request('password');
        $remember = request('remember_me', 1);

        $field = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        $attempt = Auth::guard($this->guard)->attempt([$field => $username, 'password' => $password], $remember);

        if($attempt) {
            $user = Auth::guard($this->guard)->user();
            
            if($user->status != "active") {
                Auth::guard($this->guard)->logout();
                session()->flash('notification-status', 'warning');
                session()->flash('notification-msg', 'Account is not yet active.');
                goto callback;
            }

            session()->forget('admin_login');

            session()->flash('notification-status', 'success');
            session()->flash('notification-msg', 'Login successfully.');

            if($user->type == 'user') { 
                return redirect()->route('admin.transaction.index');
            } else if($user->type == 'staff') { 
                return redirect()->route('admin.registrant.index');
            } else {
                return redirect()->route('admin.index');
            }
        }

        session()->flash('notification-status', "failed");
        session()->flash('notification-msg', "Invalid account credentials.");

        callback:
        return redirect()->back();
    }

    public function logout()
    {
        $guard = "admin";
        $route = "admin.login";

        Auth::guard($guard)->logout();

        session()->flash('notification-status', 'success');
        session()->flash('notification-msg', 'You are now signed off.');
        return redirect()->route($route);
    }

    public function register(PageRequest $request)
    {
        return view('system.auth.register');
    }

    public function store(RegisterRequest $request)
    {

        if(!$request->hasFile('file')) {
            session()->flash('notification-status', 'warning');
            session()->flash('notification-msg', 'You must attach atleast one document.');

            return redirect()->route('admin.register');
        }

        DB::beginTransaction();
        try{
            $registrant = new Registrant;
            $registrant->firstname = Str::upper(request('firstname'));
            $registrant->lastname = Str::upper(request('lastname'));
            $registrant->email = Str::lower(request('email'));
            $registrant->password = bcrypt(request('password'));
            $registrant->status = 'pending';
            $registrant->save();

            if($request->hasFile('file')) {
                foreach($request->file as $index => $file){
                    $mime_type = $file->getMimeType();
                    $file_type = "image";
                    $asset_type = "images";

                    if($mime_type == "application/pdf") {
                        $asset_type = "files";
                        $file_type = "file";
                    }

                    $document = new RegistrantDocument;
                    $document->registrant_id = $registrant->id;
                    if($file_type == "file") {
                        $attachment = FileUploader::upload($file, "uploads/{$registrant->id}/documents/{$asset_type}");
                    } else {
                        $attachment = ImageUploader::upload($file, "uploads/{$registrant->id}/documents/{$asset_type}");
                    }
                    $document->path = $attachment['path'];
                    $document->directory = $attachment['directory'];
                    $document->filename = $attachment['filename'];
                    $document->source = $attachment['source'];
                    $document->file_type = $file_type;
                    $document->save();
                }
            }

            DB::commit();

            session()->flash('notification-status', 'success');
            session()->flash('notification-msg', 'Application submitted. You will receive an email once your application has been approved.');

            return redirect()->route('admin.login');
        }catch(\Exception $e){
            DB::rollback();
            
            Log::info("ERROR: ", array($e));

            session()->flash('notification-status', 'failed');
            session()->flash('notification-msg', "Server Error: Code #{$e->getLine()}");

            return redirect()->route('admin.login');        
        }
    }
}