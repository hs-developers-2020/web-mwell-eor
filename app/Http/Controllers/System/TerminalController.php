<?php 

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
/*
 * Models used in this Controller
 */
use App\Models\Merchant;
use App\Models\Terminal;
use App\Models\Office;

/*
 * Requests used in this Controller
 */
use App\Http\Requests\System\TerminalRequest;

use App\Http\Requests\System\PageRequest;
/*
 * App Classes used in this Controller
 */
use Illuminate\Support\Facades\DB;

class TerminalController extends Controller
{

    protected $data;
    protected $guard = 'admin';

    public function __construct()
    {
        // parent::__construct();
        // array_merge($this->data, parent::get_data());
        $this->data['merchants'] = ['' => '--Choose Merchant--'] + Merchant::pluck('merchant_name', 'id')->toArray();
        $this->data['offices'] = ['' => "--Choose Office--"] + Office::pluck('name', 'code')->toArray();
    }
    
    public function index(PageRequest $request)
    {
        $per_page = request('per_page', 10);
        $this->data['user'] = $request->user($this->guard);

        $this->data['terminals'] = Terminal::orderBy('created_at', "DESC")->paginate($per_page);
        return view('system.terminal.index', $this->data);
    }

    public function create(PageRequest $request)
    {
        $this->data['user'] = $request->user($this->guard);
        
        $total = Terminal::count();
        $this->data['terminal_number'] = str_pad(++$total, 5, "0", STR_PAD_LEFT);
        return view('system.terminal.create', $this->data);
    }

    public function store(TerminalRequest $request)
    {
        try{
            DB::beginTransaction();
            $total = Terminal::count();

            $terminal = new Terminal;

            $terminal->serial_code = mt_rand(10000000, 99999999);
            $terminal->device_name = request('device_name');
            $terminal->merchant_id = request('merchant_id');
            $terminal->terminal_number = str_pad(++$total, 5, "0", STR_PAD_LEFT);
            $terminal->office_code = request('office_code');
            $terminal->device_serial = request('device_serial');
            $terminal->save();

            DB::commit();

            session()->flash('notification-status', "success");
            session()->flash('notification-msg', "New terminal has been created.");
            return redirect()->route('admin.terminal.index');

        }catch(\Exception $e){
            DB::rollBack();
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Server Error. Please try again.".$e->getMessage());
            return redirect()->back();
        }

    }

    public function edit(PageRequest $request, $id = null)
    {
        $this->data['user'] = $request->user($this->guard);

        $this->data['terminal'] = Terminal::find($id);

        if(!$this->data['terminal']) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Terminal record not found.");
            return redirect()->route('admin.terminal.index');
        }
        return view('system.terminal.edit', $this->data);
    }

    public function update(TerminalRequest $request, $id = null)
    {
        $this->data['user'] = $request->user($this->guard);

        $terminal = Terminal::find($id);

        if (!$terminal) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Terminal record not found.");
            return redirect()->route('admin.terminal.index');
        }
        try {
            DB::beginTransaction();
            
            // $terminal->serial_code = request('serial_code');
            $terminal->device_name = request('device_name');
            $terminal->merchant_id = request('merchant_id');
            $terminal->office_code = request('office_code');
            $terminal->device_serial = request('device_serial');
            $terminal->save();

            DB::commit();

            session()->flash('notification-status', "success");
            session()->flash('notification-msg', "Terminal record has been modified.");
            return redirect()->route('admin.terminal.index');

        } catch(\Exception $e) {
            DB::rollBack();
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Server Error. Please try again.".$e->getMessage());
            return redirect()->back();
        }

    }
}