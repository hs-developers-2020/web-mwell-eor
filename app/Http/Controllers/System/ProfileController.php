<?php 

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;

/*
 * Models used in this Controller
 */

/*
 * Requests used in this Controller
 */
use App\Http\Requests\System\ProfilePasswordRequest;
use App\Http\Requests\System\PageRequest;
/*
 * App Classes used in this Controller
 */
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{

    protected $data;
    protected $guard = 'admin';

    public function __construct()
    {
        // parent::__construct();
        // array_merge($this->data, parent::get_data());
    }
    
    public function edit_password(PageRequest $request)
    {
        $this->data['user'] = $request->user($this->guard);
        
        return view('system.profile.edit-password', $this->data);
    }

    public function update_password(ProfilePasswordRequest $request)
    {
        $account = $request->user($this->guard);
        try{
            DB::beginTransaction();
            $account->password = bcrypt(request('password'));
            $account->save();

            DB::commit();

            session()->flash('notification-status', "success");
            session()->flash('notification-msg', "New password has been configured.");
            return redirect()->route('admin.index');

        }catch(\Exception $e){
            DB::rollBack();
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Server Error. Please try again.".$e->getMessage());
            return redirect()->back();
        }

    }
}