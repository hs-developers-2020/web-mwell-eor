<?php 

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;

/*
 * Models used in this Controller
 */
use App\Models\Merchant;
use App\Models\Section;
use App\Models\Division;
use App\Models\User;

/*
 * Requests used in this Controller
 */
use App\Http\Requests\System\MerchantRequest;
use App\Http\Requests\System\MerchantSettingRequest;
use App\Http\Requests\System\PageRequest;
/*
 * App Classes used in this Controller
 */
use App\Services\Helper;
use Str,Carbon,DB,PDF,Auth,Input;

class MerchantController extends Controller
{

    protected $data;
    protected $guard = 'admin';

    public function __construct()
    {
        // parent::__construct();
        // array_merge($this->data, parent::get_data());
        $this->data['sections'] = ['' => "--Choose Section--"] + Section::pluck('name', 'code')->toArray();
        $this->data['divisions'] = ['' => "--Choose Division--"] + Division::pluck('name', 'code')->toArray();
        $this->data['types'] = ['' => "--Choose Type--", 'govt' => 'Government', 'commercial' => 'Commercial'];
        $this->data['owners'] = ['' => "--Choose Owner--"] + User::where('type', 'user')->select(DB::raw("CONCAT(firstname,' ',lastname, ' [', email, ']') AS full_name"), "id")->pluck('full_name', 'id')->toArray();
    }
    
    public function index(PageRequest $request)
    {
        
        $per_page = request('per_page', 10);
        $this->data['user'] = $request->user($this->guard);
        $this->data['keyword'] = Str::lower(request('keyword'));

        $first_record = Merchant::orderBy('created_at', 'ASC')->first();
        $start_date = request('start_date', now()->startOfMonth());
        if($first_record) {
            $start_date = request('start_date', $first_record->created_at->format("Y-m-d"));
        }

        $this->data['start_date'] = Carbon::parse($start_date)->format("Y-m-d");
        $this->data['end_date'] = $this->data['end_date'] = Carbon::parse(request('end_date', now()))->format("Y-m-d");

        $this->data['merchants'] = Merchant::where(
            function ($query) {
                if(strlen($this->data['keyword']) > 0) {
                    return $query->whereRaw("LOWER(merchant_name) LIKE '%{$this->data['keyword']}%'")
                        ->orWhereRaw("LOWER(merchant_code) LIKE '%{$this->data['keyword']}%'")
                        ->orWhereRaw("LOWER(merchant_type) LIKE '%{$this->data['keyword']}%'")
                        ->orWhereIn(
                            'section_code', function ($query) {
                                $query->select('code')
                                    ->whereRaw("LOWER(name)  LIKE  '%{$this->data['keyword']}%'")
                                    ->from('section');
                            }
                        )->orWhereIn(
                            'division_code', function ($query) {
                                $query->select('code')
                                    ->whereRaw("LOWER(name)  LIKE  '%{$this->data['keyword']}%'")
                                    ->from('division');
                            }
                        );
                }
            }
        )
                                            ->whereDate('created_at', '>=', Helper::date_db($this->data['start_date']))
                                            ->whereDate('created_at', '<=', Helper::date_db($this->data['end_date']))
                                            ->orderBy('created_at', "DESC")
                                            ->paginate($per_page);

        if($this->data['user']->type == 'staff') {
            $this->data['merchants'] = Merchant::where('merchant_type', 'commercial')
                ->where(
                    function ($query) {
                        if(strlen($this->data['keyword']) > 0) {
                            return $query->whereRaw("LOWER(merchant_name) LIKE '%{$this->data['keyword']}%'")
                                ->orWhereRaw("LOWER(merchant_code) LIKE '%{$this->data['keyword']}%'")
                                ->orWhereIn(
                                    'section_code', function ($query) {
                                        $query->select('code')
                                            ->whereRaw("LOWER(name)  LIKE  '%{$this->data['keyword']}%'")
                                            ->from('section');
                                    }
                                )->orWhereIn(
                                    'division_code', function ($query) {
                                        $query->select('code')
                                            ->whereRaw("LOWER(name)  LIKE  '%{$this->data['keyword']}%'")
                                            ->from('division');
                                    }
                                );
                        }
                    }
                )
               ->whereDate('created_at', '>=', Helper::date_db($this->data['start_date']))
               ->whereDate('created_at', '<=', Helper::date_db($this->data['end_date']))
               ->orderBy('created_at', "DESC")
               ->paginate($per_page);
        }
        return view('system.merchant.index', $this->data);
    }

    public function create(PageRequest $request)
    {
        $this->data['user'] = $request->user($this->guard);
        
        if(!in_array($this->data['user']->type, ['super_user', 'admin'])) {
            session()->flash('notification-status', "warning");
            session()->flash('notification-msg', "You are not authorized to do this action.");
            return redirect()->route('admin.merchant.index');
        }

        return view('system.merchant.create', $this->data);
    }

    public function store(MerchantRequest $request)
    {
        try{
            DB::beginTransaction();
            $merchant = new Merchant;

            $merchant->section_code = request('section_code');
            $merchant->division_code = request('division_code');
            $merchant->owner_user_id = request('owner_user_id');
            $merchant->merchant_name = request('merchant_name');
            $merchant->merchant_code = strtoupper(request('merchant_code'));
            $merchant->submerchant_code = strtoupper(request('submerchant_code'));
            $merchant->submerchant_name = request('submerchant_name');
            $merchant->merchant_type = request('merchant_type');
            $merchant->address = request('address');
            $merchant->tin = request('tin');
            $merchant->tel_no = request('tel_no');
            $merchant->save();

            DB::commit();

            session()->flash('notification-status', "success");
            session()->flash('notification-msg', "New merchant has been created.");
            return redirect()->route('admin.merchant.index');

        }catch(\Exception $e){
            DB::rollBack();
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Server Error. Please try again.".$e->getMessage());
            return redirect()->back();
        }

    }

    public function edit(PageRequest $request, $id = null)
    {
        $this->data['user'] = $request->user($this->guard);

        $this->data['merchant'] = Merchant::find($id);

        if(!$this->data['merchant']) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Merchant record not found.");
            return redirect()->route('admin.merchant.index');
        }
        return view('system.merchant.edit', $this->data);
    }

    public function update(MerchantRequest $request, $id = null)
    {
        $this->data['user'] = $request->user($this->guard);

        $merchant = Merchant::find($id);

        if(!$merchant) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Merchant record not found.");
            return redirect()->route('admin.merchant.index');
        }
        try{
            DB::beginTransaction();
            
            $merchant->section_code = request('section_code');
            $merchant->division_code = request('division_code');
            $merchant->owner_user_id = request('owner_user_id');
            $merchant->merchant_name = request('merchant_name');
            $merchant->merchant_code = request('merchant_code');
            $merchant->submerchant_code = request('submerchant_code');
            $merchant->submerchant_name = request('submerchant_name');
            $merchant->merchant_type = request('merchant_type');
            $merchant->address = request('address');
            $merchant->tin = request('tin');
            $merchant->tel_no = request('tel_no');
            $merchant->max_online = request('max_online');
            $merchant->max_booklet = request('max_booklet');
            $merchant->save();

            DB::commit();

            session()->flash('notification-status', "success");
            session()->flash('notification-msg', "Merchant record has been modified.");
            return redirect()->route('admin.merchant.index');

        }catch(\Exception $e){
            DB::rollBack();
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Server Error. Please try again.".$e->getMessage());
            return redirect()->back();
        }

    }

    public function get_division()
    {
        $divisions = Division::where('section_code', request('code'))->get();

        $this->response['divisions'] = $divisions;
        return response()->json($this->response);
    }

    public function edit_details(PageRequest $request, $id = null)
    {
        $this->data['user'] = $request->user($this->guard);

        $this->data['merchant'] = Merchant::where('merchant_code', $id)->first();

        if(!$this->data['merchant']) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Merchant record not found.");
            return redirect()->route('admin.merchant.index');
        }
        return view('system.merchant.setting', $this->data);
    }

    public function update_details(MerchantSettingRequest $request, $id = null)
    {
        $this->data['user'] = $request->user($this->guard);

        $merchant = Merchant::where('merchant_code', $id)->first();

        if(!$merchant) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Merchant record not found.");
            return redirect()->route('admin.merchant.index');
        }
        try{
            DB::beginTransaction();
            
            $merchant->max_online = request('max_online');
            $merchant->max_booklet = request('max_booklet');
            $merchant->save();

            DB::commit();

            session()->flash('notification-status', "success");
            session()->flash('notification-msg', "Merchant record has been modified.");
            return redirect()->route('admin.merchant.index');

        }catch(\Exception $e){
            DB::rollBack();
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Server Error. Please try again.".$e->getMessage());
            return redirect()->back();
        }

    }
}