<?php 

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
/*
*
* Models used for this controller
*/
use App\Models\Order;
use App\Models\BookletTransaction;
use App\Models\Merchant;

/*
*
* Requests used for validating inputs
*/

use App\Http\Requests\System\PageRequest;
// use App\Events\SendEmail;

/*
*
* Classes used for this controller
*/
use App\Services\Helper;
use Illuminate\Support\Carbon;
use Str, DB, Log;

class DigipepController  extends Controller
{

    /*
    *
    * @var Array $data
    */
    protected $data;

    public function __construct()
    {
        $this->data = [];
    }

    public function success(PageRequest $request,$code = null)
    {
        Log::info("Digipep Success", array($request->all()));
        $response = json_decode(json_encode($request->all()));
        if(isset($response->referenceCode)) {
            $transaction = Order::where('reference_code', Str::upper($response->referenceCode))->first();

            if(!$transaction) {
                Log::info("Digipep Record not found : {$response->referenceCode}");
                goto end;
            }

            if(isset($response->payment) AND Str::upper($response->payment->status) == "PAID") {
                // $payment = $response->payment;
            
                DB::beginTransaction();
                try{
                    $transaction->payment_reference = $response->transactionCode;
                    $transaction->payment_method  = $response->payment->paymentMethod;
                    $transaction->payment_type  = $response->payment->paymentType;

                    $transaction->payment_option  = "DIGIPEP";

                    $transaction->payment_date =  now();
                    $transaction->payment_status  = "PAID";
                    $transaction->status  = "COMPLETED";

                    $convenience_fee = $response->payment->convenienceFee;
                    $transaction->convenience_fee = $convenience_fee; 
                    $transaction->total_amount = $transaction->amount + $convenience_fee;
                    $transaction->save();

                    //create series if booklet transaction
                    $total_count = BookletTransaction::where('merchant_code', $transaction->merchant_code)->count();
                    $merchant = Merchant::where('merchant_code', $transaction->merchant_code)->first();

                    $batch_id = BookletTransaction::where('merchant_code', $transaction->merchant_code)->max('batch_id');
                    if($batch_id) { ++$batch_id;
                    }
                    if(is_null($batch_id)) { $batch_id = 1;
                    }

                    for ($i=0; $i < $transaction->eor_booklet_qty; $i++) { 
                        $new_series = new BookletTransaction;
                        $new_series->batch_id = $batch_id;
                        $new_series->order_id = $transaction->id;
                        $new_series->user_id = $transaction->user_id;
                        $new_series->merchant_code = $transaction->merchant_code;
                        $new_series->merchant_name = $merchant ? $merchant->merchant_name : '';
                        $new_series->or_code = $merchant->section_code . $merchant->division_code . '-' . $merchant->merchant_code . '-' . Helper::date_format(now(), 'ym') . '-'. str_random(4);
                        $new_series->series_code = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZ', mt_rand(1, 10))), 1, 3) . str_pad(++$total_count, 9, "0", STR_PAD_LEFT);
                        $new_series->status = 0;
                        $new_series->save();
                    }

                    DB::commit();

                }catch(\Exception $e){
                    DB::rollBack();
                    Log::info("ERROR", array($e));
                    Log::alert("Digipep Error : "."Server Error. Please try again.".$e->getLine());
                }
            }

            
        }

        end:
    }

    public function failed(PageRequest $request,$code = null)
    {
        Log::info("Digipep Failed", array($request->all()));
        $response = json_decode(json_encode($request->all()));
        if(isset($response->referenceCode)) {
            $transaction = Order::where('reference_code', Str::upper($response->referenceCode))->first();

            if(!$transaction) {
                Log::info("Digipep Record not found : {$response->referenceCode}");
                goto end;
            }

            if(isset($response->payment) AND Str::upper($response->payment->status) == "FAILED") {
            
                DB::beginTransaction();
                try{
                    $transaction->payment_reference = $response->transactionCode;
                    $transaction->payment_method  = $response->payment->paymentMethod;
                    $transaction->payment_type  = $response->payment->paymentType;

                    $transaction->payment_option  = "DIGIPEP";

                    $transaction->payment_date = now();
                    $transaction->status  = "FAILED";
                    $transaction->payment_status = "FAILED";

                    $convenience_fee = $response->payment->convenienceFee;
                    $transaction->convenience_fee = $convenience_fee; 
                    $transaction->total_amount = $transaction->amount + $convenience_fee;
                    $transaction->save();
                    DB::commit();

                }catch(\Exception $e){
                    DB::rollBack();
                    Log::info("ERROR", array($e));
                    Log::alert("Digipep Error : "."Server Error. Please try again.".$e->getLine());
                }
            }
        }

        end:
    }

    public function cancel(PageRequest $request,$code = null)
    {
        // dd($request->all());
        Log::info("Digipep Cancel", array($request->all()));

        $transaction = Order::whereRaw("LOWER(reference_code) = '{$code}'")->first();
        $transaction->status  = "CANCELLED";
        $transaction->payment_status  = "CANCELLED";
        $transaction->save();

        session()->forget('transaction');
        session()->flash('notification-status', "failed");
        session()->flash('notification-msg', "Cancelled transaction.");
        return redirect()->route('admin.transaction.index');

    }

}