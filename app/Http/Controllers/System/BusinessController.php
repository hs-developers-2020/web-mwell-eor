<?php 

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;

/*
 * Models used in this Controller
 */
use App\Models\Merchant;
use App\Models\TransactionHeader;
use App\Models\BookletTransaction;
use App\Models\Section;
use App\Models\Division;

/*
 * Requests used in this Controller
 */
use App\Http\Requests\System\PageRequest;
use App\Http\Requests\System\BusinessRequest;

/*
 * App Classes used in this Controller
 */
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class BusinessController extends Controller
{

    protected $data;
    protected $guard = 'admin';

    public function __construct()
    {
        $this->data['sections'] = ['' => "--Choose Section--"] + Section::pluck('name', 'code')->toArray();
        $this->data['divisions'] = ['' => "--Choose Division--"] + Division::pluck('name', 'code')->toArray();
    }
    
    public function index(PageRequest $request)
    {
        $per_page = request('per_page', 10);
        $this->data['user'] = $request->user($this->guard);

        $this->data['merchants'] = Merchant::where('owner_user_id', $this->data['user']->id)
            ->orderBy('created_at', "DESC")
            ->paginate($per_page);
        return view('system.business.index', $this->data);
    }

    public function show(PageRequest $request, $code = null)
    {
        $per_page = request('per_page', 10);
        $this->data['user'] = $request->user($this->guard);

        $this->data['merchant'] = Merchant::where('merchant_code', $code)->first();

        if(!$this->data['merchant']) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Business record not found.");
            return redirect()->route('admin.business.index');
        }

        $this->data['transactions'] = TransactionHeader::where('merchant_code', $code)->take(5)->get();
        $this->data['booklet_transaction'] = BookletTransaction::where('merchant_code', $code)->take(5)->get();

        return view('system.business.show', $this->data);
    }

    public function create(PageRequest $request)
    {
        $this->data['user'] = $request->user($this->guard);
        
        return view('system.business.create', $this->data);
    }

    public function store(BusinessRequest $request)
    {
        $user = $request->user($this->guard);
        try{
            DB::beginTransaction();
            $merchant = new Merchant;

            $merchant->section_code = request('section_code');
            $merchant->division_code = request('division_code');
            $merchant->owner_user_id = $user->id;
            $merchant->merchant_name = request('merchant_name');
            $merchant->merchant_code = Str::slug($merchant->merchant_name);
            $merchant->merchant_type = 'commercial';
            $merchant->address = request('address');
            $merchant->tin = request('tin');
            $merchant->tel_no = request('tel_no');
            $merchant->save();

            DB::commit();

            session()->flash('notification-status', "success");
            session()->flash('notification-msg', "New business has been created.");
            return redirect()->route('admin.business.index');

        }catch(\Exception $e){
            DB::rollBack();
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Server Error. Please try again.".$e->getMessage());
            return redirect()->back();
        }

    }

    public function update_status(PageRequest $request, $code = null)
    {
        $merchant = Merchant::where('merchant_code', $code)->first();

        if(!$merchant) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Business record not found.");
            return redirect()->route('admin.business.index');
        }

        if($merchant->status == 'active') {
            $merchant->status = 'inactive';
            goto callback;
        }

        $merchant->status = 'active';

        callback:
        $merchant->save();

        session()->flash('notification-status', "success");
        session()->flash('notification-msg', "Business was successfully" . ($merchant->status == "active" ? " activated." : " deactivated."));
        return redirect()->route('admin.business.index');
    }
}