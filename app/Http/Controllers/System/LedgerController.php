<?php 

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
/*
 * Models used in this Controller
 */
use App\Models\TransactionHeader;
use App\Models\BookletTransaction;
use App\Models\Merchant;
use App\Models\EorBundle;

/*
 * Requests used in this Controller
 */
use App\Http\Requests\System\PageRequest;
use App\Http\Requests\System\UpdateSeriesRequest;

/*
 * App Classes used in this Controller
 */
use App\Services\Helper;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\PDF;
use Excel;

class LedgerController extends Controller
{

    protected $data;
    protected $guard = 'admin';
    public function __construct()
    {
        $this->data['statuses'] = [
            '' => "--Choose Status--", 
            '' => 'All',
            'available' => 'Available',
            'not_available' => 'Not Available'
        ];
    }
    
    public function index(PageRequest $request)
    {
        $per_page = request('per_page', 10);
        $this->data['user'] = $request->user($this->guard);

        $this->data['keyword'] = Str::lower(request('keyword'));
        $this->data['status'] = request('status');
        $this->data['start_date'] = request('start_date', now()->format('m/d/Y'));
        $this->data['end_date'] = request('end_date', now()->format('m/d/Y'));

        $this->data['transactions'] = TransactionHeader::whereIn(
            'merchant_code', function ($query) {
                                    return $query->select('merchant_code')
                                        ->where('owner_user_id', $this->data['user']->id)
                                        ->whereRaw('deleted_at IS NULL')
                                        ->from('merchant');
            }
        )
                                ->where(
                                    function ($query) {
                                        return $query->where(DB::raw("DATE(created_at)"), '>=', Helper::date_db($this->data['start_date']))->where(DB::raw("DATE(created_at)"), '<=', Helper::date_db($this->data['end_date']));
                                    }
                                )
                                ->where(
                                    function ($query) {
                                        if(strlen($this->data['keyword']) > 0) {
                                            return $query->whereRaw("LOWER(or_code) LIKE '{$this->data['keyword']}%'")
                                                ->orWhereRaw("LOWER(merchant_code) LIKE '{$this->data['keyword']}%'")
                                                ->orWhereRaw("LOWER(merchant_name) LIKE '{$this->data['keyword']}%'");

                                        }
                                    }
                                )
                                ->orderBy('created_at', 'DESC')->paginate($per_page);

        return view('system.ledger.index', $this->data);
    }

    public function show(PageRequest $request, $code = null)
    {
        $this->data['user'] = $request->user($this->guard);

        $this->data['transaction'] = TransactionHeader::where('or', $code)->first();

        if(!$this->data['transaction']) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Transaction record not found.");
            return redirect()->route('admin.ledger.index');
        }
        return view('system.ledger.show', $this->data);
    }

    public function print(PageRequest $request, $code = null)
    {
        $this->data['user'] = $request->user($this->guard);

        $this->data['transaction'] = EorBundle::where('bundle_id', $code)->first();

        if(!$this->data['transaction']) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Transaction record not found.");
            return redirect()->route('admin.ledger.index');
        }

        $this->data['flag'] = true;
        switch (count($this->data['transaction']->items)) {
        case 1:
            $this->data['flag'] = false;
        case 2:
            if($this->data['transaction']->dst_fee || $this->data['transaction']->dst_fee > 0) {
                $this->data['flag'] = false;
            }
            $this->data['index'] = 1;
            break;
        default:
            $this->data['flag'] = true;
            $this->data['index'] = 2;
            break;
        }


        $pdf = PDF::loadView('pdf.receipt', $this->data);
        return $pdf->stream("RECEIPT.pdf");
    }

    public function online(PageRequest $request, $code = null)
    {
        $per_page = request('per_page', 10);
        $this->data['user'] = $request->user($this->guard);

        $this->data['merchant'] = Merchant::where('merchant_code', $code)->first();

        if(!$this->data['merchant']) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Business record not found.");
            return redirect()->route('admin.business.index');
        }

        $this->data['transactions'] = TransactionHeader::where('merchant_code', $code)->paginate($per_page);

        return view('system.ledger.eor', $this->data);
    }

    public function booklet(PageRequest $request, $code = null)
    {
        $per_page = request('per_page', 10);
        $this->data['user'] = $request->user($this->guard);

        $this->data['keyword'] = request('keyword', null);
        $this->data['status'] = request('status', null);

        $this->data['merchant'] = Merchant::where('merchant_code', $code)->first();

        if(!$this->data['merchant']) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Business record not found.");
            return redirect()->route('admin.business.index');
        }

        $this->data['transactions'] = BookletTransaction::where('merchant_code', $code)
            ->where(
                function ($query) {
                    if(strlen($this->data['keyword']) > 0) {
                         return $query->whereRaw("series_code LIKE  UPPER('%{$this->data['keyword']}%')");
                    }
                }
            )
                                                        ->where(
                                                            function ($query) {
                                                                if(strlen($this->data['status']) > 0) {
                                                                     $status = $this->data['status'] == 'available' ? 0 : 1;
                                                                     return $query->where('status', $status);
                                                                }
                                                            }
                                                        )
                                                        ->paginate($per_page);

        return view('system.ledger.booklet', $this->data);
    }

    public function edit(PageRequest $request, $merchant_code = null, $code = null)
    {
        $per_page = request('per_page', 10);
        $this->data['user'] = $request->user($this->guard);

        $this->data['series'] = BookletTransaction::where('series_code', $code)->where('merchant_code', $merchant_code)->first();

        if(!$this->data['series']) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Record not found.");
            return redirect()->route('admin.ledger.booklet', [$merchant_code]);
        }

        return view('system.ledger.edit', $this->data);
    }

    public function update(UpdateSeriesRequest $request, $merchant_code = null, $code = null)
    {
        $per_page = request('per_page', 10);
        $this->data['user'] = $request->user($this->guard);

        $batch_id = BookletTransaction::where('merchant_code', $merchant_code)->max('batch_id');

        $series = BookletTransaction::where('series_code', $code)->where('merchant_code', $merchant_code)->first();

        if($batch_id > $series->batch_id AND $series->amount) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "You cannot modify this record anymore.");
            return redirect()->route('admin.ledger.booklet', [$merchant_code]);
        }

        if(!$series) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Record not found.");
            return redirect()->route('admin.ledger.booklet', [$merchant_code]);
        }

        $series->transaction_date = Carbon::parse(request('transaction_date'));
        $series->date_expiry = Carbon::parse(request('transaction_date'))->addYears(5);
        $series->amount = request('amount');
        // $series->or_code = request('or_code');
        $series->name = request('name');
        $series->tin = request('tin');
        $series->status = 1;
        $series->save();

        session()->flash('notification-status', "success");
        session()->flash('notification-msg', "Record was successfully updated.");
        return redirect()->route('admin.ledger.booklet', [$merchant_code, 'status' => 'available']);
    }

    public function export(PageRequest $request)
    {
        $report_type = request('report_type');
        $type = request('type');
        $code = request('code');

        $this->data['keyword'] = request('keyword', null);
        $this->data['status'] = request('status', null);

        switch ($type) {
        case 'online':
            $this->data['transactions'] = TransactionHeader::where('merchant_code', $code)->get();
            break;
        default:
            $this->data['transactions'] = BookletTransaction::where('merchant_code', $code)
                ->where(
                    function ($query) {
                        if(strlen($this->data['keyword']) > 0) {
                                 return $query->whereRaw("series_code LIKE  UPPER('%{$this->data['keyword']}%')");
                        }
                    }
                )
            ->where(
                function ($query) {
                    if(strlen($this->data['status']) > 0) {
                             $status = $this->data['status'] == 'available' ? 0 : 1;
                             return $query->where('status', $status);
                    }
                }
            )
            ->get();
            break;
        }

        switch ($report_type) {
        case 'excel':
            Excel::create(
                "Exported Data - ".now()->format("m-d-Y h:i:s A"), function ($excel) use ($type) {
                    $excel->setTitle("Exported Data".now()->format("m-d-Y h:i:s A"))
                        ->setCreator("Highly Succeed")
                        ->setCompany('PMTI');
                    $excel->sheet(
                        "SUMMARY", function ($sheet) use ($type) {
                            $sheet->setFontFamily('Calibri Light');

                            $sheet->setColumnFormat(
                                array(
                                'A' => '0000'
                                )
                            );

                            $sheet->row(1, ['OR', 'ISSUED DATE', 'NAME', 'TIN', 'AMOUNT']);
                            $i = 2;
                            foreach($this->data['transactions'] as $index => $value){
                                $sheet->row($i++, [$value->or_code, $type == 'online' ? Helper::date_format($value->created_at, 'M. d, Y') : ($value->transaction_date ? Helper::date_format($value->transaction_date, 'M. d, Y') : ''), $type == 'online' ? $value->bundle->name : $value->name, $type == 'online' ? $value->bundle->tin : $value->tin, $value->amount]);
                            }
                        }
                    );
                }
            )->export('xls');
            break;
        default:
            return redirect()->back();
                break;
        }
    }
}