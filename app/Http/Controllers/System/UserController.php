<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;

/*
 * Models used in this Controller
 */
use App\Models\User;

/*
 * Requests used in this Controller
 */
use App\Http\Requests\System\UserRequest;

use App\Http\Requests\System\PageRequest;
/*
 * App Classes used in this Controller
 */
use App\Services\Helper;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Exception;


class UserController extends Controller
{
    protected $data;
    protected $guard = 'admin';
    public function __construct()
    {
        // parent::__construct();
        // array_merge($this->data, parent::get_data());
        $this->data['type'] = ['' => "--Choose Type--",'admin' => "Admin",'staff' => "BIR Staff"];
    }
    
    public function index(PageRequest $request)
    {
        $per_page = request('per_page', 10);
        $this->data['user'] = $request->user($this->guard);
        $this->data['start_date'] = request('start_date', now()->subMonth()->format('m/d/Y'));
        $this->data['end_date'] = request('end_date', now()->format('m/d/Y'));
        $this->data['keyword'] = Str::lower(request('keyword'));

        $this->data['users'] = User::whereIn('type', ['staff', 'admin'])
            ->keyword($this->data['keyword'])
            ->whereDate('created_at', '>=', Helper::date_db($this->data['start_date']))
            ->whereDate('created_at', '<=', Helper::date_db($this->data['end_date']))
            ->orderBy('created_at', "DESC")
            ->paginate($per_page);

        return view('system.user.index', $this->data);
    }

    public function create(PageRequest $request)
    {
        $this->data['user'] = $request->user($this->guard);
        return view('system.user.create', $this->data);
    }

    public function store(UserRequest $request)
    {
        try {
            $new_user = new User;
            $new_user->firstname = Str::upper(request('firstname'));
            $new_user->lastname = Str::upper(request('lastname'));
            $new_user->email = Str::lower(request('email'));
            $new_user->name = $new_user->firstname . ' ' . $new_user->lastname;
            $new_user->type = request('type');
            $new_user->status = "active";
            $new_user->password = bcrypt(request('password'));
            if($new_user->save()) {
                session()->flash('notification-status', 'success');
                session()->flash('notification-msg', "New account has been added.");
                return redirect()->route('admin.user.index');
            }
            session()->flash('notification-status', 'failed');
            session()->flash('notification-msg', 'Something went wrong.');

            return redirect()->back();
        } catch (Exception $e) {
            session()->flash('notification-status', 'failed');
            session()->flash('notification-msg', $e->getMessage());
            return redirect()->back();
        }
    }

    public function edit(PageRequest $request, $id = null)
    {
        $this->data['user'] = $request->user($this->guard);
        $this->data['admin'] = User::whereIn('type', ['staff', 'admin'])->find($id);

        if(!$this->data['admin']) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Record not found.");
            return redirect()->route('admin.user.index');
        }
        return view('system.user.edit', $this->data);
    }

    public function update(UserRequest $request, $id = null)
    {
        try {
            $user = User::whereIn('type', ['staff', 'admin'])->find($id);

            if (!$user) {
                session()->flash('notification-status', "failed");
                session()->flash('notification-msg', "Record not found.");
                return redirect()->route('system.user.index');
            }

            $user->firstname = Str::upper(request('firstname'));
            $user->lastname = Str::upper(request('lastname'));
            $user->name = $user->firstname . ' ' . $user->lastname;
            $user->email = Str::lower(request('email'));
            $user->type = request('type');
            
            if($request->filled('password')) {
                $user->password = bcrypt(request('password'));
            }

            if($user->save()) {
                session()->flash('notification-status', 'success');
                session()->flash('notification-msg', "Account has been modified successfully.");
                return redirect()->route('admin.user.index');
            }

            session()->flash('notification-status', 'failed');
            session()->flash('notification-msg', 'Something went wrong.');

        } catch (Exception $e) {
            session()->flash('notification-status', 'failed');
            session()->flash('notification-msg', $e->getMessage());
            return redirect()->back();
        }
    }

    public function update_status(PageRequest $request, $id = null)
    {
        $this->data['user'] = $request->user($this->guard);

        $admin = User::whereIn('type', ['staff', 'admin'])->find($id);

        if(!$admin) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Doctor record not found.");
            return redirect()->route('admin.user.index');
        }

        if($admin->status == 'active') {
            $admin->status = 'inactive';
            $admin->save();

            session()->flash('notification-status', "success");
            session()->flash('notification-msg', "Doctor account was successfully deactivated.");
            return redirect()->route('admin.user.index');
        }

        $admin->status = 'active';
        $admin->save();

        session()->flash('notification-status', "success");
        session()->flash('notification-msg', "Doctor account was successfully activated.");
        return redirect()->route('admin.user.index');
    }
}
