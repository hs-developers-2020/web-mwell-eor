<?php 

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;

/*
 * Models used in this Controller
 */
use App\Models\Setting;

/*
 * Requests used in this Controller
 */
use App\Http\Requests\System\PageRequest;
use App\Http\Requests\System\SettingRequest;

/*
 * App Classes used in this Controller
 */

class SettingController extends Controller
{

    protected $data;
    protected $guard = 'admin';
    public function __construct()
    {
        // parent::__construct();
        // array_merge($this->data, parent::get_data());
    }

    public function index(PageRequest $request)
    {
        $this->data['user'] = $request->user($this->guard);

        $this->data['settings'] = Setting::orderBy('created_at', 'DESC')->take(1)->get();
        return view('system.setting.index', $this->data);
    }

    public function create(PageRequest $request)
    {
        $this->data['user'] = $request->user($this->guard);

        $setting = Setting::orderBy('created_at', 'DESC')->first();

        $this->data['current_min_online'] = 1000;
        $this->data['current_max_online'] = 1000;
        $this->data['current_min_booklet'] = 5;
        $this->data['current_max_booklet'] = 5;

        if ($setting) {
            $this->data['current_min_online'] = $setting->min_online;
            $this->data['current_max_online'] = $setting->max_online;
            $this->data['current_min_booklet'] = $setting->min_booklet;
            $this->data['current_max_booklet'] = $setting->max_booklet;
        }
        return view('system.setting.create', $this->data);
    }

    public function store(SettingRequest $request)
    {
        $setting = new Setting;
        $setting->min_online = request('min_online');
        $setting->max_online = request('max_online');
        $setting->min_booklet = request('min_booklet');
        $setting->max_booklet = request('max_booklet');
        $setting->save();

        session()->flash('notification-status', 'success');
        session()->flash('notification-msg', 'New setting has been saved.');
        return redirect()->route('admin.setting.index');
    }
}