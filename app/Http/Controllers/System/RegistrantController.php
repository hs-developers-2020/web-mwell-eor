<?php 

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;

/*
 * Models used in this Controller
 */
use App\Models\User;
use App\Models\UserDocument;
use App\Models\Registrant;
use App\Models\{RegistrantDocument, RegistrantLog};

/*
 * Requests used in this Controller
 */
use App\Http\Requests\System\PageRequest;
/*
 * App Classes used in this Controller
 */
use App\Services\Helper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Crypt;

class RegistrantController extends Controller
{

    protected $data;
    protected $guard = 'admin';

    public function __construct()
    {
    }
    
    public function index(PageRequest $request)
    {
        $per_page = request('per_page', 10);
        $this->data['user'] = $request->user($this->guard);
        $this->data['keyword'] = Str::lower(request('keyword'));

        $first_record = Registrant::orderBy('created_at', 'ASC')->first();
        $start_date = request('start_date', now()->startOfMonth());
        if($first_record) {
            $start_date = request('start_date', $first_record->created_at->format("Y-m-d"));
        }

        $this->data['start_date'] = Carbon::parse($start_date)->format("Y-m-d");
        $this->data['end_date'] = $this->data['end_date'] = Carbon::parse(request('end_date', now()))->format("Y-m-d");

        $this->data['registrants'] = Registrant::keyword($this->data['keyword'])
            ->where('status', 'pending')
            ->whereDate('created_at', '>=', Helper::date_db($this->data['start_date']))
            ->whereDate('created_at', '<=', Helper::date_db($this->data['end_date']))
            ->orderBy('created_at', "DESC")
            ->paginate($per_page);

        return view('system.registrant.index', $this->data);
    }

    public function show(PageRequest $request, $id = null)
    {
        $this->data['user'] = $request->user($this->guard);

        $this->data['registrant'] = Registrant::find($id);
        $this->data['documents'] = RegistrantDocument::where('registrant_id', $id)->get();

        if(!$this->data['registrant']) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Registrant record not found.");
            return redirect()->route('admin.registrant.index');
        }
        return view('system.registrant.show', $this->data);
    }

    public function activate(PageRequest $request, $id = null)
    {
        $user = $request->user($this->guard);
        $registrant =  Registrant::find($id);

        $is_duplicate =  User::where('email', $registrant->email)->count();
        if($is_duplicate) {
            session()->flash('notification-status', "warning");
            session()->flash('notification-msg', "Unable to approved account. Email address already in used.");
            return redirect()->route('admin.registrant.show', [$registrant->id]);
        }

        $decline = RegistrantDocument::where('registrant_id', $id)->where('status', 'declined')->count();

        if($decline > 0) {
            session()->flash('notification-status', "warning");
            session()->flash('notification-msg', "Unable to approved account. One the documents is tagged as declined.");
            return redirect()->route('admin.registrant.show', [$registrant->id]);
        }

        DB::beginTransaction();
        try{
            $account = new User;
            $account->firstname = $registrant->firstname;
            $account->lastname = $registrant->lastname;
            $account->name = $registrant->firstname . ' ' . $registrant->lastname;
            $account->email = $registrant->email;
            $account->password = $registrant->password;
            $account->type = 'user';
            $account->status = 'active';
            $account->save();

            RegistrantDocument::where('registrant_id', $id)->where('status', 'pending')->update(['status' => 'approved']);

            $documents =  RegistrantDocument::where('registrant_id', $id)->where('status', 'approved')->get();

            foreach($documents as $index => $document){
                $new_document = new UserDocument;
                $new_document->user_id = $account->id;
                $new_document->path = $document->path;
                $new_document->directory = $document->directory;
                $new_document->filename = $document->filename;
                $new_document->source = $document->source;
                $new_document->file_type = $document->file_type;
                $new_document->save();
            }

            $registrant->user_id = $account->id;
            $registrant->is_activated = 1;
            $registrant->status = "completed";
            $registrant->save();

            $log = new RegistrantLog;
            $log->registrant_id = $registrant->id;
            $log->user_id = $user->id;
            $log->remarks = "{$user->name} approved the account request of {$registrant->name}.";
            $log->save();

            $data = ['name' => $account->name];

            Mail::send(
                'emails.activate', $data, function ($message) use ($account) {
                    $message->from(env('MAIL_ADDRESS', 'noreply@domain.com'));
                    $message->to($account->email);
                    $message->subject('APPROVED ACCOUNT : EOR REQUEST ACCOUNT');
                }
            );
            
            DB::commit();

            session()->flash('notification-status', "success");
            session()->flash('notification-msg', "New account has been activated.");
            return redirect()->route('admin.account.show', [$account->id]);
        }catch(\Exception $e){
            DB::rollback();
            Log::info("ERROR: ", array($e));
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Server Error: Code #{$e->getLine()}");

            return redirect()->route('admin.registrant.show', [$registrant->id]);
        }
    }

    public function decline(PageRequest $request, $id = null)
    {
        $registrant =  Registrant::find($id);
        $user = $request->user($this->guard);

        DB::beginTransaction();
        try{
            RegistrantDocument::where('registrant_id', $id)->where('status', 'pending')->update(['status' => 'declined']);

            $registrant->is_activated = 0;
            $registrant->status = "declined";
            $registrant->remarks = strtoupper(request('remarks'));
            $registrant->save();

            $log = new RegistrantLog;
            $log->registrant_id = $registrant->id;
            $log->user_id = $user->id;
            $log->remarks = "{$user->name} declined the account request of {$registrant->name}. Remarks: {$registrant->remarks}";
            $log->save();

            $documents =  RegistrantDocument::where('registrant_id', $id)->get();

            $data = ['registrant' => $registrant, 'documents' => $documents, 'code' => Crypt::encryptString($registrant->id)];

            Mail::send(
                'emails.decline', $data, function ($message) use ($registrant) {
                    $message->from(env('MAIL_ADDRESS', 'noreply@domain.com'));
                    $message->to($registrant->email);
                    $message->subject('DECLINE ACCOUNT : EOR REQUEST ACCOUNT');
                }
            );
            
            DB::commit();

            session()->flash('notification-status', "success");
            session()->flash('notification-msg', "Account request has been declined.");
            return redirect()->route('admin.registrant.index');
        }catch(\Exception $e){
            DB::rollback();

            dd($e);
            Log::info("ERROR: ", array($e));
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Server Error: Code #{$e->getLine()}");

            return redirect()->route('admin.registrant.show', [$registrant->id]);
        }
    }

    public function updateStatus(PageRequest $request)
    {
        $user = $request->user($this->guard);
        $document = RegistrantDocument::where('id', request('document_id'))->where('registrant_id', request('registrant_id'))->first();

        if(!$document) {
            session()->flash('notification-status', "warning");
            session()->flash('notification-msg', "Record not found.");
            return redirect()->route('admin.registrant.show', [request('registrant_id')]);
        }

        $document->status = request('status');
        $document->save();

        $log = new RegistrantLog;
        $log->registrant_id = $document->registrant_id;
        $log->user_id = $user->id;
        $log->remarks = "{$user->name} {$document->status} the submitted documented. [{$document->filename}]";
        $log->save();

        session()->flash('notification-status', "success");
        session()->flash('notification-msg', "Document attachment has been {$document->status}.");
        return redirect()->route('admin.registrant.show', [request('registrant_id')]);
    }
}