<?php 

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;

/*
 * Models used in this Controller
 */
use App\Models\Teller;
use App\Models\Office;
use App\Models\Merchant;

/*
 * Requests used in this Controller
 */
use App\Http\Requests\System\TellerRequest;

use App\Http\Requests\System\PageRequest;
/*
 * App Classes used in this Controller
 */
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use App\Services\Helper;

class TellerController extends Controller
{

    protected $data;
    protected $guard = 'admin';

    public function __construct()
    {
        // parent::__construct();
        // array_merge($this->data, parent::get_data());
        $this->data['offices'] = ['' => "--Choose Office--"] + Office::pluck('name', 'code')->toArray();
        $this->data['merchants'] = ['' => "--Choose Merchant--"] + Merchant::pluck('merchant_name', 'id')->toArray();
    }
    
    public function index(PageRequest $request)
    {
        $per_page = request('per_page', 10);
        $this->data['user'] = $request->user($this->guard);
        $this->data['start_date'] = request('start_date', now()->subMonth()->format('m/d/Y'));
        $this->data['end_date'] = request('end_date', now()->format('m/d/Y'));
        $this->data['keyword'] = Str::lower(request('keyword'));

        $this->data['tellers'] = Teller::keyword($this->data['keyword'])
            ->whereDate('created_at', '>=', Helper::date_db($this->data['start_date']))
            ->whereDate('created_at', '<=', Helper::date_db($this->data['end_date']))
            ->orderBy('created_at', "DESC")
            ->paginate($per_page);
        return view('system.teller.index', $this->data);
    }

    public function create(PageRequest $request)
    {
        $this->data['user'] = $request->user($this->guard);
        
        return view('system.teller.create', $this->data);
    }

    public function store(TellerRequest $request)
    {
        try{
            DB::beginTransaction();
            $teller = new Teller;

            $teller->code = request('code');
            $teller->fname = request('fname');
            $teller->mname = request('mname');
            $teller->lname = request('lname');
            $teller->pin_code = request('pin_code');
            $teller->office_code = request('office_code');
            $teller->merchant_id = request('merchant_id');
            $teller->save();

            DB::commit();

            session()->flash('notification-status', "success");
            session()->flash('notification-msg', "New teller has been created.");
            return redirect()->route('admin.teller.index');

        }catch(\Exception $e){
            DB::rollBack();
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Server Error. Please try again.".$e->getMessage());
            return redirect()->back();
        }

    }

    public function edit(PageRequest $request, $id = null)
    {
        $this->data['user'] = $request->user($this->guard);

        $this->data['teller'] = Teller::find($id);

        if(!$this->data['teller']) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Teller record not found.");
            return redirect()->route('admin.teller.index');
        }
        return view('system.teller.edit', $this->data);
    }

    public function update(TellerRequest $request, $id = null)
    {
        $this->data['user'] = $request->user($this->guard);

        $teller = Teller::find($id);

        if(!$teller) {
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "teller record not found.");
            return redirect()->route('admin.teller.index');
        }
        try{
            DB::beginTransaction();
            
            $teller->code = request('code');
            $teller->fname = request('fname');
            $teller->mname = request('mname');
            $teller->lname = request('lname');
            $teller->pin_code = request('pin_code');
            $teller->office_code = request('office_code');
            $teller->merchant_id = request('merchant_id');
            $teller->save();

            DB::commit();

            session()->flash('notification-status', "success");
            session()->flash('notification-msg', "Teller record has been modified.");
            return redirect()->route('admin.teller.index');

        }catch(\Exception $e){
            DB::rollBack();
            session()->flash('notification-status', "failed");
            session()->flash('notification-msg', "Server Error. Please try again.".$e->getMessage());
            return redirect()->back();
        }

    }
}