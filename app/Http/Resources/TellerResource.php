<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TellerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id ?: 0,
        'code' => $this->code,
        'office_code' => $this->office_code ?: '',
        'merchant_id' => $this->merchant_id ?: '',
        'fname' => $this->fname ?: '',
        'mname' => $this->mname ?: '',
        'lname' => $this->lname ?: '',
        'pin_code' => $this->pin_code ?: '',
        ];
    }
}
