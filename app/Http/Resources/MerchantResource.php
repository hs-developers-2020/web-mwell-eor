<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MerchantResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id ?: 0,
            'code' => $this->merchant_code,
            'name' => $this->merchant_name,
            'address' => $this->address,
            'tin' => $this->tin,
            'telephone' => $this->tel_no,
        ];
    }
}
