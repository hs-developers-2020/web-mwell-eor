<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Services\Helper;

class TransactionHeaderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id ?: 0,
        'type' => $this->type,
        'bundle_id' => $this->bundle_id,
        'or' => $this->or_code ?: '',
        'merchant_code' => $this->merchant_code ?: '',
        'submerchant_code' => $this->submerchant_code ?: '',
        'merchant_name' => $this->merchant_name ?: '',
        'office_name' => $this->office_name ?: '',
        'office_code' => $this->office_code ?: '',
        'merchant_address' => $this->merchant_address ?: '',
        'tin' => $this->tin ?: '',
        'tel_no' => $this->tel_no ?: '',

        'terminal_code' => $this->terminal_code ?: '',
        'teller_code' => $this->teller_code ?: '',
        'teller_name' => $this->teller_name ?: '',
        'amount' => $this->amount ?: '',
        'date_created' => [
        'date_db' => $this->date_db($this->created_at, env("MASTER_DB_DRIVER", "mysql")),
        'month_year' => $this->month_year($this->created_at),
        'time_passed' => $this->time_passed($this->created_at),
        'timestamp' => $this->created_at
            ],
            'date_expiry' => [
            'date_db' => $this->date_db($this->date_expiry, env("MASTER_DB_DRIVER", "mysql")),
            'month_year' => $this->month_year($this->date_expiry),
            'time_passed' => $this->time_passed($this->date_expiry),
            'timestamp' => $this->date_expiry 
            ],
        ];
    }
}
