<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TerminalResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id ?: 0,
        'office_code' => $this->office_code ?: '',
        'merchant_id' => $this->merchant_id ?: '',
        'terminal_number' => $this->terminal_number ?: '',
        'serial_code' => $this->serial_code ?: '',
        'device_name' => $this->device_name ?: '',
        'device_serial' => $this->device_serial ?: '',
        ];
    }
}
