<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Services\Helper;

class TerminalDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id ?: 0,
        'transaction_id' => $this->transaction_id,
        'name' => $this->name ?: '',
        'description' => $this->description ?: '',
        'price' => $this->price ?: '',
        'qty' => $this->qty ?: '',
        'subtotal' => Helper::money_format($this->price * $this->qty),
        'date_created' => [
        'date_db' => $this->date_db($this->created_at, env("MASTER_DB_DRIVER", "mysql")),
        'month_year' => $this->month_year($this->created_at),
        'time_passed' => $this->time_passed($this->created_at),
        'timestamp' => $this->created_at
            ],
        ];
    }
}
