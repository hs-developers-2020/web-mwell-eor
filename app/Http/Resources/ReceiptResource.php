<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Services\Helper;

class ReceiptResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id ?: 0,
            // 'bundle_id' => $this->bundle_id,
            'receipt_number' => $this->receipt_number,
            'merchant' => new MerchantResource($this->merchant),
            'dst_fee' => $this->dst_fee ?: '',
            'convenience_fee' => $this->convenience_fee ?: '',
            'fname' => $this->fname ?: '',
            'lname' => $this->lname ?: '',
            'mname' => $this->mname ?: '',
            'suffix' => $this->suffix ?: '',
            'address' => $this->address ?: '',
            'name' => $this->name ?: '',
            'tin' => $this->tin ?: '',
            'total' => $this->total ?: 0,
            'display_total' => Helper::money_format($this->total),
            'subtotal' => $this->sub_total ?: 0,
            'display_subtotal' => Helper::money_format($this->sub_total),
            'vat_sales' => Helper::money_format($this->vat_sales) ?: "0",
            'vat_amount' => Helper::money_format($this->vat_amount) ?: "0",
            'vat_percentage' => $this->vat_percentage ?: 0,
            'payment_reference_code' => $this->payment_reference_code,
            'transaction_code' => $this->transaction_code,
            'bir_number' => "352-890-256-00000",
            'created_at' => $this->created_at,
            'url' => route('portal.generate', $this->payment_reference_code),
        ];
    }
}
