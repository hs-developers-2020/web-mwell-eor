<?php 

namespace App\Http\Requests\Api;

use App\Http\Requests\ApiRequestManager;

class TellerRequest extends ApiRequestManager
{

    public function rules()
    {
        $id = $this->route('id')?:0;

        $rules = [
        'code'    => "required|max:4|min:4|unique_teller:{$id}",
        'fname' => "required|alpha_spaces",
        'lname' => "required|alpha_spaces",
        'pin_code' => "required",

        ];

        return $rules;
    }

    public function messages()
    {
        return [
        'required'    => "Field is required.",
        'required_if'    => "Field is required.",
        'qty.integer'    => "No. of copies must be a number min. of 3 and divisible by 3. ",
        'qty.min'    => "Minimum no. of copies is 3.",
        'qty.max'    => "Maximum no. of copies is 99.",
        'numeric'    => "Please indicate a valid amount.",
        'min'    => "Negative amount is not allowed.",
        'code.max'   => "Input value is too long maxlength is :max",
        'code.min'   => "Input value is too long minlength is :min",
        'unique_teller' => "Teller code already exists.",
        'alpha_spaces' => "Special characters and number are not allowed.",
        ];
    }
}