<?php 

namespace App\Http\Requests\Api;

use App\Http\Requests\ApiRequestManager;

class TransactionRequest extends ApiRequestManager
{
    public function rules()
    {
        $rules = [
            'merchant_code'    => "required",
            "payment_reference_code" => "required",
            "transaction_code" => "required",
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => "Field is required.",
            'required_if' => "Field is required.",
            'qty.integer' => "No. of copies must be a number min. of 3 and divisible by 3. ",
            'qty.min' => "Minimum no. of copies is 3.",
            'qty.max' => "Maximum no. of copies is 99.",
            'numeric' => "Please indicate a valid amount.",
            'min' => "Negative amount is not allowed."
        ];
    }
}