<?php 

namespace App\Http\Requests\Api;

use App\Http\Requests\ApiRequestManager;

class RegisterRequest extends ApiRequestManager
{

    public function rules()
    {
        $rules = [
        'username' => "required|unique_username:0|username_format",
        'password' => "required",

        ];

        return $rules;
    }

    public function messages()
    {
        return [
        'required'    => "Field is required.",
        ];
    }
}