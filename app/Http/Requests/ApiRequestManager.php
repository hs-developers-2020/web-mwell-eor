<?php 

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

use Illuminate\Http\Exceptions\HttpResponseException;

class ApiRequestManager extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        if($validator->fails()) {
            $format = $this->route('format');
            $_response = [
            'msg' => "Incomplete or invalid input",
            'status' => false,
            'status_code' => "INVALID_DATA",
            'errors' => $validator->errors(),
            ];

            switch ($format) {
            case 'json':
                throw new HttpResponseException(response()->json($_response, 422));
            break;
            case 'xml':
                throw new HttpResponseException(response()->xml($_response, 422));
                    
                break;
            }
        }
        
    }

    public function response(array $errors)
    {
        $format = $this->route('format');
        $_response = [
        'msg' => "Incomplete or invalid input",
        'status' => false,
        'status_code' => "INVALID_DATA",
        'errors' => $errors,
        ];

        switch ($format) {
        case 'json':
            return response()->json($_response, 422);
            break;
        case 'xml':
            return response()->xml($_response, 422);
            break;
        }
    }
}