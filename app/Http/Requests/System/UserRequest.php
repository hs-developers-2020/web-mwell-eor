<?php 

namespace App\Http\Requests\System;

use App\Http\Requests\RequestManager;

class UserRequest extends RequestManager
{

    public function rules()
    {
        $id = $this->route('id')?:0;

        $rules = [
        'firstname' => 'required|alpha_spaces',
        'lastname' => "required|alpha_spaces",
        'email' => "required|email|unique_email:{$id},user",
        'type' => "required",
        'password' => "required|password_format|confirmed",
        ];

        if($id != 0) {
            $rules['password'] = "nullable|password_format|confirmed";
        }

        return $rules;
    }

    public function messages()
    {
        return [
        'required'    => "Field is required.",
        'email' => "Invalid email  address format.",
        'not_registered' => "Email address already taken.",
        'mimes' => "Invalid attachment. Valid file type is image, document or pdf file.",
        'email.pending_application' => "Already taken with pending application.",
        'password_format' => "Password must be 6-20 alphanumeric and some allowed special characters only.",
        'unique_email' => "Email address already taken.",
        'alpha_spaces' => "Invalid input. Please input a valid data.",
        ];
    }
}