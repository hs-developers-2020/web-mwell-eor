<?php 

namespace App\Http\Requests\System;

use App\Http\Requests\RequestManager;

class SettingRequest extends RequestManager
{

    public function rules()
    {

        $rules = [
        'min_online' => "required|integer",
        'max_online' => "required|integer",
        'min_booklet' => "required|integer",
        'max_booklet' => "required|integer",
        ];

        return $rules;
    }

    public function messages()
    {
        return [
        'required'        => "Field is required.",
        'integer'        => "Please input a valid number.",
        ];
    }
}