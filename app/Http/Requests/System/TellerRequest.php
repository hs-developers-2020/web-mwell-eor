<?php 

namespace App\Http\Requests\System;

use App\Http\Requests\RequestManager;

class TellerRequest extends RequestManager
{

    public function rules()
    {

        $id = $this->route('id')?:0;
        // $id = Auth::user()->id;

        $rules = [
        'fname'    => "required|alpha_spaces",
        'lname' => "required|alpha_spaces",
        'code'    => "required|max:4|min:4|unique_teller:{$id}",
        'pin_code'    => "required",
        // 'office_code' => "required",
        'merchant_id' => "required",
        ];

        return $rules;
    }

    public function messages()
    {
        return [
        'required'    => "Field is required.",
        'max'           => "Input value is too long maxlength is :max",
        'min'           => "Input value is too short minlength is :min",
        'unique_teller' => "Teller code already exists.",
        'alpha_spaces' => "Special characters and number are not allowed.",
        ];
    }
}