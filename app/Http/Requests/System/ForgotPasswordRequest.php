<?php

namespace App\Http\Requests\System;

use App\Http\Requests\RequestManager;

class ForgotPasswordRequest extends RequestManager
{

    public function rules()
    {

        $rules = [
        'email' => "required|email",
        ];

        return $rules;
    }

    public function messages()
    {
        return [
        'required'    => "Field is required.",
        'unique_username'    => "Username already used. Try another",
        'unique_email'    => "Email already used. Try another",
        'password.confirmed'    => "Password mismatch.",
        'password_format' => "Password must be 6-20 alphanumeric and some allowed special characters only.",
        ];
    }
}