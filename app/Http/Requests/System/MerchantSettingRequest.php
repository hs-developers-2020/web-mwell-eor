<?php

namespace App\Http\Requests\System;

use App\Http\Requests\RequestManager;

class MerchantSettingRequest extends RequestManager
{

    public function rules()
    {

        $rules = [
        'max_online' => "required|integer|min:1000",
        'max_booklet' => "required|integer|min:1",
        ];

        return $rules;
    }

    public function messages()
    {
        return [
        'required'       => "Field is required.",
        'integer'      => "Please input a valid number."
        ];
    }
}