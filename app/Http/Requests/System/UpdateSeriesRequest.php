<?php 

namespace App\Http\Requests\System;

use App\Http\Requests\RequestManager;

class UpdateSeriesRequest extends RequestManager
{

    public function rules()
    {

        $id = $this->route('id')?:0;
        // $id = Auth::user()->id;

        $rules = [
        'amount'        => "required|numeric|min:1|max:100000000000",
        'transaction_date' => "required|date",
        'name' => "required|alpha_spaces",
        'tin' => "required|regex:/^[0-9][0-9\-]+[0-9]$/|min:9|max:15",
        // 'or_code' => "required",
        ];

        return $rules;
    }

    public function messages()
    {
        return [
        'required'        => "Field is required.",
        'numeric'        => "Please provide a valid amount.",
        'regex'            => "Invalid format.",
        'alpha_spaces'     => "Special characters and number are not allowed."
        ];
    }
}