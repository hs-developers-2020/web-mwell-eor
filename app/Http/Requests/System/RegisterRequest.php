<?php 

namespace App\Http\Requests\System;

use App\Http\Requests\RequestManager;

class RegisterRequest extends RequestManager
{

    public function rules()
    {

        $rules = [
        'firstname' => 'required|alpha_spaces',
        'lastname' => "required|alpha_spaces",
        'email' => 'required|email|pending_application|not_registered|valid_email',
        'password' => "required|password_format|confirmed",
        'file.*' => 'required|min:1|mimes:jpg,jpeg,png,pdf|max:5000'
        ];
        
        return $rules;
    }

    public function messages()
    {
        return [
        'required'    => "Field is required.",
        'email' => "Invalid email  address format.",
        'not_registered' => "Email address already taken.",
        'mimes' => "Invalid attachment. Valid file type is image, document or pdf file.",
        'email.pending_application' => "Already taken with pending application.",
        'password_format' => "Password must be 6-20 alphanumeric and some allowed special characters only.",
        // 'alpha_spaces' => "Field must be entirely alphabetic characters only.",
        'alpha_spaces' => "Invalid input. Please input a valid data.",
        'max' => "Max size per file is 5MB.",
        'valid_email' => "Invalid email address format.",
        ];
    }
}