<?php 

namespace App\Http\Requests\System;

use App\Http\Requests\RequestManager;

class BusinessRequest extends RequestManager
{

    public function rules()
    {

        $id = $this->route('id')?:0;
        // $id = Auth::user()->id;

        $rules = [
        'section_code'        => "required",
        'division_code'     => "required",
        'merchant_name'        => "required|unique_merchant_name:{$id}",
        'address'             => "required",
        'tin'                => "required|unique_tin:{$id}|regex:/^[0-9][0-9\-]+[0-9]$/|min:9|max:15",    
        'tel_no'            => "nullable|regex:/^[0-9][0-9\-]+[0-9]$/|max:15",
        ];

        return $rules;
    }

    public function messages()
    {
        return [
        'required'        => "Field is required.",
        'required_if'     => "Field is required.",
        'max'           => "Input value is too long maxlength is :max",
        'min'           => "Input value is too short minlength is :min",
        'unique_merchant' => "Merchant code already exists.",
        'unique_merchant_name' => "Business already exists. Please try another or contact the administrator of this site.",
        'unique' => "Business with this TIN Number already exists.",
        'regex' => "Invalid format.",
        'max' => "Invalid format.",
        'min' => "Invalid format.",
        ];
    }
}