<?php

namespace App\Http\Requests\System;

use App\Http\Requests\RequestManager;

class MerchantRequest extends RequestManager
{

    public function rules()
    {
        $id = $this->route('id')?:0;

        $rules = [
            'section_code'        => "required_if:merchant_type,commercial",
            'division_code'     => "required_if:merchant_type,commercial",
            'merchant_code'        => "required|unique_merchant:{$id}",
            'merchant_name'        => "required",
            'merchant_type'     => "required",
            'submerchant_code'     => "nullable|max:8",
            // 'submerchant_name'     => "required_if:merchant_type,govt",
            'address'             => "required",
            'tin'                => "required|unique_tin:{$id}|regex:/^[0-9][0-9\-]+[0-9]$/|min:9|max:15",    
            'tel_no'            => "nullable|regex:/^[0-9][0-9\-]+[0-9]$/|max:15",
            'max_online'        => "required|integer|min:1000",
            'max_booklet'        => "required|integer|min:1",
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'required'        => "Field is required.",
            'required_if'     => "Field is required.",
            'max'           => "Input value is too long maxlength is :max",
            'min'           => "Input value is too short minlength is :min",
            'unique_merchant' => "Merchant code already exists.",
            'regex' => "Invalid format.",
            'integer' => "Please input a valid data.",
            'max' => "Invalid format.",
            'min' => "Invalid format.",
        // 'min' => "Minimum value is 1."
        ];
    }
}