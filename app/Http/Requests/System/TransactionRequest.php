<?php 

namespace App\Http\Requests\System;

use App\Http\Requests\RequestManager;

use App\Models\Setting;
use App\Models\Merchant;

class TransactionRequest extends RequestManager
{

    public function rules()
    {
        $max_online = 1000;
        $min_online = 1000;
        $max_booklet = 5;
        $min_booklet = 5;

        $setting = Setting::orderBy('created_at', 'DESC')->first();
        $merchant = Merchant::where('merchant_code', $this->request->get('merchant_code'))->first();

        if($setting) {
            $min_online = $setting->min_online ? $setting->min_online : 1000;
            $max_online = $setting->max_online ? $setting->max_online : 1000;
            $min_booklet = $setting->min_booklet ? $setting->min_booklet : 5;
            $max_booklet = $setting->max_booklet ? $setting->max_booklet : 5;
        }

        if($merchant AND $merchant->max_booklet AND $merchant->max_online) {
            $max_online = $merchant->max_online ? $merchant->max_online : 1000;
            $max_booklet = $merchant->max_booklet ? $merchant->max_booklet : 5;
        }

        $rules = [
        'merchant_code' => "required",
        'eor_booklet_qty' => "required_if:type,booklet|nullable|min:{$min_booklet}|max:{$max_booklet}|integer",
        'eor_online_qty' => "required_if:type,eor|nullable|min:{$min_online}|max:{$max_online}|integer",
        'layout' => "required_if:type,booklet",
        'atp_no' => "required_if:type,booklet", 
        'printer_accreditation' => "required_if:type,booklet",
        'type' => "required"
        ];

        return $rules;
    }

    public function messages()
    {
        return [
        'required_without_all'    => "Field is required. You must either input in No. Of Booklet or in No. of eOR Series.",
        'required' => 'Field is required.',
        'required_with' => "Field is required.",
        'required_if' => "Field is required.",
        ];
    }
}