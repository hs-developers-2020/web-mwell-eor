<?php 

namespace App\Http\Requests\System;

use App\Http\Requests\RequestManager;

class TerminalRequest extends RequestManager
{

    public function rules()
    {

        $id = $this->route('id')?:0;
        // $id = Auth::user()->id;

        $rules = [
        // 'serial_code'    => "required",
        'device_name' => "required|alpha_numeric_dash",
        'merchant_id'    => "required",
        'device_serial'    => "required",
        'office_code' => "required"
        ];

        return $rules;
    }

    public function messages()
    {
        return [
        'required'    => "Field is required.",
        'alpha_numeric_dash' => "Special characters are not allowed. Only letters, numbers and - are allowed.",
        ];
    }
}