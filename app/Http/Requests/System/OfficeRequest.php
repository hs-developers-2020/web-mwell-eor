<?php

namespace App\Http\Requests\System;

use App\Http\Requests\RequestManager;

class OfficeRequest extends RequestManager
{

    public function rules()
    {

        $id = $this->route('id')?:0;
        // $id = Auth::user()->id;

        $rules = [
        'name'    => "required",
        'code'    => "required|max:5|min:5|unique_office:{$id}",
        'address'    => "required",
        'merchant_id' => "required",
        ];

        return $rules;
    }

    public function messages()
    {
        return [
        'required'    => "Field is required.",
        'max'           => "Input value is too long maxlength is :max",
        'min'           => "Input value is too short minlength is :min",
        'unique_office' => "Office code already exists.",
        ];
    }
}