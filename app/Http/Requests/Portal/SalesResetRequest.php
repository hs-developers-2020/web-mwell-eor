<?php namespace App\Http\Requests\Portal;

use Session,Auth;
use App\Http\Requests\RequestManager;

class SalesResetRequest extends RequestManager
{

    public function rules()
    {

        $rules = [
        'sale_date' => "required|date",
        // 'sort_type'        => "required",
        ];

        return $rules;
    }

    public function messages()
    {
        return [
        'required'    => "Field is required.",
        'sale_date.date' => "Please indicate sales date you want to reset."
        ];
    }
}