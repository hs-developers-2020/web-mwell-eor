<?php

namespace App\Http\Middleware\Api;

use Closure, Helper;

use App\Models\Teller;
use App\Models\Terminal;
use App\Models\Merchant;
use App\Models\EorBundle;

class ExistRecord
{

    protected $format;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @param  string                   $record
     * @return mixed
     */
    public function handle($request, Closure $next, $record)
    {
        $this->format = $request->format;
        $response = array();

        switch (strtolower($record)) {
        case 'teller':
            if(! $this->_exist_teller($request)) {
                $response = [
                    'msg' => "Teller not found",
                    'status' => false,
                    'status_code' => "TELLER_NOT_FOUND",
                    'hint' => "Make sure the 'teller_code' from your request parameter exists and valid."
                ];
            }
            break;

        case 'terminal':
            if(! $this->_exist_terminal($request)) {
                $response = [
                    'msg' => "Terminal not found",
                    'status' => false,
                    'status_code' => "TERMINAL_NOT_FOUND",
                    'hint' => "Make sure the 'terminal_code' from your request parameter exists and valid."
                ];
            }
            break;

        case 'merchant':
            if(! $this->_exist_merchant($request)) {
                $response = [
                    'msg' => "Merchant not found",
                    'status' => false,
                    'status_code' => "MERCHANT_NOT_FOUND",
                    'hint' => "Make sure the 'merchant_code' from your request parameter exists and valid."
                ];
            }
            break;

        case 'sub_merchant':
            if(! $this->_exist_sub_merchant($request)) {
                $response = [
                    'msg' => "Submerchant not found",
                    'status' => false,
                    'status_code' => "SUB_MERCHANT_NOT_FOUND",
                    'hint' => "Make sure the 'submerchant_code' from your request parameter exists and valid."
                ];
            }
            break;

        case 'bundle':
            if(! $this->_exist_bundle($request)) {
                $response = [
                    'msg' => "Transaction not found",
                    'status' => false,
                    'status_code' => "TRANSACTION_NOT_FOUND",
                    'hint' => "Make sure the 'bundle_id' from your request parameter exists and valid."
                ];
            }
            break;

        case 'merchant_by_id':
            if(! $this->_exist_merchant_by_id($request)) {
                $response = [
                    'msg' => "Merchant not found",
                    'status' => false,
                    'status_code' => "MERCHANT_NOT_FOUND",
                    'hint' => "Make sure the 'merchant_id' from your request parameter exists and valid."
                ];
            }
            break;

        case 'own_merchant':
            if(! $this->_exist_own_merchant($request)) {
                $response = [
                    'msg' => "Merchant not found",
                    'status' => false,
                    'status_code' => "MERCHANT_NOT_FOUND",
                    'hint' => "Make sure the 'merchant_code' from your request parameter exists and valid."
                ];
            }
            break;
        }

        if(empty($response)) {
            return $next($request);
        }

        switch ($this->format) {
        case 'json':
            return response()->json($response, 404);
            break;
        case 'xml':
            return response()->xml($response, 404);
            break;
        }
    }

    private function _exist_teller($request)
    {
        $teller = Teller::where('code', request('teller_code'))->first();
        
        if($teller) {
            $request->merge(['teller_data' => $teller]);
            return true;
        }

        return false;
    }

    private function _exist_terminal($request)
    {
        $terminal = Terminal::where('terminal_number', request('terminal_code'))->first();
        
        if($terminal) {
            $request->merge(['terminal_data' => $terminal]);
            return true;
        }

        return false;
    }

    private function _exist_merchant($request)
    {
        $merchant = Merchant::where('merchant_code', request('merchant_code'))->first();
        
        if($merchant) {
            $request->merge(['merchant_data' => $merchant]);
            return true;
        }

        return false;
    }

    private function _exist_own_merchant($request)
    {
        return true;
        $auth = $request->user();
        $merchant = Merchant::where('merchant_code', request('merchant_code'))->where('owner_user_id', $auth->id)->first();
        
        if($merchant) {
            $request->merge(['merchant_data' => $merchant]);
            return true;
        }

        return false;
    }

    private function _exist_sub_merchant($request)
    {
        $sub_merchant = Merchant::where('merchant_code', request('merchant_code'))->where('submerchant_code', request('submerchant_code'))->first();
        
        if($sub_merchant) {
            $request->merge(['sub_merchant_data' => $sub_merchant]);
            return true;
        }

        return false;
    }

    private function _exist_bundle($request)
    {
        $bundle = EorBundle::where('bundle_id', request('bundle_id'))->first();
        
        if($bundle) {
            $request->merge(['bundle_data' => $bundle]);
            return true;
        }

        return false;
    }

    private function _exist_merchant_by_id($request)
    {
        $merchant = Merchant::find(request('merchant_id'));
        
        if($merchant) {
            $request->merge(['merchant_data' => $merchant]);
            return true;
        }

        return false;
    }
}