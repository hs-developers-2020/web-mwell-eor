<?php

namespace App\Http\Middleware\System;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @param  string|null              $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $guard = 'admin';
        if (Auth::guard($guard)->check()) {
            $user = Auth::guard($guard)->user();
            if($user->type == 'user') { 
                return redirect()->route('admin.transaction.index');
            } else {
                return redirect()->route('admin.index');
            }
        }

        return $next($request);
    }
}
