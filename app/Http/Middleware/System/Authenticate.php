<?php

namespace App\Http\Middleware\System;

use Closure, Session;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
     /**
      * Handle an incoming request.
      *
      * @param  \Illuminate\Http\Request $request
      * @param  \Closure                 $next
      * @param  string|null              $guard
      * @return mixed
      */
    public function handle($request, Closure $next, $guard = null)
    {
        $guard = 'admin';
        if (!Auth::guard($guard)->check() ) {
            
            // $redirect_uri = $request->url();
            // $redirect_key = base64_encode($redirect_uri);
            // session()->put($redirect_key, $redirect_uri);

            // return redirect()->route('frontend.auth.login', [$redirect_key]);
            return redirect()->route('admin.login');
        }

        $user = $request->user($guard);

        if($user->status != "active") {
            Auth::guard($guard)->logout();
            session()->flash('notification-status', 'warning');
            session()->flash('notification-msg', 'Account is not yet active.');
            return redirect()->route('admin.login');
        }

        return $next($request);
    }
}
