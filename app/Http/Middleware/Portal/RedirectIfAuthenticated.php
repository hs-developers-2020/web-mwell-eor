<?php

namespace App\Http\Middleware\Portal;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @param  string|null              $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $guard = session()->get('is_admin', 'no') == "yes" ? 'admin' : 'partner';
        if (Auth::guard($guard)->check()) {
            return redirect()->route('portal.index');
        }

        return $next($request);
    }
}
