<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>{{$user->name}}</title>
</head>
<body>
	<img src="data:image/png;base64, {!!base64_encode(QrCode::format('png')->size(1000)->generate(base64_encode($user->id)))!!}" style="height: 100%; width: 100%">
</body>
</html>