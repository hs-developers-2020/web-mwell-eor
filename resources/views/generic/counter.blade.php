

<script src="{{asset('generic/js/bootstrap.min.js')}}"></script>

<link href="{{asset('generic/css/bootstrap.min.css')}}" rel="stylesheet">
<script src="{{asset('generic/js/jquery.min.js')}}"></script>



<link rel="stylesheet" type="text/css" href="{{asset('generic/css/style.css')}}">
<!------ Include the above in your HEAD tag ---------->
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
<div class="container" style="padding-top: 100px;">
   
        <div class="row text-center" style="padding-bottom: 20px;">
            <div class="col-md-6">
                <div class="counter">
                    <i class="fa fa-tint fa-2x"></i>
                    <h2 class="counter-count donated" id="donate" data-attribute="12321"></h2>
                    <p class="count-text ">Donated</p>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-6">
                <div class="counter" >
                    <i class="fa fa-tablet fa-2x"></i>
                    <h2 class="counter-count2 downloaded" id="download" data-attribute="500"></h2>
                    <p class="count-text">App Download</p>
                </div>
            </div>
        </div>
</div>

<script type="text/javascript">


    $('.counter-count').each(function () {
         var donated = $('#donate').attr("data-attribute");
         var counter = 1;

    var id = setInterval(function(){ 
        $('.donated').text(counter);
        counter++;
        if(counter > donated){ clearInterval(id);}
    }, 100);  
    });

    $('.counter-count2').each(function () {
         var downloaded = $('#download').attr("data-attribute");
         var counter = 1;

    var id = setInterval(function(){ 
        $('.downloaded').text(counter);
        counter++;
        if(counter > downloaded){ clearInterval(id);}
    }, 400);  
    });
</script>