@extends('system._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Administrators</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
  </ul> --}}
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-10">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Record Data </h3>
            <span class="float-right">
              <a href="{{route('admin.administrator.create')}}" class="btn btn-primary">Add New Account</a>
            </span>
        </div>
        <div class="card-body">
            @include('system.components.notifications')
            <div class="table-responsive">
                <table class="table table-bordered text-center">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" class="text-left">Name</th>
                            <th scope="col">Username</th>
                            <th scope="col">Email</th>
                            <th scope="col">Status</th>
                            <th scope="col">Date Registered</th>
                            <th scope="col" class="text-left">Last Modified</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($accounts as $index => $account)
                        <tr>
                            <td class="text-left">{{$account->name}}</td>
                            <td>{{$account->username}}</td>
                            <td>{{$account->email}}</td>
                            <td>
                              <span class="badge badge-{{Helper::status_badge($account->status)}} r-badge">{{$account->status}}</span>

                            </td>
                            <td>{{$account->created_at->format("(D) M. d, Y")}}</td>
                            <td class="text-left">
                              <div>{{$account->updated_at->format("M. d, Y")}}</div>
                              <div>{{$account->updated_at->format("h:i A (D)")}}</div>
                            </td>

                            <td>
                                <div class="btn-group">
                                  <button type="button" data-toggle="dropdown" class="btn btn-sm btn-secondary dropdown-toggle" aria-expanded="false">Actions <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                                  <div class="dropdown-menu">
                                      <a class="dropdown-item" href="{{route('admin.administrator.edit',[$account->id])}}">Edit Details</a>
                                      <a class="dropdown-item" href="{{route('admin.administrator.reset_password',[$account->id])}}">Reset Password</a>
                                  </div>
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                          <td colspan="7"><div class="text-center"><i>No records found yet.</i></div></td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
  </div>
</div>
@stop