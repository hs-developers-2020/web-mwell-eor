@extends('system._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Administrators</h1>
  <ul>
      <li>Add New Account</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Administrator Account Form</h3>
        </div>
        <div class="card-body">
          @include('system.components.notifications')
          <form action="" method="POST">
            {!!csrf_field()!!}
            <div class="form-group has-error">
                <label for="input_name"><b>Name</b></label>
                <input type="text" class="form-control" id="input_name" placeholder="" value="{{old('name')}}" name="name">
                @if($errors->first('name'))
                <p class="form-text text-danger">{{$errors->first('name')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_username"><b>Username</b></label>
                <input type="text" class="form-control" id="input_username" placeholder="" value="{{old('username')}}" name="username">
                @if($errors->first('username'))
                <p class="form-text text-danger">{{$errors->first('username')}}</p>
                @endif
            </div>


            <div class="form-group">
                <label for="input_email"><b>Email</b></label>
                <input type="email" class="form-control" id="input_email" placeholder="" value="{{old('email')}}" name="email">
                @if($errors->first('email'))
                <p class="form-text text-danger">{{$errors->first('email')}}</p>
                @endif
            </div>
            <div class="form-group">
                <label for="input_password"><b>Password</b></label>
                <input type="password" class="form-control" id="input_password" placeholder="" value="" name="password">
                @if($errors->first('password'))
                <p class="form-text text-danger">{{$errors->first('password')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_password_confirmation"><b>Confirm Password</b></label>
                <input type="password" class="form-control" id="input_password_confirmation" placeholder="" value="" name="password_confirmation">
            </div>

            <div class="form-group">
                <label for="input_status"><b>Status</b></label>
                {!!Form::select('status',$statuses,old('status'),['class' => "form-control",'id' => "input_status"])!!}
                @if($errors->first('status'))
                <p class="form-text text-danger">{{$errors->first('status')}}</p>
                @endif
            </div>

            <div class="form-group">
              <a href="{{route('admin.administrator.index')}}" class="btn btn-secondary">Go back to Administrators List</a>
              <button type="submit" class="btn  btn-primary">Submit</button>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
@stop