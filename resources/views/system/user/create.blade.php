@extends('system._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>System Account</h1>
  <ul>
      <li>Create Account</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">User Account Form</h3>
        </div>
        <div class="card-body">
          @include('system.components.notifications')
          <form action="" method="POST">
            {!!csrf_field()!!}
          
            <div class="form-group">
                <label for="input_firstname"><b>First Name <span style="color:red">*</span></b></label>
                <input type="text" class="form-control" id="input_firstname" placeholder="" value="{{old('firstname')}}" name="firstname" style="text-transform: uppercase;">
                @if($errors->first('firstname'))
                <p class="form-text text-danger">{{$errors->first('firstname')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_lastname"><b>Last Name <span style="color:red">*</span></b></label>
                <input type="text" class="form-control" id="input_lastname" placeholder="" value="{{old('lastname')}}" name="lastname" style="text-transform: uppercase;">
                @if($errors->first('lastname'))
                <p class="form-text text-danger">{{$errors->first('lastname')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_email"><b>Email <span style="color:red">*</span></b></label>
                <input type="text" class="form-control" id="input_email" placeholder="" value="{{old('email')}}" name="email">
                @if($errors->first('email'))
                <p class="form-text text-danger">{{$errors->first('email')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_password"><b>Password <span style="color:red">*</span></b></label>
                <input type="password" class="form-control" id="input_password" placeholder="" value="{{old('password')}}" name="password">
                @if($errors->first('password'))
                <p class="form-text text-danger">{{$errors->first('password')}}</p>
                @endif
            </div>

            <div class="form-group">
              <label for="input_password"><b>Confirm Password <span style="color:red">*</span></b></label>
              <input type="password" class="form-control" name="password_confirmation" >
            </div>

            <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
                <label for="input_position">Account Type</label>
                {!!Form::select('type', $type, old('type'), ['class' => "form-control",'id' => "input_position"])!!}
                @if($errors->first('type'))
                <p class="form-text text-danger">{{$errors->first('type')}}</p>
                @endif
            </div>
     
            <div class="form-group">
              <a href="{{route('admin.user.index')}}" class="btn btn-secondary">Go back to User List</a>
              <button type="submit" class="btn  btn-primary">Submit</button>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
@stop

@section('page-scripts')
<script type="text/javascript">

</script>
@endsection