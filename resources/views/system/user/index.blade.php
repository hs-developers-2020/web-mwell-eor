@extends('system._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>System Account</h1>
  <ul>
      <li>List of Accounts</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card mb-5">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Advance Filters </h3>
            <span class="float-right">
              <a href="{{route('admin.user.index')}}" class="btn btn-default btn-sm">[Reset Filter]</a>

            </span>
        </div>
        <form action="" method="GET">
        <div class="card-body">
            <div class="row row-xs mb-4">
              <div class="col-md-3">
                <label for="">Keyword <small>(Name, Email)</small></label>
                <input type="text" class="form-control" placeholder="Keyword" name="keyword" value="{{$keyword}}">
              </div>
              <div class="col-md-3">
                  <label for="">Transaction Date Range</label>
                  <input type="text" class="form-control datepicker" placeholder="Start Date" name="start_date" value="{{$start_date}}">
              </div>
              <div class="col-md-3 mt-3 mt-md-0">
                  <label for="">&nbsp;</label>
                  <input type="text" class="form-control datepicker" placeholder="End Date" name="end_date" value="{{$end_date}}">
              </div>
              <div class="col-md-3 mt-3 mt-md-0">
                  <label for="">&nbsp;</label>
                  <button type="submit" class="btn btn-primary btn-block">Apply Filters</button>
              </div>
            </div>
        </div>
        </form>
    </div>
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Record Data </h3>
            <span class="float-right">
              <a href="{{route('admin.user.create')}}" class="btn btn-primary">Add User</a>
            </span>
        </div>
        <div class="card-body">
            @include('system.components.notifications')
          
            <div class="table-responsive">
              <table class="table table-bordered text-center">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Type</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                       
                    </tr>
                </thead>
                <tbody>
                    @forelse($users as $index => $value)
                    <tr>
                        <td>{{$value->name}}</td>
                        <td>{{$value->email}}</td>
                        <td>{{ $value->type == 'staff' ? 'BIR Staff' : Str::title($value->type) }}</td>
                        <td><span class="badge badge-pill badge-{{Helper::status_badge($value->status)}} p-2">{{Str::upper($value->status)}}</span></td>
                        <td>
                            <div class="btn-group">
                              <button type="button" data-toggle="dropdown" class="btn btn-sm btn-secondary dropdown-toggle" aria-expanded="false">Actions <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                              <div class="dropdown-menu">
                                  <a class="dropdown-item" href="{{route('admin.user.edit',[$value->id])}}">Edit Details</a>
                                  <a class="dropdown-item" href="{{route('admin.user.update_status',[$value->id])}}">{{ $value->status == 'active' ? 'Deactivate' : 'Activate'}}</a>
                              </div>
                            </div>
                        </td>
                    </tr>
                    @empty
                    <tr>
                      <td colspan="7"><div class="text-center"><i>No records found yet.</i></div></td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
            </div>
        </div>
    </div>
  </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{asset('assets/css/pickadate/classic.css',env('SECURE_ASSET',FALSE))}}">
<link rel="stylesheet" href="{{asset('assets/css/pickadate/classic.date.css',env('SECURE_ASSET',FALSE))}}">
<style type="text/css" media="screen">
  .table-responsive div a:hover{ text-decoration: underline; font-size: 600; }  
</style>
@stop

@section('page-scripts')
<script src="{{asset('assets/js/vendor/pickadate/picker.js',env('SECURE_ASSET',FALSE))}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js',env('SECURE_ASSET',FALSE))}}"></script>
<script type="text/javascript">
  $(function(){

    $(".datepicker").pickadate({
      format: 'mm/dd/yyyy',
    });
  })
</script>
@stop