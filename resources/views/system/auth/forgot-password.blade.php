@extends('system._layouts.auth')

@section('content')

<div class="col-lg-12 row p-0 m-0 w-100 d-flex flex-sm-column-reverse flex-md-row">
    <div class="col-lg-8 col-sm-12 m-0 p-0 h-100 col-sm-12">
        <!-- <img src="{{asset('images/Login_bg.jpg')}}" class="w-100 login-content" alt="Background Image"/> -->
    </div>
    <div class="col-lg-4 col-sm-12 card">
        <form method="POST" action="">
            @csrf
            <div class="card-body" style="margin-top:10%;">
                <div class="d-flex flex-column text-center">
                    <!-- <img class="mx-auto mb-1" src="{{asset('images/Logo.png')}}" alt="logo" style="height: 200px; width:200px;"/> -->
                    <h3 class="text-warning font-weight-bold mb-1">EOR PORTAL</h3>
                    <h4 class="font-weight-bold mb-5 mt-3">Trouble Logging In?</h4>
                </div>
                @include('system.components.notifications')
                <div class="form-group">
                    <label for="email"><h4 class="text-warning">Email</h4></label>
                    <input type="email" class="form-control form-control-lg" name="email" id="email" placeholder="Enter your email" tabindex="1" />
                    @error('email')<small>{{ $message }}</small>@enderror
                </div>
                <div class="text-center mt-5">
                    <button class="btn btn-lg btn-warning font-weight-bold text-white">Send Login Link</button>
                    <p class="mt-4"> OR</p>
                </div>
                <div class="mt-3">
                    <div class="text-center" style="font-size: 16px;">
                        Don't have an account? <a href="{{ route('admin.register') }}" style="color: #3c8dbc;">Sign up</a>
                    </div>
                </div>
                <hr>
                <div class="text-center" style="font-size: 16px;">
                    <a href="{{ route('admin.login') }}" style="color: #3c8dbc;">Back to Login</a>
                </div>
            </div>
        </form>
    </div>
</div>
@stop