@extends('system._layouts.auth')

@section('content')

<div class="col-lg-12 row p-0 m-0 w-100 d-flex flex-sm-column-reverse flex-md-row">
    <div class="col-lg-8 col-sm-12 m-0 p-0 h-100 col-sm-12">
        <img src="{{asset('images/Login_bg.jpg')}}" class="w-100 login-content" alt="Background Image"/>
    </div>
    <div class="col-lg-4 col-sm-12 card">
        <form method="POST" action="" enctype="multipart/form-data">
            @csrf
            <div class="card-body" style="margin-top:10%;">
                <div class="d-flex flex-column text-center">
                    <img class="mx-auto mb-1" src="{{asset('images/Logo.png')}}" alt="logo" style="height: 200px; width:200px;"/>
                    <h3 class="text-warning font-weight-bold mb-1">EOR PORTAL</h3>
                    <h4 class="font-weight-bold mb-5 mt-3">Submit Documents</h4>
                </div>
                @include('system.components.notifications')
                    <div class="form-group">
                        <p class="text-left font-weight-normal text-color-1 mb-0">To confirm you account:</p>
                        <small class="text-secondary">Attach your documents here. Maximum file size per document is 5MB.</small>
                    </div>
                    <div class="form-group">
                        <div class="dropzone text-center" style="min-height: 50px;">
                            <div class="fallback">
                                <input name="file[]" type="file" maxFiles="2" multiple style="max-width: 100%;" required />
                            </div>
                        </div>
                        @if($errors->first('file'))
                        <p class="form-text text-danger">{{$errors->first('file')}}</p>
                        @endif

                    </div>
                </div>
                <div class="text-center mt-3">
                    <button class="btn btn-lg btn-warning font-weight-bold text-white">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
@stop