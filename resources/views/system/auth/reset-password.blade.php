@extends('system._layouts.auth')

@section('content')

<div class="col-lg-12 row p-0 m-0 w-100 d-flex flex-sm-column-reverse flex-md-row">
    <div class="col-lg-8 col-sm-12 m-0 p-0 h-100 col-sm-12">
        <img src="{{asset('images/Login_bg.jpg')}}" class="w-100 login-content" alt="Background Image"/>
    </div>
    <div class="col-lg-4 col-sm-12 card">
        <form method="POST" action="">
            @csrf
            <div class="card-body" style="margin-top:10%;">
                <div class="d-flex flex-column text-center">
                    <img class="mx-auto mb-1" src="{{asset('images/Logo.png')}}" alt="logo" style="height: 200px; width:200px;"/>
                    <h3 class="text-warning font-weight-bold mb-1">EOR PORTAL</h3>
                    <h4 class="font-weight-bold mb-5 mt-3">Trouble Logging In?</h4>
                </div>
                @include('system.components.notifications')
                <div class="form-group">
                    <label for="password"><h4 class="text-warning">New Password</h4></label>
                    <input type="password" class="form-control form-control-lg" name="password" id="password" placeholder="Enter your password" tabindex="1" />
                    @error('password')<p style="color: red; font-size: 14px;" class="mt-1">{{$errors->first('password')}}</p>@enderror
                </div>
                <div class="form-group">
                    <label for="password"><h4 class="text-warning">Confirm Password</h4></label>
                    <input type="password" class="form-control form-control-lg" name="password_confirmation" id="password_confirmation" placeholder="Confirm new password" tabindex="2" />
                </div>
                <div class="text-center mt-5">
                    <button class="btn btn-lg btn-warning font-weight-bold text-white">Update Password</button>
                </div>
            </div>
        </form>
    </div>
</div>
@stop