@extends('system._layouts.auth')

@section('content')
{{-- <div class="auth-content">
    <div class="card o-hidden">
        <div class="row">
            <div class="col-md-4">
                <div class="p-4">
                    <h4>eOR</h4>
                </div>
            </div>
            <div class="col-md-8">
                <div class="p-4">
                    <div class="auth-logo text-center mb-4">
                        <img src="{{asset('assets/images/logo.png')}}" alt="">
                    </div>
                    <h1 class="mb-2 text-18">Sign In</h1>
                    @include('system.components.notifications')
                    <form action="" method="POST">
                        {!!csrf_field()!!}
                        <div class="form-group">
                            <label for="username">username address or Username</label>
                            <input id="username" class="form-control form-control-rounded" type="text" name="username" value="{{old('username')}}">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input id="password" class="form-control form-control-rounded" type="password" name="password">
                        </div>
                        <button type="submit" class="btn btn-rounded btn-primary btn-block mt-2">Sign In</button>
                    </form>
                    <div class="mt-3">
                        <div class="text-center">
                            Don't have an account? <a href="{{ route('admin.register') }}" style="color: #3c8dbc;">Sign up</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> --}}

<div class="col-lg-12 row p-0 m-0 w-100 d-flex flex-sm-column-reverse flex-md-row">
    <div class="col-lg-8 col-sm-12 m-0 p-0 h-100 col-sm-12">
        <!-- <img src="{{asset('images/Login_bg.jpg')}}" class="w-100 login-content" alt="Background Image"/> -->
    </div>
    <div class="col-lg-4 col-sm-12 card">
        <form method="POST" action="">
        @csrf
            <div class="card-body" style="margin-top:15%;">
                <div class="d-flex flex-column text-center">
                    <!-- <img class="mx-auto mb-1" src="{{asset('images/Logo.png')}}" alt="logo" style="height: 200px; width:200px;"/> -->
                    <h3 class="text-warning font-weight-bold mb-1">EOR PORTAL</h3>
                    <h4 class="text-warning font-weight-bold mb-5">Sign in to your account</h4>
                </div>
                @include('system.components.notifications')
                <div class="form-group">
                    <label for="username"><h4 class="text-warning">Username</h4></label>
                    <input type="username" class="form-control form-control-lg" name="username" id="username" placeholder="Enter Your Username" tabindex="1" />
                    @error('username')<small>{{ $message }}</small>@enderror
                </div>
                <div class="form-group mt-3">
                    <label for="password"><h4 class="text-warning">Password</h4></label>
                    <input type="password" class="form-control form-control-lg" name="password" id="password" placeholder="Enter Your Password" tabindex="2" />
                    @error('password')<small>{{ $message }}</small>@enderror
                </div>
                <a href="{{ route('admin.forgot_password') }}" style="float: right; font-size: 16px; color: #3c8dbc;" class="mt-1">Forgot Password?</a>
                <div class="text-center mt-5">
                    <button class="btn btn-lg btn-warning font-weight-bold text-white">Sign In</button>
                    {{-- <div class="form-check">
                        <input type="checkbox" name="remember_me" class="form-check-input mt-2 remember-me" style="" id="remember_me">
                        <span style=""></span>
                        <label class="form-check-label ml-2 pt-2" for="remember_me"><h4 class="font-weight-bold">Keep me logged in</h4></label>
                    </div> --}}
                </div>
                <div class="mt-3">
                    <div class="text-center" style="font-size: 16px;">
                        Don't have an account? <a href="{{ route('admin.register') }}" style="color: #3c8dbc;">Sign up</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@stop