@extends('system._layouts.auth')

@section('content')
<div class="auth-content">
    <div class="card o-hidden">
        <div class="row">
            <div class="col-md-12">
                <div class="p-4">
                    <h1 class="mb-2 text-18">CREATE YOUR ACCOUNT</h1>
                    @include('system.components.notifications')
                    <form action="" method="POST" enctype="multipart/form-data">
                        {!!csrf_field()!!}
                        <div class="form-group">
                            <label for="firstname">First Name</label>
                            <input id="firstname" class="form-control form-control-rounded" type="text" name="firstname" value="{{old('firstname')}}" style="text-transform: uppercase;">
                            @if($errors->first('firstname'))
                            <p class="form-text text-danger">{{$errors->first('firstname')}}</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="lastname">Last Name</label>
                            <input id="lastname" class="form-control form-control-rounded" type="text" name="lastname" value="{{old('lastname')}}" style="text-transform: uppercase;">
                            @if($errors->first('lastname'))
                            <p class="form-text text-danger">{{$errors->first('lastname')}}</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input id="email" class="form-control form-control-rounded" type="text" name="email" value="{{old('email')}}">
                            @if($errors->first('email'))
                            <p class="form-text text-danger">{{$errors->first('email')}}</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input id="input_password" class="form-control form-control-rounded" type="password" name="password">
                            @if($errors->first('password'))
                            <p class="form-text text-danger">{{$errors->first('password')}}</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">Confirm Password</label>
                            <input id="input_password_confirmation" class="form-control form-control-rounded" type="password" name="password_confirmation">
                        </div>
                        <div class="form-group" style="margin-left: 10px;">
                            <div style="display: flex; align-items:center;">
                                <input type="checkbox" style="width: 5%; margin-right: 10px; margin-left: -15px;" id="input_rememberme">
                                <label style="margin-top: 5px;">Show Password</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <p class="text-left font-weight-normal text-color-1 mb-0">To confirm you account:</p>
                            <small class="text-secondary">Attach your documents here. Maximum file size per document is 5MB.</small>
                        </div>
                        <div class="form-group">
                            <div class="dropzone text-center" style="min-height: 50px;">
                                <div class="fallback">
                                    <input name="file[]" type="file" maxFiles="2" multiple style="max-width: 100%;" required />
                                </div>
                            </div>
                            @foreach(range(1,count(old('file', []))) as $index => $value)
                                @if($errors->first("file.{$index}"))
                                <p class="form-text text-danger">{{$errors->first("file.{$index}")}}</p>
                                @endif
                            @endforeach

                        </div>
                        <button type="submit" class="btn btn-rounded btn-primary btn-block mt-2">Sign up</button>
                    </form>
                    <div class="mt-3">
                        <div class="text-center">
                            Have an account? <a href="{{ route('admin.login') }}" style="color: #3c8dbc;">Sign in</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('page-scripts')
<script type="text/javascript">
    $(function(){
        $('#input_rememberme').on('change', function(){
            $('#input_password').attr('type', $('#input_password').is(':password') ? 'text' : 'password');
            $('#input_password_confirmation').attr('type', $('#input_password_confirmation').is(':password') ? 'text' : 'password');
            $('#input_password').is(':password') ? $(this).attr('checked', false) : $(this).attr('checked', true)
        });
    });
</script>
@stop