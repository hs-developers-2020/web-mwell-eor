@extends('system._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Terminal</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
  </ul> --}}
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-8">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Record Data </h3>
            <span class="float-right">
              <a href="{{route('admin.terminal.create')}}" class="btn btn-primary">Add Terminal</a>
            </span>
        </div>
        <div class="card-body">
            @include('system.components.notifications')
          
            <div class="table-responsive">
              <table class="table table-bordered text-center">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Terminal Number</th>
                        <th scope="col">Serial Code</th>
                        <th scope="col">Device Name</th>
                        <th scope="col">Merchant</th>
                        <th scope="col">Office</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($terminals as $index => $terminal)
                    <tr>
                        <td>{{$terminal->terminal_number}}</td>
                        <td>{{$terminal->serial_code}}</td>
                        <td>{{$terminal->device_name}}</td>
                        <td>{{$terminal->merchant ? $terminal->merchant->merchant_name : ''}}</td>
                        <td>{{$terminal->office ? $terminal->office->name : ''}}</td>
                        <td>
                            <div class="btn-group">
                              <button type="button" data-toggle="dropdown" class="btn btn-sm btn-secondary dropdown-toggle" aria-expanded="false">Actions <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                              <div class="dropdown-menu">
                                  <a class="dropdown-item" href="{{route('admin.terminal.edit',[$terminal->id])}}">Edit Details</a>
                              </div>
                            </div>
                        </td>
                    </tr>
                    @empty
                    <tr>
                      <td colspan="6"><div class="text-center"><i>No records found yet.</i></div></td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
            </div>
        </div>
    </div>
  </div>
</div>
@stop