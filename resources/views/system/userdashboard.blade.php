@extends('system._layouts.main')
@section('styles')
    <style>
      .dashboard-card {
        box-sizing: border-box;
        padding: 0;
        margin-right: 5px;
        margin-left: 5;
        border-radius: 5px;
      }
      .dashboard-card .card-body{
        height: 100px;
        border-radius: 5px;
        color: white;
        margin-bottom: 10px;
        font-size: 20px;
      }
      .bg-mustard{
        background-color:rgb(248, 193, 70);
      }
      .bg-maroon{
        background-color:rgb(195, 74, 74);
      }

      .bg-blue{
        background-color: rgb(62, 170, 245);
      }

      .bg-green{
        background-color: rgb(139, 195, 74);
      }

      .bg-violet{
        background-color: rgb(190, 74, 195);
      }

    </style>
@endsection
@section('breadcrumb')
<div class="breadcrumb">
  <h1>Dashboard</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
  </ul> --}}
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
  <div class="col-md-12">
    @include('system.components.notifications')
    <h3 class="text-muted font-weight-bold">Dashboard</h3>
    <p class="text-muted font-weight-bold" style="font-size:18px;">Access the Portal Relevant information on this page</p>
    <h4 class="text-muted mt-5 font-weight-bold">Quick Links</h4>
    <div class="row row-cols-md-1 row-cols-lg-5 mt-3">
      <div class="dashboard-card col">
        <div class="card-body bg-mustard">
          <div class="d-flex justify-content-between">
            <i class="i-Bar-Chart float-left" style="font-size:50px"></i>
            <span class="text-right">Transactions</span>
          </div>
        </div>
      </div>
      <div class="dashboard-card col">
        <div class="card-body bg-blue">
          <div class="d-flex justify-content-between">
            <i class="i-Bar-Chart float-left" style="font-size:50px"></i>
            <span class="text-right">Merchant</span>
          </div>
        </div>
      </div>
    </div>
    <h4 class="text-muted mt-3 font-weight-bold">Contact Us</h4>
    <h5 class="text-muted mt-2 font-weight-bold">For question and concerns, contact us through our customer email:</h5>

    <div class="mt-2 d-flex justify-content-lg-start">
      <i class="i-Mail text-warning mt-1" style="font-size:25px;"></i> <span class="text-warning ml-2" style="font-size:20px;">eziapayemai@gmail.com</span>
    </div>
  </div>
@stop

@section('page-scripts')
<script type="text/javascript">
  $(function(){

  })
</script>
@stop