@extends('system._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Account Details</h1>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
@include('system.components.notifications')
<div class="row">
  <div class="col-md-5">
    <div class="card mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Account Details</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="display table table-striped table-bordered">
                <tbody>
                  <tr>
                    <td>Name</td>
                    <td>{{ $account->name }}</td>
                  </tr>
                  <tr>
                    <td>Email</td>
                    <td>{{ $account->email }}</td>
                  </tr>
                </tbody>
            </table>
          </div>
        </div>
    </div>
  </div>
  <div class="col-md-7">
    <div class="card mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Submitted Documents</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="display table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Document</th>
                  <th>Date Added</th>
                </tr>
              </thead>
              <tbody>
                @forelse($documents as $index => $document)
                <tr>
                  <td><a target="_blank" href="{{$document->source == "file" ? asset("{$document->path}/{$document->created_at->format("Ymd")}/{$document->filename}") : "{$document->directory}/{$document->filename}"}}"><u>{{$document->filename}}</u></a></td>
                  <td>{{$document->created_at->format("d F d h:i  A")}}</td>
                </tr>
                @empty
                <tr>
                  <td colspan="3">
                    <p>No record found.</p>
                  </td>
                </tr>
                @endforelse
              </tbody>
            </table>
          </div>
        </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="card o-hidden mb-4">
        <div class="card-body">
          
            <div class="table-responsive">
              <table class="table table-bordered text-center">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Business Name</th>
                        <th scope="col">TIN</th>
                        <th scope="col">Address</th>
                        <th scope="col">Available eOR Series</th>
                        <th scope="col">Available Booklet Series</th>
                        <th scope="col">Status</th>
                        {{-- <th scope="col">Action</th> --}}
                    </tr>
                </thead>
                <tbody>
                    @forelse($merchants as $index => $merchant)
                    <tr>
                        <td>
                          <div><a href="{{ route('admin.business.show', [$merchant->merchant_code]) }}" style="color: #3c8dbc;">{{ $merchant->merchant_name }}</a></div>
                          <div><small>{{ $merchant->merchant_code }}</small></div>
                        </td>
                        <td>{{ $merchant->tin ?: '---' }}</td>
                        <td>{{ $merchant->address ?: '---'}}</td>
                        <td>{{ $merchant->available_or }}</td>
                        <td>{{ $merchant->available_booklet }}</td>
                        <td><span class="badge badge-{{Helper::status_badge($merchant->status)}} r-badge">{{ Str::title($merchant->status) }}</span></td>
                        {{-- <td>
                            <div class="btn-group">
                              <button type="button" data-toggle="dropdown" class="btn btn-sm btn-secondary dropdown-toggle" aria-expanded="false">Actions <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                              <div class="dropdown-menu">
                                  <a class="dropdown-item" href="{{route('admin.business.show',[$merchant->merchant_code])}}">Show Details</a>
                              </div>
                            </div>
                        </td> --}}
                    </tr>
                    @empty
                    <tr>
                      <td colspan="7"><div class="text-center"><i>No records found yet.</i></div></td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
            {!! $merchants->appends(request()->query())->render() !!}
            </div>
        </div>
    </div>
  </div>
</div>
@stop

@section('page-modals')

@stop

@section('page-styles')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<style type="text/css" media="screen">
.qrcode{ height: 150px;  margin: 0px auto;}  
</style>
@stop

@section('page-scripts')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
<script type="text/javascript">
  $(function(){
    $(".action-delete").on("click",function(){
      var btn = $(this);
      $("#btn-confirm-delete").attr({"href" : btn.data('url')});
    });

    $('#btn-confirm-delete').on('click', function() {
      $('.btn-link').hide();
      $('.btn-loading').button('loading');
      $('#target').submit();
    });
  })
</script>
@stop