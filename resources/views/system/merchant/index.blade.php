@extends('system._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Merchant</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
  </ul> --}}
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card mb-5">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Advance Filters </h3>
            <span class="float-right">
              <a href="{{route('admin.merchant.index')}}" class="btn btn-default btn-sm">[Reset Filter]</a>

            </span>
        </div>
        <form action="" method="GET">
        <div class="card-body">
            <div class="row row-xs mb-4">
              <div class="col-md-3">
                <label for="">Keyword <small>(Name, Code)</small></label>
                <input type="text" class="form-control" placeholder="Keyword" name="keyword" value="{{$keyword}}">
              </div>
              <div class="col-md-3">
                  <label for="">Transaction Date Range</label>
                  <input type="text" class="form-control datepicker" placeholder="Start Date" name="start_date" value="{{$start_date}}">
              </div>
              <div class="col-md-3 mt-3 mt-md-0">
                  <label for="">&nbsp;</label>
                  <input type="text" class="form-control datepicker" placeholder="End Date" name="end_date" value="{{$end_date}}">
              </div>
              <div class="col-md-3 mt-3 mt-md-0">
                  <label for="">&nbsp;</label>
                  <button type="submit" class="btn btn-primary btn-block">Apply Filters</button>
              </div>
            </div>
        </div>
        </form>
    </div>
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Record Data </h3>
            @if(in_array($user->type, ['super_user', 'admin']))
            <span class="float-right">
              <a href="{{route('admin.merchant.create')}}" class="btn btn-primary">Add Merchant</a>
            </span>
            @endif
        </div>
        <div class="card-body">
            @include('system.components.notifications')
          
            <div class="table-responsive">
              <table class="table table-bordered text-center">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Merchant Name</th>
                        <th scope="col">Merchant Code</th>
                        <th scope="col">Section</th>
                        <th scope="col">Division</th>
                        <th scope="col">Type</th>
                        <th scope="col">Available eOR Series</th>
                        <th scope="col">Available Booklet Series</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($merchants as $index => $merchant)
                    <tr>
                        <td>{{$merchant->merchant_name}}</td>
                        <td>{{$merchant->merchant_code}}</td>
                        <td>{{$merchant->section ? $merchant->section->name : '-'}}</td>
                        <td>{{$merchant->division ? $merchant->division->name : '-'}}</td>
                        <td>{{ $merchant->merchant_type == 'govt' ? 'Government' : 'Commercial' }}</td>
                        <td>{{ $merchant->merchant_type == 'govt' ? '---' : $merchant->available_or }}</td>
                        <td>{{ $merchant->merchant_type == 'govt' ? '---' : $merchant->available_booklet }}</td>
                        <td>
                            <div class="btn-group">
                              <button type="button" data-toggle="dropdown" class="btn btn-sm btn-secondary dropdown-toggle" aria-expanded="false">Actions <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                              @if(in_array($user->type, ['super_user', 'admin']))
                              <div class="dropdown-menu">
                                  <a class="dropdown-item" href="{{route('admin.merchant.edit',[$merchant->id])}}">Edit Details</a>
                              </div>
                              @endif
                              @if(in_array($user->type, ['staff']))
                              <div class="dropdown-menu">
                                  <a class="dropdown-item" href="{{route('admin.merchant.edit_details',[$merchant->merchant_code])}}">Edit Details</a>
                              </div>
                              @endif
                            </div>
                        </td>
                    </tr>
                    @empty
                    <tr>
                      <td colspan="8"><div class="text-center"><i>No records found yet.</i></div></td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
            {!! $merchants->appends(request()->query())->render() !!}
            </div>
        </div>
    </div>
  </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{asset('assets/css/pickadate/classic.css',env('SECURE_ASSET',FALSE))}}">
<link rel="stylesheet" href="{{asset('assets/css/pickadate/classic.date.css',env('SECURE_ASSET',FALSE))}}">
<style type="text/css" media="screen">
  .table-responsive div a:hover{ text-decoration: underline; font-size: 600; }  
</style>
@stop

@section('page-scripts')
<script src="{{asset('assets/js/vendor/pickadate/picker.js',env('SECURE_ASSET',FALSE))}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js',env('SECURE_ASSET',FALSE))}}"></script>
<script type="text/javascript">
  $(function(){

    $(".datepicker").pickadate({
      format: 'yyyy-mm-dd',
    });
  })
</script>
@stop