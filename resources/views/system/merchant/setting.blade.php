@extends('system._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Merchant</h1>
  <ul>
      <li>Merchant Details :: {{ $merchant->merchant_name }}</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-5">
    <div class="card mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Merchant Details</h3>
        </div>
        <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-hover">
                <tbody>
                  <tr>
                    <td>Merchant Name</td>
                    <td colspan="3"><strong class="text-danger">{{ $merchant->merchant_name }}</strong></td>
                  </tr>
                  <tr>
                    <td>Section</td>
                    <td colspan="3">{{ $merchant->section ? $merchant->section->name : '---' }}</td>
                  </tr>
                  <tr>
                    <td>Division</td>
                    <td colspan="3">{{ $merchant->division ? $merchant->division->name : '---' }}</td>
                  </tr>
                  <tr>
                    <td>Address</td>
                    <td colspan="3">{{ $merchant->address ?: '---' }}</td>
                  </tr>
                  <tr>
                    <td>TIN</td>
                    <td colspan="3">{{ $merchant->tin ?: '---' }}</td>
                  </tr>
                  <tr>
                    <td>Telephone Number</td>
                    <td colspan="3">{{ $merchant->tel_no ?: '---' }}</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="form-group">
              <a href="{{route('admin.merchant.index')}}" class="btn btn-secondary">Go back to Merchant List</a>
            </div>
        </div>
    </div>
  </div>
  <div class="col-md-7">
    <div class="card mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Update Merchant Details</h3>
        </div>
        <div class="card-body">
        <form method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="input_max_online"><b>Max. Online Series</b></label>
                <input type="number" class="form-control" id="input_max_online" placeholder="" value="{{old('max_online', $merchant->max_online)}}" name="max_online">
                @if($errors->first('max_online'))
                <p class="form-text text-danger">{{$errors->first('max_online')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_max_booklet"><b>Max. Booklet Series (x50)</b></label>
                <input type="number" class="form-control" id="input_max_booklet" placeholder="" value="{{old('max_booklet', $merchant->max_booklet)}}" name="max_booklet">
                @if($errors->first('max_booklet'))
                <p class="form-text text-danger">{{$errors->first('max_booklet')}}</p>
                @endif
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-secondary">Submit</button>
            </div>
        </form>
        </div>
    </div>
  </div>
</div>
@stop

@section('page-modals')

@stop

@section('page-styles')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<style type="text/css" media="screen">
.qrcode{ height: 150px;  margin: 0px auto;}  
</style>
@stop

@section('page-scripts')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
<script type="text/javascript">
  $(function(){
    $(".action-delete").on("click",function(){
      var btn = $(this);
      $("#btn-confirm-delete").attr({"href" : btn.data('url')});
    });

    $('#btn-confirm-delete').on('click', function() {
      $('.btn-link').hide();
      $('.btn-loading').button('loading');
      $('#target').submit();
    });
  })
</script>
@stop