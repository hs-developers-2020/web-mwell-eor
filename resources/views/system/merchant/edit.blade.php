@extends('system._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Merchant</h1>
  <ul>
      <li>Update Merchant Details</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Update Merchant Form</h3>
        </div>
        <div class="card-body">
          @include('system.components.notifications')
          <form action="" method="POST">
            {!!csrf_field()!!}
            <div class="form-group">
                <label for="input_type"><b>Type</b></label>
                {!!Form::select('merchant_type',$types,old('merchant_type', $merchant->merchant_type),['class' => "form-control",'id' => "input_type"])!!}
                @if($errors->first('merchant_type'))
                <p class="form-text text-danger">{{$errors->first('merchant_type')}}</p>
                @endif
            </div>
            <div class="form-group">
                <label for="input_section_code"><b>Section</b></label>
                {!!Form::select('section_code',$sections,old('section_code', $merchant->section_code),['class' => "form-control",'id' => "input_section_code"])!!}
                @if($errors->first('section_code'))
                <p class="form-text text-danger">{{$errors->first('section_code')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_division_code"><b>Division</b></label>
                {!!Form::select('division_code',$divisions,old('division_code', $merchant->division_code),['class' => "form-control",'id' => "input_division_code"])!!}
                <input type="hidden" value="{{old('division_code', $merchant->division_code)}}" id="input_division" />
                @if($errors->first('division_code'))
                <p class="form-text text-danger">{{$errors->first('division_code')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_owner_user_id"><b>Owner</b></label>
                {!!Form::select('owner_user_id',$owners,old('owner_user_id', $merchant->owner_user_id),['class' => "form-control",'id' => "input_owner_user_id"])!!}
                @if($errors->first('owner_user_id'))
                <p class="form-text text-danger">{{$errors->first('owner_user_id')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_merchant_code"><b>Merchant Code</b></label>
                <input type="text" class="form-control" id="input_merchant_code" placeholder="" value="{{old('merchant_code', $merchant->merchant_code)}}" name="merchant_code">
                @if($errors->first('merchant_code'))
                <p class="form-text text-danger">{{$errors->first('merchant_code')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_merchant_name"><b>Merchant Name</b></label>
                <input type="text" class="form-control" id="input_merchant_name" placeholder="" value="{{old('merchant_name', $merchant->merchant_name)}}" name="merchant_name">
                @if($errors->first('merchant_name'))
                <p class="form-text text-danger">{{$errors->first('merchant_name')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_sub_merchant_code"><b>Submerchant Code</b></label>
                <input type="text" class="form-control" id="input_sub_merchant_code" placeholder="" value="{{old('submerchant_code', $merchant->submerchant_code)}}" name="submerchant_code">
                @if($errors->first('submerchant_code'))
                <p class="form-text text-danger">{{$errors->first('submerchant_code')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_sub_merchant_name"><b>Submerchant Name</b></label>
                <input type="text" class="form-control" id="input_sub_merchant_name" placeholder="" value="{{old('submerchant_name', $merchant->submerchant_name)}}" name="submerchant_name">
                @if($errors->first('submerchant_name'))
                <p class="form-text text-danger">{{$errors->first('submerchant_name')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_address"><b>Address</b></label>
                <input type="text" class="form-control" id="input_address" placeholder="" value="{{old('address', $merchant->address)}}" name="address">
                @if($errors->first('address'))
                <p class="form-text text-danger">{{$errors->first('address')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_tin"><b>TIN No.</b></label>
                <input type="text" class="form-control" id="input_tin" placeholder="" value="{{old('tin', $merchant->tin)}}" name="tin">
                @if($errors->first('tin'))
                <p class="form-text text-danger">{{$errors->first('tin')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_tel_no"><b>Telephone No.</b></label>
                <input type="text" class="form-control" id="input_tel_no" placeholder="(XX) XXXX-XXXX / +63 9XX-XXX-XXX" value="{{old('tel_no', $merchant->tel_no)}}" name="tel_no">
                @if($errors->first('tel_no'))
                <p class="form-text text-danger">{{$errors->first('tel_no')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_max_online"><b>Max. Online Series</b></label>
                <input type="number" class="form-control" id="input_max_online" placeholder="" value="{{old('max_online', $merchant->max_online)}}" name="max_online">
                @if($errors->first('max_online'))
                <p class="form-text text-danger">{{$errors->first('max_online')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_max_booklet"><b>Max. Booklet Series (x50)</b></label>
                <input type="number" class="form-control" id="input_max_booklet" placeholder="" value="{{old('max_booklet', $merchant->max_booklet)}}" name="max_booklet">
                @if($errors->first('max_booklet'))
                <p class="form-text text-danger">{{$errors->first('max_booklet')}}</p>
                @endif
            </div>

            <div class="form-group">
              <a href="{{route('admin.merchant.index')}}" class="btn btn-secondary">Go back to Merchant List</a>
              <button type="submit" class="btn  btn-primary">Submit</button>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
@stop

@section('page-scripts')
<script type="text/javascript">
    $('#input_section_code').on('change', function(){
        $.ajax({
            url : "{{route('admin.get_division')}}",
            dataType : "json",
            data: {code : $(this).val()},
            type: "GET",
            success : function(data){
                if(data.divisions){
                    $('#input_division_code').empty()
                    $('#input_division_code').append('<option value="">-- Choose Division --</option>');
                    $.each(data.divisions,function(i, division){
                        if($('#input_division').val() == division.code)
                            $('#input_division_code').append('<option value="' + division.code +'" selected="selected">' + division.name +"</option>");
                        else
                            $('#input_division_code').append('<option value="' + division.code +'">' + division.name +"</option>");
                    });
                }else {

                }
            },  
            error : function(jqXHR,textStatus,thrownError){
            }
        });
    }).change();
</script>
@endsection