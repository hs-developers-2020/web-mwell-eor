@extends('system._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Transactions</h1>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card mb-5">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Advance Filters </h3>
            <span class="float-right">
              <a href="{{route('admin.transaction.index')}}" class="btn btn-default btn-sm">[Reset Filter]</a>

            </span>
        </div>
        <form action="" method="GET">
        <div class="card-body">
            <div class="row row-xs mb-4">
              <div class="col-md-2">
                <label for="">Keyword  <small>(Reference Code)</small></label>
                <input type="text" class="form-control" placeholder="Keyword" name="keyword" value="{{$keyword}}">
              </div>
              <div class="col-md-2">
                <label for="">Status</label>
                {!!Form::select('status',$statuses,old('status',$status),['class' => "custom-select mw-100 ",'id' => "input_rdo"])!!}
              </div>
              <div class="col-md-3">
                  <label for="">Transaction Date Range</label>
                  <input type="text" class="form-control datepicker" placeholder="Start Date" name="start_date" value="{{$start_date}}">
              </div>
              <div class="col-md-3 mt-3 mt-md-0">
                  <label for="">&nbsp;</label>
                  <input type="text" class="form-control datepicker" placeholder="End Date" name="end_date" value="{{$end_date}}">
              </div>
              <div class="col-md-2 mt-3 mt-md-0">
                  <label for="">&nbsp;</label>
                  <button type="submit" class="btn btn-primary btn-block">Apply Filters</button>
              </div>
            </div>
        </div>
        </form>
    </div>
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">
              Record Data {{--  {{$start_date == $end_date ? Carbon::parse($start_date)->format("M. d Y (D)") : Carbon::parse($start_date)->format("M. d Y (D)")." - ".Carbon::parse($end_date)->format("M. d Y (D)")}} --}}
            </h3>
            @if(in_array($user->type, ['user']))
            <div>
              <span class="float-right">
                <a class="btn btn-sm btn-primary text-white" href="{{route('admin.transaction.create')}}">Purchase Official Receipt</a>
              </span>
            </div>
            @endif
        </div>
        <div class="card-body">
            @include('system.components.notifications')
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Transaction Date</th>
                            <th scope="col">Reference Code</th>
                            <th scope="col">Business</th>
                            <th scope="col">Status</th>
                            <th scope="col">Amount</th>
                            <th scope="col">Payment Date</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                      @forelse($transactions as $index => $transaction)
                      <tr>
                        <td>
                          <div>{{$transaction->created_at->format("d M Y")}}</div>
                          <div><small>{{$transaction->created_at->format("h:i A")}}</small></div>
                        </td>
                        <td>
                          <div><a href="{{route('admin.transaction.show',[Str::lower($transaction->reference_code)])}}" style="color: #3c8dbc;">{{$transaction->reference_code}}</a></div>
                        </td>
                        <td>
                          <div>{{$transaction->merchant_code}}</div>
                          <div><small>{{$transaction->merchant ? $transaction->merchant_name : '---'}}</small></div>
                        </td>
                        <td>
                          <div><span class="badge badge-{{Helper::status_badge($transaction->status)}}">{{str_replace("_"," ",$transaction->status)}}</span></div>
                          <div><span class="badge badge-{{Helper::status_badge($transaction->payment_status)}}">{{$transaction->payment_status}}</span></div>
                          <div><span class="badge badge-{{Helper::payment_type($transaction->payment_type)}}">{{str_replace("_"," ",$transaction->payment_type)}}</span></div>

                        </td>
                        <td>₱ {{ $transaction->total_amount?:$transaction->amount }}</td>
                        <td>
                          @if($transaction->payment_status == "PAID")
                          <div>{{$transaction->payment_date->format("d M Y")}}</div>
                          <div><small>{{$transaction->payment_date->format("h:i A")}}</small></div>
                          @else 
                          ---
                          @endif
                        </td>
                        <td>
                          <div class="btn-group">
                            <button type="button" data-toggle="dropdown" class="btn btn-sm btn-secondary dropdown-toggle" aria-expanded="false">Actions <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{route('admin.transaction.show',[Str::lower($transaction->reference_code)])}}">View Transaction</a>
                                @if($transaction->eor_booklet_qty > 0)
                                <a class="dropdown-item" href="{{route('admin.transaction.download',[Str::lower($transaction->reference_code)])}}">Download PDF</a>
                                @endif
                            </div>
                          </div>
                        </td>
                      </tr>
                      @empty
                      <tr>
                        <td colspan="7" class="text-center">No available data</td>
                      </tr>
                      @endforelse
                    </tbody>
                </table>
                {!! $transactions->appends(request()->query())->render() !!}
            </div>
        </div>
    </div>
  </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css',env('SECURE_ASSET',FALSE))}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css',env('SECURE_ASSET',FALSE))}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css',env('SECURE_ASSET',FALSE))}}">
<style type="text/css" media="screen">
  .table-responsive div a:hover{ text-decoration: underline; font-size: 600; }  
</style>
@stop

@section('page-scripts')
<script src="{{asset('assets/js/vendor/datatables.min.js',env('SECURE_ASSET',FALSE))}}"></script>
<script src="{{asset('assets/js/datatables.script.js',env('SECURE_ASSET',FALSE))}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.js',env('SECURE_ASSET',FALSE))}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js',env('SECURE_ASSET',FALSE))}}"></script>
<script type="text/javascript">
  $(function(){

    $(".datepicker").pickadate({
      format: 'yyyy-mm-dd',
    });

  })
</script>
@stop