@extends('system._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Transaction</h1>
  <ul>
      <li>Transaction Details :: {{$transaction->reference_code}}</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-8">
    <div class="card mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Transaction Details</h3>
        </div>
        <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-hover">
                <tbody>
                  <tr>
                    <td>Transaction Code</td>
                    <td colspan="3"><strong class="text-danger">{{ $transaction->reference_code }}</strong></td>
                  </tr>
                  <tr>
                    <td>Transaction Date</td>
                    <td colspan="3">{{ $transaction->created_at->format("d M Y h:i A") }}</td>
                  </tr>
                  <tr>
                    <td>Business</td>
                    <td colspan="3">{{ $transaction->merchant ? $transaction->merchant->merchant_name : '---' }}</td>
                  </tr>
                  @if($transaction->eor_online_qty > 0)
                  <tr>
                    <td>eOr Series</td>
                    <td colspan="3"><strong>₱ {{$transaction->eor_online_total}} (<strong>x{{$transaction->eor_online_qty}}</strong> @  ₱ {{$transaction->eor_online_price}})</strong></td>
                  </tr>
                  @endif
                  @if($transaction->eor_booklet_qty > 0)
                  <tr>
                    <td>Booklet</td>
                    <td colspan="3"><strong>₱ {{$transaction->eor_booklet_total}} (<strong>x{{$transaction->eor_booklet_qty}}</strong> @  ₱ {{$transaction->eor_booklet_price}})</strong></td>
                  </tr>
                  @endif
                  <tr>
                    <td>Payment  Date</td>
                    <td colspan="3">
                      @if($transaction->payment_status == "PAID")
                      {{ $transaction->payment_date->format("d M Y h:i A") }}
                      @else
                      ---
                      @endif
                    </td>
                  </tr>
                  <tr>
                    <td>Amount</td>
                    <td colspan="3"><strong>₱ {{ Helper::money_format($transaction->amount) }}</strong></td>
                  </tr>
                  <tr>
                    <td>Convenience Fee</td>
                    <td colspan="3"><strong>₱ {{ Helper::money_format($transaction->convenience_fee?:"0") }}</strong></td>
                  </tr>
                </tbody>
              </table>
              <span class="float-left">
                <span class="badge badge-{{ Helper::status_badge($transaction->status) }}">{{ str_replace("_"," ",$transaction->status) }}</span>
                <span class="badge badge-{{ Helper::status_badge($transaction->payment_status) }}">{{ $transaction->payment_status }}</span>
                <span class="badge badge-{{ Helper::payment_type($transaction->payment_type) }}">{{ str_replace("_"," ",$transaction->payment_type) }}</span>

              </span>
              <span class="float-right">
                <h2><strong>TOTAL AMOUNT: ₱ {{ Helper::money_format($transaction->total_amount?:$transaction->amount) }}</strong></h2>
              </span>
            </div>
            <div class="form-group">
              <a href="{{route('admin.transaction.index')}}" class="btn btn-secondary">Go back to Transaction List</a>
            </div>
        </div>
    </div>
  </div>
</div>
@stop

@section('page-modals')

@stop

@section('page-styles')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<style type="text/css" media="screen">
.qrcode{ height: 150px;  margin: 0px auto;}  
</style>
@stop

@section('page-scripts')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
<script type="text/javascript">
  $(function(){
    $(".action-delete").on("click",function(){
      var btn = $(this);
      $("#btn-confirm-delete").attr({"href" : btn.data('url')});
    });

    $('#btn-confirm-delete').on('click', function() {
      $('.btn-link').hide();
      $('.btn-loading').button('loading');
      $('#target').submit();
    });
  })
</script>
@stop