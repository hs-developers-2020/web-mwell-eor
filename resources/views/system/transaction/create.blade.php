@extends('system._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Transaction</h1>
  <ul>
      <li>Create New Record</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Transaction Form</h3>
        </div>
        <div class="card-body">
          @include('system.components.notifications')
          <form action="" method="POST">
            {!!csrf_field()!!}

            <div class="form-group">
                <label for="input_merchant_code"><b>Business</b></label>
                {!!Form::select('merchant_code',$business,old('merchant_code'),['class' => "form-control",'id' => "input_merchant_code"])!!}
                @if($errors->first('merchant_code'))
                <p class="form-text text-danger">{{$errors->first('merchant_code')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_type"><b>Transaction Type</b></label>
                {!!Form::select('type',['' => '--Select Transaction Type--', 'eor' => 'Buy eOR Series', 'booklet' => 'Buy Booklet'],old('type'),['class' => "form-control",'id' => "input_type"])!!}
                @if($errors->first('type'))
                <p class="form-text text-danger">{{$errors->first('type')}}</p>
                @endif
            </div>

            <div class="form-group" id="layout_container">
                <label for="input_layout"><b>Layout</b></label>
                {!!Form::select('layout',['' => '--Select Layout--', 'vertical' => 'Vertical', 'horizontal' => 'Horizontal'],old('layout'),['class' => "form-control",'id' => "input_layout"])!!}
                @if($errors->first('layout'))
                <p class="form-text text-danger">{{$errors->first('layout')}}</p>
                @endif
            </div>

            <div class="form-group {{$errors->first('atp_no') ? 'text-danger' : NULL}}" id="atp_container">
              <label for="input_atp_no">ATP No.</label>
              <input type="text" id="input_atp_no" class="form-control" name="atp_no" value="{{old('atp_no', $transaction->atp_no)}}">
              @if($errors->first('atp_no'))
              <p class="help-block text-danger">{{$errors->first('atp_no')}}</p>
              @endif
            </div>

            <div class="form-group {{$errors->first('printer_accreditation') ? 'text-danger' : NULL}}" id="printer_container">
              <label for="input_printer_accreditation">Printer Accreditation No.</label>
              <input type="text" id="input_printer_accreditation" class="form-control" name="printer_accreditation" value="{{old('printer_accreditation', $transaction->printer_accreditation)}}">
              @if($errors->first('printer_accreditation'))
              <p class="help-block text-danger">{{$errors->first('printer_accreditation')}}</p>
              @endif
            </div>

            <div class="form-group {{$errors->first('eor_booklet_qty') ? 'text-danger' : NULL}}" id="booklet_container">
              <label for="input_eor_booklet_qty">No. of Booklet (x 50)</label>
              <input type="number" id="input_eor_booklet_qty" class="form-control" name="eor_booklet_qty" value="{{old('eor_booklet_qty')}}" maxlength="7">
              @if($errors->first('eor_booklet_qty'))
              <p class="help-block text-danger">{{$errors->first('eor_booklet_qty')}}</p>
              @endif
            </div>

            <div class="form-group {{$errors->first('eor_online_qty') ? 'text-danger' : NULL}}" id="eor_container">
              <label for="input_eor_online_qty">No. of eOR Series (Min. 1000)</label>
              <input type="number" id="input_eor_online_qty" class="form-control" name="eor_online_qty" value="{{old('eor_online_qty')}}" maxlength="7">
              @if($errors->first('eor_online_qty'))
              <p class="help-block text-danger">{{$errors->first('eor_online_qty')}}</p>
              @endif
            </div>

            <div class="form-group">
              <a href="{{route('admin.transaction.index')}}" class="btn btn-secondary">Go back to Transaction List</a>
              <button type="submit" class="btn  btn-primary">Submit</button>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
@stop

@section('page-scripts')
<script type="text/javascript">
  $(function(){

    $('#layout_container').hide();
    $('#atp_container').hide();
    $('#printer_container').hide();
    $('#booklet_container').hide();
    $('#eor_container').hide();

    $('#input_type').on('change', function(){
      var _val = $(this).val();

      if(_val == 'booklet'){
        $('#layout_container').show();
        $('#atp_container').show();
        $('#printer_container').show();
        $('#booklet_container').show();
        $('#eor_container').hide();
      } else {
        $('#layout_container').hide();
        $('#atp_container').hide();
        $('#printer_container').hide();
        $('#booklet_container').hide();
        $('#eor_container').show();
      }
    });

    @if(strlen(old('type')) > 0)
      $('#input_type').change();
    @endif
  });
</script>
@stop