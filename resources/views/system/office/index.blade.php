@extends('system._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Office</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
  </ul> --}}
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-8">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Record Data </h3>
            <span class="float-right">
              <a href="{{route('admin.office.create')}}" class="btn btn-primary">Add Office</a>
            </span>
        </div>
        <div class="card-body">
            @include('system.components.notifications')
          
            <div class="table-responsive">
              <table class="table table-bordered text-center">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Office Code</th>
                        <th scope="col">Name</th>
                        <th scope="col">Address</th>
                        <th scope="col">Merchant</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($offices as $index => $office)
                    <tr>
                        <td>{{$office->code}}</td>
                        <td>{{$office->name}}</td>
                        <td>{{$office->address}}</td>
                        <td>{{$office->merchant ? $office->merchant->merchant_name : ''}}</td>
                        <td>
                            <div class="btn-group">
                              <button type="button" data-toggle="dropdown" class="btn btn-sm btn-secondary dropdown-toggle" aria-expanded="false">Actions <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                              <div class="dropdown-menu">
                                  <a class="dropdown-item" href="{{route('admin.office.edit',[$office->id])}}">Edit Details</a>
                              </div>
                            </div>
                        </td>
                    </tr>
                    @empty
                    <tr>
                      <td colspan="6"><div class="text-center"><i>No records found yet.</i></div></td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
            </div>
        </div>
    </div>
  </div>
</div>
@stop