@extends('system._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Office</h1>
  <ul>
      <li>Office</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Office Form</h3>
        </div>
        <div class="card-body">
          @include('system.components.notifications')
          <form action="" method="POST">
            {!!csrf_field()!!}
            <div class="form-group">
                <label for="input_merchant_id"><b>Merchant</b></label>
                {!!Form::select('merchant_id',$merchants,old('merchant_id'),['class' => "form-control",'id' => "input_merchant_id"])!!}
                @if($errors->first('merchant_id'))
                <p class="form-text text-danger">{{$errors->first('merchant_id')}}</p>
                @endif
            </div>
            <div class="form-group">
                <label for="input_code"><b>Office Code</b></label>
                <input type="text" class="form-control" id="input_code" placeholder="" value="{{old('code')}}" name="code">
                @if($errors->first('code'))
                <p class="form-text text-danger">{{$errors->first('code')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_name"><b>Office Name</b></label>
                <input type="text" class="form-control" id="input_name" placeholder="" value="{{old('name')}}" name="name">
                @if($errors->first('name'))
                <p class="form-text text-danger">{{$errors->first('name')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_address"><b>Office Address</b></label>
                <input type="text" class="form-control" id="input_address" placeholder="" value="{{old('address')}}" name="address">
                @if($errors->first('address'))
                <p class="form-text text-danger">{{$errors->first('address')}}</p>
                @endif
            </div>

            <div class="form-group">
              <a href="{{route('admin.office.index')}}" class="btn btn-secondary">Go back to Office List</a>
              <button type="submit" class="btn  btn-primary">Submit</button>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
@stop