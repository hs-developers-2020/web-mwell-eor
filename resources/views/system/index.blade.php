@extends('system._layouts.main')
@section('styles')
    <style>
      .dashboard-card {
        box-sizing: border-box;
        padding: 0;
        margin-right: 5px;
        margin-left: 5;
        border-radius: 5px;
      }
      .dashboard-card .card-body{
        height: 100px;
        border-radius: 5px;
        color: white;
        margin-bottom: 10px;
        font-size: 20px;
      }
      .bg-mustard{
        background-color:rgb(248, 193, 70);
      }
      .bg-maroon{
        background-color:rgb(195, 74, 74);
      }

      .bg-blue{
        background-color: rgb(62, 170, 245);
      }

      .bg-green{
        background-color: rgb(139, 195, 74);
      }

      .bg-violet{
        background-color: rgb(190, 74, 195);
      }

    </style>
@endsection
@section('breadcrumb')
<div class="breadcrumb">
  <h1>Dashboard</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
  </ul> --}}
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
  <div class="col-md-12">
    @include('system.components.notifications')
    <h3 class="text-muted font-weight-bold">Dashboard</h3>
    <p class="text-muted font-weight-bold" style="font-size:18px;">Access the portal relevant information on this page</p>
    <h4 class="text-muted mt-5 font-weight-bold">Quick Links</h4>
    <div class="row row-cols-md-1 row-cols-lg-5 mt-3">
      @if(in_array($user->type, ['super_user', 'admin', 'user']))
      <div class="dashboard-card col">
        <div class="card-body bg-mustard">
          <a href="{{ route('admin.transaction.index') }}" style="color: white;">
            <div class="d-flex justify-content-between">
              <i class="i-File float-left" style="font-size:50px"></i>
              <span class="text-right">{{ $transactions }} <br>Transactions</span>
            </div>
          </a>
        </div>
      </div>
      @endif
      <div class="dashboard-card col">
        <div class="card-body bg-blue">
          @if(in_array($user->type, ['super_user', 'admin', 'staff']))
            <a href="{{ route('admin.merchant.index') }}" style="color: white;">
              <div class="d-flex justify-content-between">
                <i class="i-Clothing-Store float-left" style="font-size:50px"></i>
                <span class="text-right">{{ $merchants }} <br>Merchant</span>
              </div>
            </a>
          @endif
          @if(in_array($user->type, ['user']))
            <a href="{{ route('admin.business.index') }}" style="color: white;">
              <div class="d-flex justify-content-between">
                <i class="i-Clothing-Store float-left" style="font-size:50px"></i>
                <span class="text-right">{{ $merchants }} <br>Business</span>
              </div>
            </a>
          @endif
        </div>
      </div>
      @if(in_array($user->type, ['super_user', 'admin', 'staff']))
      <div class="dashboard-card col">
        <div class="card-body bg-green">
          <a href="{{ route('admin.registrant.index') }}" style="color: white;">
            <div class="d-flex justify-content-between">
              <i class="i-Business-Mens float-left" style="font-size:50px"></i>
              <span class="text-right">{{ $registrants }} <br>Registrants</span>
            </div>
          </a>
        </div>
      </div>
      @endif
      <div class="dashboard-card col">
        
      </div>
      <div class="dashboard-card col">
        
      </div>
    </div>
    <h4 class="text-muted mt-3 font-weight-bold">Contact Us</h4>
    <h5 class="text-muted mt-2 font-weight-bold">For question and concerns, contact us through our customer email:</h5>

    <div class="mt-2 d-flex justify-content-lg-start">
      <i class="i-Mail text-warning mt-1" style="font-size:25px;"></i> <span class="text-warning ml-2" style="font-size:20px;">email@gmail.com</span>
    </div>
  </div>
{{-- <div class="row">
  @if(session()->get('is_admin','no') == "yes")
  <div class="col-lg-3 col-md-6 col-sm-6">
    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
      <div class="card-body text-center">
        <i class="i-Business-Mens"></i>
        <div class="content">
            <p class="text-muted mt-2 mb-0">Partners</p>
            <p class="text-primary text-24 line-height-1 mb-2">{{Helper::nice_number($total_partners)}}</p>
        </div>
      </div>
    </div>
  </div>
  @else
  <div class="col-lg-3 col-md-6 col-sm-6">
    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
      <div class="card-body text-center">
        <i class="i-Financial"></i>
        <div class="content">
            <p class="text-muted mt-2 mb-0">Today</p>
            <p class="text-primary text-24 line-height-1 mb-2">₱ {{Helper::format_num($today_sales)}}</p>
        </div>
      </div>
    </div>
  </div>
  @endif
  <div class="col-lg-3 col-md-6 col-sm-6">
    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
      <div class="card-body text-center">
        <i class="i-Financial"></i>
        <div class="content">
            <p class="text-muted mt-2 mb-0">Yesterday</p>
            <p class="text-primary text-24 line-height-1 mb-2">₱ {{Helper::format_num($yesterday_sales)}}</p>
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-3 col-md-6 col-sm-6">
    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
      <div class="card-body text-center">
        <i class="i-Money-2"></i>
        <div class="content">
            <p class="text-muted mt-2 mb-0">{{Carbon::now()->format("M Y")}}</p>
            <p class="text-primary text-24 line-height-1 mb-2">₱ {{Helper::format_num($this_month)}}</p>
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-3 col-md-6 col-sm-6">
    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
      <div class="card-body text-center">
        <i class="i-Money-2"></i>
        <div class="content">
            <p class="text-muted mt-2 mb-0">This Week</p>
            <p class="text-primary text-24 line-height-1 mb-2">₱ {{Helper::format_num($this_week)}}</p>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
    <div class="col-lg-8 col-md-12">
        <div class="card mb-4">
            <div class="card-body">
                <div class="card-title">This Year Sales</div>
                <div id="echartBar" style="height: 300px;"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-sm-12">
        <div class="card mb-4">
            <div class="card-body">
                <div class="card-title">Sales by Countries</div>
                <div id="echartPie" style="height: 300px;"></div>
            </div>
        </div>
    </div>
</div> 

<div class="row">
    <div class="col-lg-8 col-md-12">
        <div class="card mb-4">
            <div class="card-body">
                <div class="card-title">This Week Sales</div>
                <div id="echartBar" style="height: 300px;"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-sm-12">
        <div class="card mb-4">
            <div class="card-body">
                <div class="card-title">Sales by Products</div>
                <div id="echartPie" style="height: 300px;"></div>
            </div>
        </div>
    </div> 
</div> --}}
@stop

@section('page-scripts')
<script type="text/javascript">
  $(function(){

  })
</script>
@stop