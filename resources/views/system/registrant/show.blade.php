@extends('system._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Registrant Details</h1>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
@include('system.components.notifications')
<div class="row">
  <div class="col-md-5">
    <div class="card mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Registrant Details</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="display table table-striped table-bordered">
                <tbody>
                  <tr>
                    <td>Name</td>
                    <td>{{ $registrant->name }}</td>
                  </tr>
                  <tr>
                    <td>Email</td>
                    <td>{{ $registrant->email }}</td>
                  </tr>
                  <tr>
                    <td>Date Registered</td>
                    <td>{{ Helper::date_format($registrant->created_at, 'd M Y h:i A') }}</td>
                  </tr>
                </tbody>
            </table>
            @if($registrant->status == "pending")
            <a href="#" class="btn btn-sm btn-success" id="btn_approve" data-url="{{ route('admin.registrant.activate', $registrant->id) }}">Approve Account</a>
            <a type="button" class="btn btn-sm btn-danger" id="btn_decline" style="color: white;" data-url="{{ route('admin.registrant.decline', $registrant->id) }}">Decline Account</a>
            @endif
          </div>
        </div>
    </div>
  </div>
  <div class="col-md-7">
    <div class="card mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Submitted Documents</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="display table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Document</th>
                  <th>Status</th>
                  <th>Date Added</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @forelse($documents as $index => $document)
                <tr>
                  <td><a target="_blank" href="{{$document->source == "file" ? asset("{$document->path}/{$document->created_at->format("Ymd")}/{$document->filename}") : "{$document->directory}/{$document->filename}"}}"><u>{{$document->filename}}</u></a></td>
                  <th>{{ strtoupper($document->status) }}</th>
                  <td>{{$document->created_at->format("d F d h:i  A")}}</td>
                  <td>
                      @if($document->status == 'pending')
                      <div class="btn-group">
                        <button type="button" data-toggle="dropdown" class="btn btn-sm btn-secondary dropdown-toggle" aria-expanded="false">Actions <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                        <div class="dropdown-menu">
                          <a class="dropdown-item action-delete" href="#" data-url="{{ route('admin.registrant.update_status') }}?registrant_id={{$registrant->id}}&document_id={{$document->id}}&status=approved" data-toggle="modal" data-target="#confirm-delete" title="Decline Record">Approve</a>
                          <a class="dropdown-item action-delete" href="#" data-url="{{ route('admin.registrant.update_status') }}?registrant_id={{$registrant->id}}&document_id={{$document->id}}&status=declined" data-toggle="modal" data-target="#confirm-delete" title="Decline Record">Decline</a>
                        </div>
                      </div>
                      @else
                        ---
                      @endif
                  </td>
                </tr>

                @empty
                <tr>
                  <td colspan="3">
                    <p>No record found.</p>
                  </td>
                </tr>
                @endforelse
              </tbody>
            </table>
          </div>
        </div>
    </div>
  </div>
</div>
@stop

@section('page-modals')
<div id="confirm-delete" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        {{-- <button type="button" class="close" data-dismiss="modal">&times;</button> --}}
        <h1 class="modal-title">Confirm your action</h1>
      </div>

      <div class="modal-body">
        <div role="alert" class="alert alert-warning alert-icon alert-icon-border alert-dismissible">
          <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
          <div class="message">
            <strong>Warning!</strong> This action can not be undone.
          </div>
        </div>
        <h3 class="text-semibold">Are you sure?</h3>
        <p>You are about to update the status this document, are you sure you want to proceed?</strong></p>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        <a href="#" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Verifying record ..." class="btn btn-primary btn-raised btn-loading" id="btn-confirm-delete">Proceed</a>
      </div>
    </div>
  </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<link rel="stylesheet" href="{{asset('assets/js/vendor/sweet-alert2/sweetalert2.min.css')}}">
<style type="text/css" media="screen">
  .qrcode{ height: 150px;  margin: 0px auto;}  
  .swal2-container .swal2-styled:not(.swal2-cancel) {
      background: #3085d6 !important;
      outline: none;
  }

  .swal2-footer{
    text-align: justify;
    font-size: 11px;
  }
</style>
@stop

@section('page-scripts')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
<script src="{{asset('assets/js/vendor/sweet-alert2/sweetalert2.min.js')}}"></script>
<script type="text/javascript">
  $(function(){
    $(".action-delete").on("click",function(){
      var btn = $(this);
      $("#btn-confirm-delete").attr({"href" : btn.data('url')});
    });

    $('#btn-confirm-delete').on('click', function() {
      $('.btn-link').hide();
      $('.btn-loading').button('loading');
      $('#target').submit();
    });

    $('#btn_decline').on('click', function($table){
      var url = $(this).data('url');
      //Warning Message
      var self = $(this)
      Swal.fire({
          icon: "question",   
          title: "You are about to decline this request?",
          text: "You will not be able to undo this action, proceed?",
          footer: "Please make sure that you have validated the submitted documents if there's any. Note that once you decline this account request, the pending documents attached will be also be decline.",
          input: 'text',
          inputPlaceholder: "Put remarks for declining this request",
          showCancelButton: true,
          confirmButtonText: 'Decline',
          inputValidator: (value) => {
            if (!value) {
              return 'You need to write something!'
            }
          }
      }).then((result) => {
        if (result.value) {
          window.location.href = url + "?remarks=" + result.value;
        }
      })
    });
      
    $('#btn_approve').on('click', function($table){
      var url = $(this).data('url');
      //Warning Message
      Swal.fire({
          title: "Are you sure?",   
          text: "You want to approve the account request.",   
          footer: "Please make sure that you have validated the submitted documents if there's any. Note that once you approved this account request, the pending documents attached will be also be approved.",
          icon: "question",   
          showCancelButton: true,
          backdrop: false,
          allowOutsideClick: false,
          confirmButtonText: "Approve Request!"
      }).then((result) => {
        if (result.value) {
          window.location.href = url;
        }
      })
    });
  })
</script>
@stop