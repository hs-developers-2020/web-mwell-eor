@extends('system._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Teller</h1>
  <ul>
      <li>Teller</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Teller Form</h3>
        </div>
        <div class="card-body">
          @include('system.components.notifications')
          <form action="" method="POST">
            {!!csrf_field()!!}
            <div class="form-group">
                <label for="input_merchant"><b>Merchant</b></label>
                {!!Form::select('merchant_id',$merchants,old('merchant_id'),['class' => "form-control",'id' => "input_merchant"])!!}
                @if($errors->first('merchant_id'))
                <p class="form-text text-danger">{{$errors->first('merchant_id')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_office"><b>Office</b></label>
                {!!Form::select('office_code',$offices,old('office_code'),['class' => "form-control",'id' => "input_office"])!!}
                <input type="hidden" value="{{old('office_code')}}" id="input_office_code" />
                @if($errors->first('office_code'))
                <p class="form-text text-danger">{{$errors->first('office_code')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_code"><b>Code</b></label>
                <input type="text" class="form-control" id="input_code" placeholder="" value="{{old('code')}}" name="code">
                @if($errors->first('code'))
                <p class="form-text text-danger">{{$errors->first('code')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_fname"><b>First Name</b></label>
                <input type="text" class="form-control" id="input_fname" placeholder="" value="{{old('fname')}}" name="fname">
                @if($errors->first('fname'))
                <p class="form-text text-danger">{{$errors->first('fname')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_mname"><b>Middle Name</b></label>
                <input type="text" class="form-control" id="input_mname" placeholder="" value="{{old('mname')}}" name="mname">
                @if($errors->first('mname'))
                <p class="form-text text-danger">{{$errors->first('mname')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_lname"><b>Last Name</b></label>
                <input type="text" class="form-control" id="input_lname" placeholder="" value="{{old('lname')}}" name="lname">
                @if($errors->first('lname'))
                <p class="form-text text-danger">{{$errors->first('lname')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_pin_code"><b>Pin Code</b></label>
                <input type="text" class="form-control" id="input_pin_code" placeholder="" value="{{old('pin_code')}}" name="pin_code">
                @if($errors->first('pin_code'))
                <p class="form-text text-danger">{{$errors->first('pin_code')}}</p>
                @endif
            </div>

            <div class="form-group">
              <a href="{{route('admin.teller.index')}}" class="btn btn-secondary">Go back to Teller List</a>
              <button type="submit" class="btn  btn-primary">Submit</button>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
@stop

@section('page-scripts')
<script type="text/javascript">
    $('#input_merchant').on('change', function(){
        $.ajax({
            url : "{{route('admin.get_office')}}",
            dataType : "json",
            data: {merchant_id : $(this).val()},
            type: "GET",
            success : function(data){
                if(data.offices){
                    $('#input_office').empty()
                    $('#input_office').append('<option value="">-- Choose Office --</option>');
                    $.each(data.offices,function(i, office){
                        if($('#input_office_code').val() == office.code)
                            $('#input_office').append('<option value="' + office.code +'" selected="selected">' + office.name +"</option>");
                        else
                            $('#input_office').append('<option value="' + office.code +'">' + office.name +"</option>");
                    });
                }else {

                }
            },  
            error : function(jqXHR,textStatus,thrownError){
            }
        });
    });

    @if(old('merchant_id'))
        $('#input_merchant').change();
    @endif
</script>
@endsection