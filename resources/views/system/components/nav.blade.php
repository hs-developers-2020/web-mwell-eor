<div class="side-content-wrap">
  <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
  <ul class="navigation-left">
    @if(in_array($user->type, ['super_user', 'admin', 'user', 'staff']))
      <li class="nav-item {{ Request::segment(2) == NULL ? 'active' : '' }}">
          <a class="nav-item-hold" href="{{route('admin.index')}}">
              <i class="nav-icon i-Bar-Chart"></i>
              <span class="nav-text">Dashboard</span>
          </a>
          <div class="triangle"></div>
      </li>
    @endif

    @if(in_array($user->type, ['super_user', 'admin', 'user']))
      <li class="nav-item {{ Request::segment(2) == "transaction" ? 'active' : '' }}">
          <a class="nav-item-hold" href="{{route('admin.transaction.index')}}">
              <i class="nav-icon i-File"></i>
              <span class="nav-text">Official Receipt</span>
          </a>
          <div class="triangle"></div>
      </li>
    @endif

    @if(in_array($user->type, ['super_user', 'admin', 'staff']))
      <li class="nav-item {{ Request::segment(2) == "registrant" ? 'active' : '' }}">
          <a class="nav-item-hold" href="{{route('admin.registrant.index')}}">
              <i class="nav-icon i-Business-Mens"></i>
              <span class="nav-text">Registrants</span>
          </a>
          <div class="triangle"></div>
      </li>
    @endif
      
    @if(in_array($user->type, ['super_user', 'admin']))
      <li class="nav-item {{ Request::segment(2) == "account" ? 'active' : '' }}">
          <a class="nav-item-hold" href="{{route('admin.account.index')}}">
              <i class="nav-icon i-Checked-User"></i>
              <span class="nav-text">Accounts</span>
          </a>
          <div class="triangle"></div>
      </li>
    @endif

    @if(in_array($user->type, ['super_user', 'admin']))
      <li class="nav-item {{ Request::segment(2) == "office" ? 'active' : '' }}">
          <a class="nav-item-hold" href="{{route('admin.office.index')}}">
              <i class="nav-icon i-Building"></i>
              <span class="nav-text">Office</span>
          </a>
          <div class="triangle"></div>
      </li>

      <li class="nav-item {{ Request::segment(2) == "teller" ? 'active' : '' }}">
          <a class="nav-item-hold" href="{{route('admin.teller.index')}}">
              <i class="nav-icon i-Business-Man"></i>
              <span class="nav-text">Teller</span>
          </a>
          <div class="triangle"></div>
      </li>
    @endif

    @if(in_array($user->type, ['super_user', 'admin', 'staff']))
      <li class="nav-item {{ Request::segment(2) == "merchant" ? 'active' : '' }}">
          <a class="nav-item-hold" href="{{route('admin.merchant.index')}}">
              <i class="nav-icon i-Clothing-Store"></i>
              <span class="nav-text">Merchant</span>
          </a>
          <div class="triangle"></div>
      </li>
    @endif

    @if(in_array($user->type, ['super_user', 'admin']))
      <li class="nav-item {{ Request::segment(2) == "terminal" ? 'active' : '' }}">
          <a class="nav-item-hold" href="{{route('admin.terminal.index')}}">
              <i class="nav-icon i-Billing"></i>
              <span class="nav-text">Terminal</span>
          </a>
          <div class="triangle"></div>
      </li>

      <li class="nav-item {{ Request::segment(2) == "system-account" ? 'active' : '' }}">
          <a class="nav-item-hold" href="{{route('admin.user.index')}}">
              <i class="nav-icon i-Boy"></i>
              <span class="nav-text">System Account</span>
          </a>
          <div class="triangle"></div>
      </li>

      <li class="nav-item {{ Request::segment(2) == "settings" ? 'active' : '' }}">
          <a class="nav-item-hold" href="{{route('admin.setting.index')}}">
              <i class="nav-icon i-Data-Settings"></i>
              <span class="nav-text">Settings</span>
          </a>
          <div class="triangle"></div>
      </li>
    @endif

    @if(in_array($user->type, ['user']))
      {{-- <li class="nav-item">
          <a class="nav-item-hold" href="{{route('admin.ledger.index')}}">
              <i class="nav-icon i-Receipt"></i>
              <span class="nav-text">Ledger</span>
          </a>
          <div class="triangle"></div>
      </li> --}}
      <li class="nav-item ">
          <a class="nav-item-hold" href="{{route('admin.business.index')}}">
              <i class="nav-icon i-Clothing-Store"></i>
              <span class="nav-text">Business</span>
          </a>
          <div class="triangle"></div>
      </li>
    @endif
  </ul>
</div>
<div class="sidebar-overlay"></div>