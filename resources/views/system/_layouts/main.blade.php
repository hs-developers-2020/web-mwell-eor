<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{csrf_token()}}">
        <title>{{env('APP_NAME')}}</title>
        <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">
                
        <link id="nooklifestylestore" rel="stylesheet" href="{{ asset('assets/styles/css/themes/lite-purple.min.css', env('SECURE_ASSET',FALSE))}}">
        <link rel="stylesheet" href="{{asset('assets/styles/vendor/perfect-scrollbar.css',env('SECURE_ASSET',FALSE))}}">
        @yield('page-styles')
        <style>

          a {
              color: #333;
          }
          .card-icon-bg .card-body .content { max-width: 100px; }
        
          .text-primary {
              color: #fd8a75 !important;
          }
          .card-icon-bg-primary [class^=i-] {
              color: rgba(0, 0, 0, 0.28);
          }

          .btn-primary, .btn-outline-primary {
              border-color: #fd8a75;
          }
          .btn-primary,.btn-primary:hover {
              color: #fff;
              background-color: #fd8a75;
              border-color: #fd8a75;
          }
        </style>
        
    </head>


    <body class="text-left">
        {{-- <!-- Pre Loader Strat  -->
        <div class='loadscreen' id="preloader">
            <div class="loader spinner-bubble spinner-bubble-primary">
            </div>
        </div>
        <!-- Pre Loader end  --> --}}
        <div class="app-admin-wrap layout-sidebar-large clearfix">
          @include('system.components.header')
          @include('system.components.nav')
        </div>        

        <div class="main-content-wrap sidenav-open d-flex flex-column">
          <div class="main-content">
            @yield('breadcrumb')
            @yield('content')
          </div>

          <div class="flex-grow-1"></div>
          <div class="app-footer">
              <div class="footer-bottom d-flex flex-column flex-sm-row align-items-center">
                  <span class="flex-grow-1"></span>
                  <div class="d-flex align-items-center">
                      {{-- <img class="logo" src="{{asset('assets/images/logo.png')}}" alt=""> --}}
                      <div>
                          <p class="m-0">&copy; {{ Carbon::now()->format('Y') }} {{env('APP_NAME')}}</p>
                          <p class="m-0">All rights reserved</p>
                      </div>
                  </div>
              </div>
          </div>
        </div>
        @yield('page-modals')
        <script src="{{asset('assets/js/common-bundle-script.js',env('SECURE_ASSET',FALSE))}}"></script>
        <script src="{{asset('assets/js/vendor/echarts.min.js',env('SECURE_ASSET',FALSE))}}"></script>
        <script src="{{asset('assets/js/es5/echart.options.min.js',env('SECURE_ASSET',FALSE))}}"></script>
        <script src="{{asset('assets/js/script.js',env('SECURE_ASSET',FALSE))}}"></script>
        <script src="{{asset('assets/js/sidebar.large.script.js',env('SECURE_ASSET',FALSE))}}"></script>
        <script src="{{asset('assets/js/customizer.script.js',env('SECURE_ASSET',FALSE))}}"></script>
        @yield('page-scripts')
        @yield('styles')
    </body>

</html>