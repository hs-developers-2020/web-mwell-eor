<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{env('APP_NAME')}} - Login</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/styles/css/themes/lite-purple.min.css',env('SECURE_ASSET',FALSE))}}">
    <style>
        a {
            color: #333;
        }
        .text-primary {
            color: #fd8a75 !important;
        }
        .btn-primary, .btn-outline-primary {
              border-color: #fd8a75;
        }

        .btn-primary,.btn-primary:hover {
          color: #fff;
          background-color: #fd8a75;
          border-color: #fd8a75;
        }
        html body {
            margin: 0;
            padding: 0;
        }

        .login-content{
            height: 980px;
        }
        
        .form-control{
            outline: none;
            border: none;
            background: white;
            font-size: 14px;
            border-bottom: 1px solid grey;
        }

        .form-control::placeholder{
            color: rgb(131, 125, 125);
            font-size: 14px;
        }

        .form-control:focus{
            box-shadow: 0 0 0 0.2rem #f1d96c !important;
            border-bottom: white;
            border-bottom: none;
        }

        .remember-me{
            width:20px; 
            height:20px; 
        }
    </style>
</head>

<body>
    <div class="auth-layout-wrap m-0 p-0">
        @yield('content')
    </div>

    <script src="{{asset('assets/js/common-bundle-script.js',env('SECURE_ASSET',FALSE))}}"></script>
    <script src="{{asset('assets/js/script.js',env('SECURE_ASSET',FALSE))}}"></script>
    @yield('page-scripts')
</body>

</html>
