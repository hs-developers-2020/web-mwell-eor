@extends('system._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Settings</h1>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Record Data </h3>
            <span class="float-right">
              <a href="{{route('admin.setting.create')}}" class="btn btn-primary">Update Settings</a>
            </span>
        </div>
        <div class="card-body">
            @include('system.components.notifications')
          
            <div class="table-responsive">
              <table class="table table-bordered text-center">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Min Online Series</th>
                        <th scope="col">Max Online Series</th>
                        <th scope="col">Min Booklet Series</th>
                        <th scope="col">Max Booklet Series</th>
                        <th scope="col">Last Updated</th>
                       
                    </tr>
                </thead>
                <tbody>
                    @forelse($settings as $index => $setting)
                    <tr>
                        <td class="text-center"><b>{{$setting->min_online}}</b> </td>
                        <td class="text-center"><b>{{$setting->max_online}}</b> </td>
                        <td class="text-center"><b>{{$setting->min_booklet}}</b> </td>
                        <td class="text-center"><b>{{$setting->max_booklet}}</b> </td>
                        <td>
                            <div>{{Helper::date_format($setting->updated_at,"m/d/Y")}}</div>
                            <div><small>{{Helper::date_format($setting->updated_at,"h:i A")}}</small></div>
                        </td>
                    </tr>
                    @empty
                        <tr>
                          <td class="text-center"><b>1000</b></td>
                          <td class="text-center"><b>1000</b></td>
                          <td class="text-center"><b>5</b></td>
                          <td class="text-center"><b>5</b></td>
                          <td class="text-left">-</td>
                        </tr>
                    @endforelse
                  </tbody>
            </table>
            </div>
        </div>
    </div>
  </div>
</div>
@stop