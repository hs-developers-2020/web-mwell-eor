@extends('system._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Settings</h1>
  <ul>
      <li>Update Settings</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Update Form</h3>
        </div>
        <div class="card-body">
          @include('system.components.notifications')
          <form action="" method="POST">
            {!!csrf_field()!!}

            <div class="form-group">
                <label for="input_min_online"><b>Min. Online Series</b></label>
                <input type="text" class="form-control" id="input_min_online" placeholder="" value="{{old('min_online', $current_min_online)}}" name="min_online">
                @if($errors->first('min_online'))
                <p class="form-text text-danger">{{$errors->first('min_online')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_max_online"><b>Max. Online Series</b></label>
                <input type="text" class="form-control" id="input_max_online" placeholder="" value="{{old('max_online', $current_max_online)}}" name="max_online">
                @if($errors->first('max_online'))
                <p class="form-text text-danger">{{$errors->first('max_online')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_min_booklet"><b>Min. Booklet Series (x50)</b></label>
                <input type="text" class="form-control" id="input_min_booklet" placeholder="" value="{{old('min_booklet', $current_min_booklet)}}" name="min_booklet">
                @if($errors->first('min_booklet'))
                <p class="form-text text-danger">{{$errors->first('min_booklet')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_max_booklet"><b>Max. Booklet Series (x50)</b></label>
                <input type="text" class="form-control" id="input_max_booklet" placeholder="" value="{{old('max_booklet', $current_max_booklet)}}" name="max_booklet">
                @if($errors->first('max_booklet'))
                <p class="form-text text-danger">{{$errors->first('max_booklet')}}</p>
                @endif
            </div>

            <div class="form-group">
              <a href="{{route('admin.setting.index')}}" class="btn btn-secondary">Go back to Setting List</a>
              <button type="submit" class="btn  btn-primary">Submit</button>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
@stop