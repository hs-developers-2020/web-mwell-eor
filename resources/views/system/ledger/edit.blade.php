@extends('system._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Edit Booklet OR Series</h1>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Update Form</h3>
        </div>
        <div class="card-body">
          @include('system.components.notifications')
          <form action="" method="POST">
            {!!csrf_field()!!}

            <div class="form-group">
                <label for="input_series_code"><b>Series Code</b></label>
                <input type="text" class="form-control" id="input_series_code" placeholder="" value="{{old('series_code', $series->series_code)}}" name="series_code" readonly>
                @if($errors->first('series_code'))
                <p class="form-text text-danger">{{$errors->first('series_code')}}</p>
                @endif
            </div>

            {{-- <div class="form-group">
                <label for="input_or_code"><b>OR</b></label>
                <input type="text" class="form-control" name="or_code" value="{{ old('or_code') }}">
                @if($errors->first('or_code'))
                <p class="form-text text-danger">{{$errors->first('or_code')}}</p>
                @endif
            </div> --}}

            <div class="form-group">
                <label for="input_transaction_date"><b>Issued Date</b></label>
                <input type="text" class="form-control datepicker" placeholder="Issued Date" name="transaction_date" value="{{ old('transaction_date', $series->transaction_date ? $series->transaction_date->format("Y-m-d") : '') }}">
                @if($errors->first('transaction_date'))
                <p class="form-text text-danger">{{$errors->first('transaction_date')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_amount"><b>Amount</b></label>
                <input type="numeric" class="form-control" id="input_amount" placeholder="" value="{{old('amount', $series->amount)}}" name="amount" min="0" max="100000000000">
                @if($errors->first('amount'))
                <p class="form-text text-danger">{{$errors->first('amount')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_name"><b>Customer Name</b></label>
                <input type="text" class="form-control" id="input_name" placeholder="" value="{{old('name', $series->name)}}" name="name">
                @if($errors->first('name'))
                <p class="form-text text-danger">{{$errors->first('name')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_tin"><b>Customer TIN</b></label>
                <input type="text" class="form-control" id="input_tin" placeholder="000-000-000-000" value="{{old('tin', $series->tin)}}" name="tin">
                @if($errors->first('tin'))
                <p class="form-text text-danger">{{$errors->first('tin')}}</p>
                @endif
            </div>

            <div class="form-group">
              <a href="{{route('admin.ledger.booklet', [$series->merchant_code])}}" class="btn btn-secondary">Go back to List</a>
              <button type="submit" class="btn  btn-primary">Submit</button>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css',env('SECURE_ASSET',FALSE))}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css',env('SECURE_ASSET',FALSE))}}">
@stop

@section('page-scripts')
<script src="{{asset('assets/js/vendor/pickadate/picker.js',env('SECURE_ASSET',FALSE))}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js',env('SECURE_ASSET',FALSE))}}"></script>
<script type="text/javascript">
  $(function(){

    $(".datepicker").pickadate({
      format: 'mm/dd/yyyy',
      max: new Date()
    });

  })
</script>
@stop