@extends('system._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Ledger</h1>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card mb-5">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Advance Filters </h3>
            <span class="float-right">
              <a href="{{route('admin.ledger.booklet', [$merchant->merchant_code])}}" class="btn btn-default btn-sm">[Reset Filter]</a>

            </span>
        </div>
        <form action="" method="GET">
        <div class="card-body">
            <div class="row row-xs mb-4">
              <div class="col-md-4">
                <label for="">Keyword</label>
                <input type="text" class="form-control" placeholder="Keyword" name="keyword" value="{{$keyword}}">
              </div>
              <div class="col-md-4">
                <label>Status</label>
                {!!Form::select('status',$statuses,old('status', $status),['class' => "form-control",'id' => "input_status"])!!}
              </div>
              {{-- <div class="col-md-3">
                  <label for="">Transaction Date Range</label>
                  <input type="text" class="form-control datepicker" placeholder="Start Date" name="start_date" value="{{$start_date}}">
              </div>
              <div class="col-md-3 mt-3 mt-md-0">
                  <label for="">&nbsp;</label>
                  <input type="text" class="form-control datepicker" placeholder="End Date" name="end_date" value="{{$end_date}}">
              </div> --}}
              <div class="col-md-2 mt-3 mt-md-0">
                  <label for="">&nbsp;</label>
                  <button type="submit" class="btn btn-primary btn-block">Apply Filters</button>
              </div>
            </div>
        </div>
        </form>
    </div>
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">
              Record Data {{--  {{$start_date == $end_date ? Carbon::parse($start_date)->format("M. d Y (D)") : Carbon::parse($start_date)->format("M. d Y (D)")." - ".Carbon::parse($end_date)->format("M. d Y (D)")}} --}}
            </h3>
            <span class="float-right">
              <a href="{{route('admin.ledger.export')}}?keyword={{$keyword}}&status={{$status}}&code={{$merchant->merchant_code}}&type=booklet&report_type=excel" class="btn btn-primary">Export to Excel</a>
            </span>
        </div>
        <div class="card-body">
            @include('system.components.notifications')
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                  <thead class="thead-dark">
                      <tr>
                          <th>Transaction Date</th>
                          <th>Date Expiry</th>
                          <th>Series</th>
                          <th>Amount</th>
                          <th></th>
                      </tr>
                  </thead>
                  <tbody>
                    @forelse($transactions as $index => $transaction)
                    <tr>
                      <td>
                        <div>{{ $transaction->transaction_date ? $transaction->transaction_date->format("d M Y") : '---' }}</div>
                      </td>
                      <td>
                        <div>{{ $transaction->date_expiry ? $transaction->date_expiry->format("d M Y") : '---' }}</div>
                      </td>
                      <td>
                        <div><strong>{{ $transaction->series_code }}</strong></div>
                      </td>
                      <td>₱ {{ $transaction->amount ? Helper::money_format($transaction->amount) : '---' }}</td>
                      <td>
                          {{-- @if($transaction->status == 0) --}}
                          <div class="btn-group">
                            <button type="button" data-toggle="dropdown" class="btn btn-sm btn-secondary dropdown-toggle" aria-expanded="false">Actions <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{route('admin.ledger.edit',[$merchant->merchant_code, $transaction->series_code])}}">Edit</a>
                            </div>
                          </div>
                          {{-- @endif --}}
                      </td>
                    </tr>
                    @empty
                    <tr>
                      <td colspan="7" class="text-center">No available data</td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
                {!! $transactions->appends(request()->query())->render() !!}
            </div>
        </div>
    </div>
  </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css',env('SECURE_ASSET',FALSE))}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css',env('SECURE_ASSET',FALSE))}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css',env('SECURE_ASSET',FALSE))}}">
<style type="text/css" media="screen">
  .table-responsive div a:hover{ text-decoration: underline; font-size: 600; }  
</style>
@stop

@section('page-scripts')
<script src="{{asset('assets/js/vendor/datatables.min.js',env('SECURE_ASSET',FALSE))}}"></script>
<script src="{{asset('assets/js/datatables.script.js',env('SECURE_ASSET',FALSE))}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.js',env('SECURE_ASSET',FALSE))}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js',env('SECURE_ASSET',FALSE))}}"></script>
<script type="text/javascript">
  $(function(){

    $(".datepicker").pickadate({
      format: 'mm/dd/yyyy',
    });

  })
</script>
@stop