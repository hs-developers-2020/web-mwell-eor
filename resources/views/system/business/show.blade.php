@extends('system._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Business</h1>
  <ul>
      <li>Business Details :: {{ $merchant->merchant_name }}</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-5">
    <div class="card mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Business Details</h3>
        </div>
        <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-hover">
                <tbody>
                  <tr>
                    <td>Business Name</td>
                    <td colspan="3"><strong class="text-danger">{{ $merchant->merchant_name }}</strong></td>
                  </tr>
                  <tr>
                    <td>Section</td>
                    <td colspan="3">{{ $merchant->section ? $merchant->section->name : '---' }}</td>
                  </tr>
                  <tr>
                    <td>Division</td>
                    <td colspan="3">{{ $merchant->division ? $merchant->division->name : '---' }}</td>
                  </tr>
                  <tr>
                    <td>Address</td>
                    <td colspan="3">{{ $merchant->address ?: '---' }}</td>
                  </tr>
                  <tr>
                    <td>TIN</td>
                    <td colspan="3">{{ $merchant->tin ?: '---' }}</td>
                  </tr>
                  <tr>
                    <td>Telephone Number</td>
                    <td colspan="3">{{ $merchant->tel_no ?: '---' }}</td>
                  </tr>
                </tbody>
              </table>
            </div>
            @if(in_array($user->type, ['user']))
            <div class="form-group">
              <a href="{{route('admin.business.index')}}" class="btn btn-secondary">Go back to Business List</a>
            </div>
            @else
            <div class="form-group">
              <a href="{{route('admin.account.show', [$merchant->owner_user_id])}}" class="btn btn-secondary">Go back to Business List</a>
            </div>
            @endif
        </div>
    </div>
  </div>
  <div class="col-md-7">
    <div class="card mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Transaction List</h3>
        </div>
        <div class="card-body">
            <h5>Available Online OR Series: <strong>{{ $merchant->available_or }}</strong></h5>
            <div class="table-responsive">
              <table class="table table-striped table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th>Transaction Date</th>
                        <th>Date Expiry</th>
                        <th>OR</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>
                  @forelse($transactions as $index => $transaction)
                  <tr>
                    <td>
                      <div>{{ $transaction->created_at->format("d M Y") }}</div>
                      <div><small>{{ $transaction->created_at->format("h:i A") }}</small></div>
                    </td>
                    <td>
                      <div>{{ $transaction->date_expiry->format("d M Y") }}</div>
                    </td>
                    <td>
                      <div><u><a href="{{route('admin.ledger.print',[$transaction->bundle_id])}}" target="_blank"><strong>{{ $transaction->or_code }}</strong></a></u></div>
                    </td>
                    <td>₱ {{ Helper::money_format($transaction->amount) }}</td>
                  </tr>
                  @empty
                  <tr>
                    <td colspan="7" class="text-center">No available data</td>
                  </tr>
                  @endforelse
                </tbody>
              </table>
              @if(count($merchant->transactions) > 5)
                <div class="form-group text-center">
                  <a href="{{route('admin.ledger.online',[$merchant->merchant_code])}}" class="btn btn-success">View More</a>
                </div>
              @endif
            </div>
        </div>
    </div>
    <div class="card mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Transaction List</h3>
        </div>
        <div class="card-body">
            <h5>Available Booklet Series: <strong>{{ $merchant->available_booklet }}</strong></h5>
            <div class="table-responsive">
              <table class="table table-striped table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th>Transaction Date</th>
                        <th>Date Expiry</th>
                        <th>Series</th>
                        <th>Amount</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                  @forelse($booklet_transaction as $index => $transaction)
                  <tr>
                    <td>
                      <div>{{ $transaction->transaction_date ? $transaction->transaction_date->format("d M Y") : '---' }}</div>
                    </td>
                    <td>
                      <div>{{ $transaction->date_expiry ? $transaction->date_expiry->format("d M Y") : '---' }}</div>
                    </td>
                    <td>
                      <div><strong>{{ $transaction->series_code }}</strong></div>
                    </td>
                    <td>₱ {{ $transaction->amount ? Helper::money_format($transaction->amount) : '---' }}</td>
                    <td>
                        {{-- @if($transaction->status == 0) --}}
                          <div class="btn-group">
                            <button type="button" data-toggle="dropdown" class="btn btn-sm btn-secondary dropdown-toggle" aria-expanded="false">Actions <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{route('admin.ledger.edit',[$merchant->merchant_code, $transaction->series_code])}}">Edit</a>
                            </div>
                          </div>
                        {{-- @endif --}}
                    </td>
                  </tr>
                  @empty
                  <tr>
                    <td colspan="7" class="text-center">No available data</td>
                  </tr>
                  @endforelse
                </tbody>
              </table>
              @if(count($merchant->booklet_transactions) > 5)
                <div class="form-group text-center">
                  <a href="{{route('admin.ledger.booklet',[$merchant->merchant_code])}}" class="btn btn-success">View More</a>
                </div>
              @endif
            </div>
        </div>
    </div>
  </div>
</div>
@stop

@section('page-modals')

@stop

@section('page-styles')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
<style type="text/css" media="screen">
.qrcode{ height: 150px;  margin: 0px auto;}  
</style>
@stop

@section('page-scripts')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
<script type="text/javascript">
  $(function(){
    $(".action-delete").on("click",function(){
      var btn = $(this);
      $("#btn-confirm-delete").attr({"href" : btn.data('url')});
    });

    $('#btn-confirm-delete').on('click', function() {
      $('.btn-link').hide();
      $('.btn-loading').button('loading');
      $('#target').submit();
    });
  })
</script>
@stop