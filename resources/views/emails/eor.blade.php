<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ELECTRONIC ONLINE RECEIPT</title>
    <style>
        body{ font-family: 'Calibri', sans-serif; }
    </style>
</head>
<body>
    
    <table style="padding: 15px;">
        <tr>
            <td>
                Dear {{ strtoupper($transaction->name)  }},<br><br>

                Attached is a copy of your Electronic Online Receipt (EOR) from your transaction yesterday using {{ strtoupper($transaction->items[0]->merchant_name)}}.

                <br><br>

                This is a system-generated email, please do not reply. Please ignore this message if you didn't request and contact us.

                <br><br>
                Thank you for using our platform.
                <br><br><br>
            </td>
        </tr>
        <tr style="margin-top-top: 50px">
            <td>Regards,<br>Support Team</td>
        </tr>
    </table>
</body>
</html>