@extends('portal._layouts.auth')

@section('content')
<div class="auth-content">
    <div class="card o-hidden">
        <div class="row">
            
            <div class="col-md-6">
                <div class="p-4">
                    @if(session()->get('admin_login','no') == "yes")
                    <h4>Admin Portal</h4>
                    @else
                    <h4>Partner Portal</h4>
                    <p>Sales Tracker &amp; Barcode Generator.</p>
                    <h3><b class="text-primary">Work Smarter</b> <br>not <b class="text-danger"><strike>Hard</strike></b>.</h3>
                    @endif 
                    
                    @if(session()->get('admin_login','no') == "no")
                    <p><a href="{{route('portal.admin_login')}}">[Admin Login]</a></p>
                    @else
                    <p><a href="{{route('portal.login')}}">[Go back to Partner Login]</a></p>
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <div class="p-4">
                    {{-- <div class="auth-logo text-center mb-4">
                        <img src="{{asset('assets/images/logo.png')}}" alt="">
                    </div> --}}
                    <h1 class="mb-2 text-18">Sign In</h1>
                    @include('portal.components.notifications')
                    <form action="" method="POST">
                        {!!csrf_field()!!}
                        <div class="form-group">
                            <label for="username">Email address or Username</label>
                            <input id="username" class="form-control form-control-rounded" type="text" name="username" value="{{old('username')}}">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input id="password" class="form-control form-control-rounded" type="password" name="password">
                        </div>
                        <button type="submit" class="btn btn-rounded btn-primary btn-block mt-2">Sign In</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop