<div class="main-header">
    <div class="logo">
        <img src="{{asset('assets/images/logo.png?v=1.1')}}" alt="">
    </div>

    <div class="menu-toggle">
        <div></div>
                <div></div>
        <div></div>
    </div>

    <div style="margin: auto"></div>

    <div class="header-part-right">
        {{-- <i class="i-Full-Screen header-icon d-none d-sm-inline-block" data-fullscreen></i>
        <div class="dropdown widget_dropdown">
            <i class="i-Safe-Box text-muted header-icon" role="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <div class="menu-icon-grid">
                    <a href="index.html#"><i class="i-Shop-4"></i> Home</a>
                    <a href="index.html#"><i class="i-Library"></i> UI Kits</a>
                    <a href="index.html#"><i class="i-Drop"></i> Apps</a>
                    <a href="index.html#"><i class="i-File-Clipboard-File--Text"></i> Forms</a>
                    <a href="index.html#"><i class="i-Checked-User"></i> Sessions</a>
                    <a href="index.html#"><i class="i-Ambulance"></i> Support</a>
                </div>
            </div>
        </div> --}}
        <div>Hi <span class="text-primary">{{Auth::guard((session()->get('is_admin','no') == "yes" ?'admin' : 'partner'))->user()->name}}</span>, {{Helper::greet()}}!</div>

        <div class="dropdown">
            <div  class="user col align-self-end">
                <img src="{{asset('placeholder/user.png')}}" id="userDropdown" alt="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <div class="dropdown-header">
                        <i class="i-Lock-User mr-1"></i> {{Auth::guard((session()->get('is_admin','no') == "yes" ?'admin' : 'partner'))->user()->name}}
                    </div>
                    <a class="dropdown-item" href="{{route('portal.profile.edit_password')}}">Update Password</a>
                    <a class="dropdown-item" href="{{route('portal.logout')}}">Sign out</a>
                </div>
            </div>
        </div>
    </div>
</div>