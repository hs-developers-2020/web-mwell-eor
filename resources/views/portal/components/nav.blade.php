<div class="side-content-wrap">
  <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
  <ul class="navigation-left">
      <li class="nav-item">
          <a class="nav-item-hold" href="{{route('portal.index')}}">
              <i class="nav-icon i-Bar-Chart"></i>
              <span class="nav-text">Dashboard</span>
          </a>
          <div class="triangle"></div>
      </li>
      @if(session()->get('is_admin','no') == "yes")
      <li class="nav-item ">
          <a class="nav-item-hold" href="{{route('portal.partner.index')}}">
              <i class="nav-icon i-Business-Mens"></i>
              <span class="nav-text">Partners</span>
          </a>
          <div class="triangle"></div>
      </li>
      @endif
      <li class="nav-item">
          <a class="nav-item-hold" href="{{route('portal.reporting.index')}}">
              <i class="nav-icon i-Line-Chart"></i>
              <span class="nav-text">Reporting</span>
          </a>
          <div class="triangle"></div>
      </li>
      @if(session()->get('is_admin','no') == "yes")
      <li class="nav-item">
          <a class="nav-item-hold" href="{{route('portal.reporting.import')}}">
              <i class="nav-icon i-Data-Upload"></i>
              <span class="nav-text">Upload Sales</span>
          </a>
          <div class="triangle"></div>
      </li>
      @endif
      
      <li class="nav-item ">
          <a class="nav-item-hold" href="{{route('portal.barcode.index')}}">
              <i class="nav-icon i-Billing"></i>
              <span class="nav-text">Barcodes</span>
          </a>
          <div class="triangle"></div>
      </li>
      @if(session()->get('is_admin','no') == "yes")
      <li class="nav-item " >
          <a class="nav-item-hold" href="{{route('portal.administrator.index')}}">
              <i class="nav-icon i-Administrator"></i>
              <span class="nav-text">Administrators</span>
          </a>
          <div class="triangle"></div>
      </li>
      @endif

  </ul>
</div>
<div class="sidebar-overlay"></div>