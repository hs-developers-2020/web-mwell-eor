@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Dashboard</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
  </ul> --}}
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    @include('portal.components.notifications')
  </div>
  
</div>
<div class="row">
  @if(session()->get('is_admin','no') == "yes")
  <div class="col-lg-3 col-md-6 col-sm-6">
    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
      <div class="card-body text-center">
        <i class="i-Business-Mens"></i>
        <div class="content">
            <p class="text-muted mt-2 mb-0">Partners</p>
            <p class="text-primary text-24 line-height-1 mb-2">{{Helper::nice_number($total_partners)}}</p>
        </div>
      </div>
    </div>
  </div>
  @else
  <div class="col-lg-3 col-md-6 col-sm-6">
    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
      <div class="card-body text-center">
        <i class="i-Financial"></i>
        <div class="content">
            <p class="text-muted mt-2 mb-0">Today</p>
            <p class="text-primary text-24 line-height-1 mb-2">₱ {{Helper::format_num($today_sales)}}</p>
        </div>
      </div>
    </div>
  </div>
  @endif
  <div class="col-lg-3 col-md-6 col-sm-6">
    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
      <div class="card-body text-center">
        <i class="i-Financial"></i>
        <div class="content">
            <p class="text-muted mt-2 mb-0">Yesterday</p>
            <p class="text-primary text-24 line-height-1 mb-2">₱ {{Helper::format_num($yesterday_sales)}}</p>
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-3 col-md-6 col-sm-6">
    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
      <div class="card-body text-center">
        <i class="i-Money-2"></i>
        <div class="content">
            <p class="text-muted mt-2 mb-0">{{Carbon::now()->format("M Y")}}</p>
            <p class="text-primary text-24 line-height-1 mb-2">₱ {{Helper::format_num($this_month)}}</p>
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-3 col-md-6 col-sm-6">
    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
      <div class="card-body text-center">
        <i class="i-Money-2"></i>
        <div class="content">
            <p class="text-muted mt-2 mb-0">This Week</p>
            <p class="text-primary text-24 line-height-1 mb-2">₱ {{Helper::format_num($this_week)}}</p>
        </div>
      </div>
    </div>
  </div>
</div>

{{-- <div class="row">
    <div class="col-lg-8 col-md-12">
        <div class="card mb-4">
            <div class="card-body">
                <div class="card-title">This Year Sales</div>
                <div id="echartBar" style="height: 300px;"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-sm-12">
        <div class="card mb-4">
            <div class="card-body">
                <div class="card-title">Sales by Countries</div>
                <div id="echartPie" style="height: 300px;"></div>
            </div>
        </div>
    </div>
</div> --}}

<div class="row">
    <div class="col-lg-8 col-md-12">
        <div class="card mb-4">
            <div class="card-body">
                <div class="card-title">This Week Sales</div>
                <div id="echartBar" style="height: 300px;"></div>
            </div>
        </div>
    </div>
    {{-- <div class="col-lg-4 col-sm-12">
        <div class="card mb-4">
            <div class="card-body">
                <div class="card-title">Sales by Products</div>
                <div id="echartPie" style="height: 300px;"></div>
            </div>
        </div>
    </div> --}}
</div>
@stop

@section('page-scripts')
<script type="text/javascript">
  $(function(){

    var echartElemBar = document.getElementById('echartBar');
    if (echartElemBar) {
        var echartBar = echarts.init(echartElemBar);
        echartBar.setOption({
            legend: {
                borderRadius: 0,
                orient: 'horizontal',
                x: 'right',
                data: ['This Week', 'Last Week']
            },
            grid: {
                left: '8px',
                right: '8px',
                bottom: '0',
                containLabel: true
            },
            tooltip: {
                show: true,
                backgroundColor: 'rgba(0, 0, 0, .8)'
            },
            xAxis: [{
                type: 'category',
                data: [ 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat','Sun'],
                axisTick: {
                    alignWithLabel: true
                },
                splitLine: {
                    show: false
                },
                axisLine: {
                    show: true
                }
            }],
            yAxis: [{
                type: 'value',
                axisLabel: {
                    formatter: '₱ {value}'
                },
                min: 0,
                max :  20000,
                // max: 1000,
                // interval: 5000,
                axisLine: {
                    show: false
                },
                splitLine: {
                    show: true,
                    interval: 'auto'
                }
            }],

            series: [{
                name: 'This Week',
                data: {!!json_encode($thisweek)!!},
                label: { show: false, color: '#0168c1' },
                type: 'bar',
                barGap: 0,
                color: '#fd8a75',
                smooth: true,
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowOffsetY: -2,
                        shadowColor: 'rgba(0, 0, 0, 0.3)'
                    }
                }
            }, {
                name: 'Last Week',
                data: {!!json_encode($lastweek)!!},
                label: { show: false, color: '#639' },
                type: 'bar',
                color: '#999999',
                smooth: true,
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowOffsetY: -2,
                        shadowColor: 'rgba(0, 0, 0, 0.3)'
                    }
                }
            }]
        });
        $(window).on('resize', function () {
            setTimeout(function () {
                echartBar.resize();
            }, 500);
        });
    }

    // Chart in Dashboard version 1
    // var echartElemPie = document.getElementById('echartPie');
    // if (echartElemPie) {
    //     var echartPie = echarts.init(echartElemPie);
    //     echartPie.setOption({
    //         color: ['#62549c', '#7566b5', '#7d6cbb', '#8877bd', '#9181bd', '#6957af'],
    //         tooltip: {
    //             show: true,
    //             backgroundColor: 'rgba(0, 0, 0, .8)'
    //         },

    //         series: [{
    //             name: 'Sales by Product',
    //             type: 'pie',
    //             radius: '60%',
    //             center: ['50%', '50%'],
    //             data: [{ value: 535, name: 'USA' }, { value: 310, name: 'Brazil' }, { value: 234, name: 'France' }, { value: 155, name: 'BD' }, { value: 130, name: 'UK' }, { value: 348, name: 'India' }],
    //             itemStyle: {
    //                 emphasis: {
    //                     shadowBlur: 10,
    //                     shadowOffsetX: 0,
    //                     shadowColor: 'rgba(0, 0, 0, 0.5)'
    //                 }
    //             }
    //         }]
    //     });
    //     $(window).on('resize', function () {
    //         setTimeout(function () {
    //             echartPie.resize();
    //         }, 500);
    //     });
    // }
  })
</script>
@stop