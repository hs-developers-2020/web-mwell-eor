@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Reporting</h1>
  <ul>
      <li>Reset Sales Data</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Sales Data Reset Form</h3>
        </div>
        <div class="card-body" style="min-height: 300px;">
          <form action="" method="POST">
            {!!csrf_field()!!}
            <div class="form-group">
                <label for="input_sale_date" class="ul-form__label"><b>Sales Date</b></label>
                <input type="text" class="form-control" id="input_sale_date" placeholder="" name="sale_date">
                @if($errors->first('sale_date'))
                <p class="form-text text-danger">{{$errors->first('sale_date')}}</p>
                @endif
            </div>

            <div class="form-group">
              <a href="{{route('portal.reporting.index')}}" class="btn btn-secondary">Go back to Sales List</a>
              <button type="submit" class="btn  btn-primary">Submit</button>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
@stop

@section('page-scripts')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
<script type="text/javascript">
  $(function(){
    $("#input_sale_date").pickadate({
      format: 'mm/dd/yyyy'
    });
  })
</script>
@stop