@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Reporting</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
  </ul> --}}
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card mb-5">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Advance Filters </h3>
            <span class="float-right">
              <a href="{{route('portal.reporting.reset')}}" class="btn btn-default btn-sm">[Reset Filter]</a>

            </span>
        </div>
        <form action="" method="GET">
        <div class="card-body">
            <div class="row row-xs">
                @if(session()->get('is_admin','no') == "yes")
                <div class="col-md-4">
                    {!!Form::select('partner_id',$partners,old('partner_id',$partner_id),['class' => "form-control",'id' => "input_partner_id"])!!}
                </div>
                <div class="col-md-3 mt-3 mt-md-0">
                    <input type="text" class="form-control datepicker" placeholder="Start Date" name="start_date" value="{{$start_date}}">
                </div>
                <div class="col-md-3 mt-3 mt-md-0">
                    <input type="text" class="form-control datepicker" placeholder="End Date" name="end_date" value="{{$end_date}}">
                </div>
                @else
                <div class="col-md-5">
                    <input type="text" class="form-control datepicker" placeholder="Start Date" name="start_date" value="{{$start_date}}">
                </div>
                <div class="col-md-5 mt-3 mt-md-0">
                    <input type="text" class="form-control datepicker" placeholder="End Date" name="end_date" value="{{$end_date}}">
                </div>
                @endif
                <div class="col-md-2 mt-3 mt-md-0">
                    <button type="submit" class="btn btn-primary btn-block">Apply Filters</button>
                </div>
            </div>
        </div>
        </form>
    </div>
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Record Data - {{$start_date == $end_date ? Carbon::parse($start_date)->format("M. d Y (D)") : Carbon::parse($start_date)->format("M. d Y (D)")." - ".Carbon::parse($end_date)->format("M. d Y (D)")}} </h3>
            @if(session()->get('is_admin','no') == "yes")
            <span class="float-right">
              <a href="{{route('portal.reporting.import')}}" class="btn btn-primary">Import Sales Data</a>
              <a href="{{route('portal.reporting.reset')}}" class="btn btn-danger">Remove Sales Data</a>
            </span>
            @endif
        </div>
        <div class="card-body">
          @include('portal.components.notifications')
            <div class="table-responsive">
                <table id="zero_configuration_table" class="display table table-striped table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Sales Date</th>
                            <th scope="col">Partner</th>
                            <th scope="col" class="text-left" width="25%">Product Name</th>
                            <th scope="col">Product Code</th>
                            <th scope="col">Orig. Price</th>
                            <th scope="col">Sale Price/Qty</th>
                            <th scope="col">Discount</th>
                            <th scope="col">Subtotal</th>
                            @if(session()->get('is_admin','no') == "yes")
                            <th scope="col">Sales Commission</th>
                            @endif

                            {{-- <th scope="col">GP</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($sales as $index => $record)
                        <?php $commission += ($record->total*($record->margin/100));?>
                        <tr>
                          <td>{{$record->sale_date}}</td>
                          <td>{{$record->partner?$record->partner->name:"n/a"}}</td>
                          <td class="text-left">{{Str::upper($record->product_name)}}</td>
                          <td class="text-center">{{Str::upper($record->sku)}}</td>
                          <td class="text-center">{{Helper::money_format($record->price)}} </td>
                          <td>{{Helper::money_format($record->sale_price)}}
                            <div><small><b>{{$record->qty}} item/s</b></small></div>
                          </td>
                          <td>{{Helper::money_format($record->discount)}}</td>
                          <td>₱ {{Helper::money_format($record->total)}}</td>
                          @if(session()->get('is_admin','no') == "yes")
                          <td>₱ {{Helper::money_format($record->total*($record->margin/100))}} <span><small><b>({{$record->margin}}%)</b></small></span></td>
                          @endif

                          {{-- <td>{{Helper::money_format($record->gp)}}</td> --}}
                        </tr>
                        @empty
                        <tr>
                          <td colspan="9">No sales record found yet.</td>
                        </tr>
                        @endforelse

                    </tbody>
                    <tbody>
                      <tr>
                        <th colspan="9">
                          @if(session()->get('is_admin','no') == "yes")
                          <div class="row">
                            <div class="col-md-4">No. of Items: {{Helper::nice_number($sales->sum('qty'))}}</div>
                            <div class="col-md-4">Total Discount: ₱ {{Helper::money_format($sales->sum('discount'))}}</div>
                          </div>
                          <div class="row">
                            <div class="col-md-4">Total Commission: ₱ ({{Helper::money_format($commission)}})</div>
                            <div class="col-md-4">Total Sales: <span class="">₱ {{Helper::money_format($sales->sum('total'))}}</span></div>
                            <div class="col-md-4">Total Sales Less Commission: <span class="text-danger">₱ {{Helper::money_format($sales->sum('total')-$commission)}}</span></div>

                          </div>
                          @else
                          <div class="row">
                            <div class="col-md-6">No. of Items: {{Helper::nice_number($sales->sum('qty'))}}</div>
                            <div class="col-md-6">Total Discount: ₱ {{Helper::money_format($sales->sum('discount'))}}</div>
                          </div>
                          <div class="row">
                            <div class="col-md-6">Total Sales: <span>₱ {{Helper::money_format($sales->sum('total'))}}</span></div>
                            <div class="col-md-6">Total Sales Less Commission Rate: <span class="text-danger">₱ {{Helper::money_format($sales->sum('total')-$commission)}}</span></div>
                          </div>

                          @endif

                        </th>
                      </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
  </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
<link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
@stop

@section('page-scripts')
<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
<script src="{{asset('assets/js/datatables.script.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
<script type="text/javascript">
  $(function(){
    $(".datepicker").pickadate({
      format: 'mm/dd/yyyy'
    });
  })
</script>
@stop