@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Brand Partners</h1>
  <ul>
      <li>Update Partner Details</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Brand Partner Update Form</h3>
        </div>
        <div class="card-body">
          @include('portal.components.notifications')
          <form action="" method="POST">
            {!!csrf_field()!!}
            <div class="form-group">
                <label for="input_name"><b>Partner Name</b></label>
                <input type="text" class="form-control" id="input_name" placeholder="" value="{{old('name',$account->name)}}" name="name">
                @if($errors->first('name'))
                <p class="form-text text-danger">{{$errors->first('name')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_code"><b>Partner Code</b></label>
                <input type="text" class="form-control" id="input_code" placeholder="" value="{{old('code',$account->code)}}" name="code">
                @if($errors->first('code'))
                <p class="form-text text-danger">{{$errors->first('code')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_margin"><b>Sales Margin</b></label>
                <input type="text" class="form-control" id="input_margin" placeholder="" value="{{old('margin',$account->margin)}}" name="margin">
                @if($errors->first('margin'))
                <p class="form-text text-danger">{{$errors->first('margin')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_owner_name"><b>Owner's Name</b></label>
                <input type="text" class="form-control" id="input_owner_name" placeholder="" value="{{old('owner_name',$account->owner_name)}}" name="owner_name">
                @if($errors->first('owner_name'))
                <p class="form-text text-danger">{{$errors->first('owner_name')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_username"><b>Username</b></label>
                <input type="text" class="form-control" id="input_username" placeholder="" value="{{old('username',$account->username)}}" name="username">
                @if($errors->first('username'))
                <p class="form-text text-danger">{{$errors->first('username')}}</p>
                @endif
            </div>


            <div class="form-group">
                <label for="input_email"><b>Email</b></label>
                <input type="email" class="form-control" id="input_email" placeholder="" value="{{old('email',$account->email)}}" name="email">
                @if($errors->first('email'))
                <p class="form-text text-danger">{{$errors->first('email')}}</p>
                @endif
            </div>
            <div class="form-group">
                <label for="input_status"><b>Status</b></label>
                {!!Form::select('status',$statuses,old('status',$account->status),['class' => "form-control",'id' => "input_status"])!!}
                @if($errors->first('status'))
                <p class="form-text text-danger">{{$errors->first('status')}}</p>
                @endif
            </div>

            <div class="form-group">
              <a href="{{route('portal.partner.index')}}" class="btn btn-secondary">Go back to Partner List</a>
              <button type="submit" class="btn  btn-primary">Submit</button>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
@stop