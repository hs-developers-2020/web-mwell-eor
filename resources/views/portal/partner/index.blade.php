@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Brand Partners</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
  </ul> --}}
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Record Data </h3>
            <span class="float-right">
              <a href="{{route('portal.partner.create')}}" class="btn btn-primary">Add New Partner</a>
              <a href="{{route('portal.partner.import')}}" class="btn btn-info">Import Data</a>

            </span>
        </div>
        <div class="card-body">
            @include('portal.components.notifications')
            <div class="table-responsive">
              <table id="zero_configuration_table" class="display table table table-bordered text-center">
                  <thead class="thead-dark">
                      <tr>
                          <th scope="col">Partner Name</th>
                          <th scope="col">Partner Code</th>
                          <th scope="col">Owner's Name</th>
                          {{-- <th scope="col">Username</th> --}}
                          <th scope="col">Email</th>
                          <th scope="col">Sales Margin (%)</th>
                          <th scope="col">Status</th>
                          <th scope="col">Date Registered</th>
                          {{-- <th scope="col" class="text-left">Last Modified</th> --}}
                          <th scope="col"></th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach($accounts as $index => $account)
                      <tr>
                          <td class="text-left">{{$account->name}}</td>
                          <td>{{Str::upper($account->code)}}</td>
                          <td class="text-left">{{$account->owner_name}}</td>
                          {{-- <td>{{$account->username}}</td> --}}
                          <td>{{$account->email}}</td>
                          <td class="text-center">{{$account->margin}}%</td>
                          <td>
                            <span class="badge badge-{{Helper::status_badge($account->status)}} r-badge">{{$account->status}}</span>

                          </td>
                          <td>{{$account->created_at->format("(D) M. d, Y")}}</td>
                          {{-- <td class="text-left">
                            <div>{{$account->updated_at->format("M. d, Y")}}</div>
                            <div>{{$account->updated_at->format("h:i A (D)")}}</div>
                          </td> --}}

                          <td>
                              <div class="btn-group">
                                <button type="button" data-toggle="dropdown" class="btn btn-sm btn-secondary dropdown-toggle" aria-expanded="false">Actions <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="{{route('portal.partner.edit',[$account->id])}}">Edit Details</a>
                                    <a class="dropdown-item" href="{{route('portal.partner.reset_password',[$account->id])}}">Reset Password</a>
                                </div>
                              </div>
                          </td>
                      </tr>
                      @endforeach
                  </tbody>
              </table>
            </div>
        </div>
    </div>
  </div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@stop

@section('page-scripts')
<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
<script src="{{asset('assets/js/datatables.script.js')}}"></script>
@stop