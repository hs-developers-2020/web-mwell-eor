@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Brand Partners</h1>
  <ul>
      <li>Import Data</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Brand Partner Import Form</h3>
        </div>
        <div class="card-body">
          <form action="" method="POST" enctype=multipart/form-data>
            {!!csrf_field()!!}
            <div class="form-group">
                <label for="input_file" class="ul-form__label"><b>Mass Upload Brand Partner</b></label>
                <input type="file" class="form-control" id="input_file" placeholder="" name="file">
                <div>Template File. <a href="{{asset('templates/partner.xls')}}" target="_blank">[.xls template]</a> click to download.</div>
                @if($errors->first('file'))
                <p class="form-text text-danger">{{$errors->first('file')}}</p>
                @endif
            </div>

            <div class="form-group">
              <a href="{{route('portal.partner.index')}}" class="btn btn-secondary">Go back to Partners List</a>
              <button type="submit" class="btn  btn-primary">Submit</button>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
@stop