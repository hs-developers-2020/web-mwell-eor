@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Profile</h1>
  <ul>
      <li>Update Password</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Update Password Form</h3>
        </div>
        <div class="card-body">
          @include('portal.components.notifications')
          <form action="" method="POST">
            {!!csrf_field()!!}

            <div class="form-group">
                <label for="input_current_password"><b>Current Password</b></label>
                <input type="password" class="form-control" id="input_current_password" placeholder="" value="" name="current_password">
                @if($errors->first('current_password'))
                <p class="form-text text-danger">{{$errors->first('current_password')}}</p>
                @endif
            </div>
            
            <div class="form-group">
                <label for="input_password"><b>New Password</b></label>
                <input type="password" class="form-control" id="input_password" placeholder="" value="" name="password">
                @if($errors->first('password'))
                <p class="form-text text-danger">{{$errors->first('password')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_password_confirmation"><b>Confirm New Password</b></label>
                <input type="password" class="form-control" id="input_password_confirmation" placeholder="" value="" name="password_confirmation">
            </div>

            <div class="form-group">
              <a href="{{route('portal.index')}}" class="btn btn-secondary">Go back to Dashboard</a>
              <button type="submit" class="btn  btn-primary">Submit</button>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
@stop