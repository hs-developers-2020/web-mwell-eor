<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{env('APP_NAME')}} - Login</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/styles/css/themes/lite-purple.min.css')}}">
    <style>
        a {
            color: #333;
        }
        .text-primary {
            color: #fd8a75 !important;
        }
        .btn-primary, .btn-outline-primary {
              border-color: #fd8a75;
        }

        .btn-primary,.btn-primary:hover {
          color: #fff;
          background-color: #fd8a75;
          border-color: #fd8a75;
        }
    </style>
</head>

<body>
    <div class="auth-layout-wrap" {{-- style="background-image: url('{{asset('assets/images/photo-wide-4.jpg?v=1.1')}}')" --}}>
        @yield('content')
    </div>

    <script src="{{asset('assets/js/common-bundle-script.js')}}"></script>

    <script src="{{asset('assets/js/script.js')}}"></script>
</body>

</html>
