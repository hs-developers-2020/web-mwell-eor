@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Barcodes Generator</h1>
  <ul>
      <li>Generate Barcode</li>
  </ul>
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Barcode Generator Form</h3>
        </div>
        <div class="card-body">
          @include('portal.components.notifications')
          <form action="" method="POST">
            {!!csrf_field()!!}
            @if(session()->get('is_admin','no') == "yes")
            <div class="form-group">
                <label for="input_partner_id"><b>Partner</b></label>
                {!!Form::select('partner_id',$partners,old('partner_id'),['class' => "form-control",'id' => "input_partner_id"])!!}
                @if($errors->first('partner_id'))
                <p class="form-text text-danger">{{$errors->first('partner_id')}}</p>
                @endif
            </div>
            @endif
            <div class="form-group">
                <label for="input_name"><b>Product Name</b></label>
                <input type="text" class="form-control" id="input_name" placeholder="" value="{{old('name')}}" name="name">
                @if($errors->first('name'))
                <p class="form-text text-danger">{{$errors->first('name')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_code"><b>Barcode/SKU</b></label>
                <input type="text" class="form-control" id="input_code" placeholder="" value="{{old('code',(Str::upper(session()->get('is_admin','no') == "no" ? Auth::guard('partner')->user()->code: "")))}}" name="code">
                <p class="form-text">Qty will be automatically adjust to divisible by 3. eg. 5 -> 6 qty will be stored. <b> ~21 barcodes per page</b></p>
                @if($errors->first('code'))
                <p class="form-text text-danger">{{$errors->first('code')}}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="input_qty"><b>No. of Copies</b></label>
                <input type="text" class="form-control" id="input_qty" placeholder="" value="{{old('qty')}}" name="qty">
                @if($errors->first('qty'))
                <p class="form-text text-danger">{{$errors->first('qty')}}</p>
                @endif
            </div>
            
            <div class="form-group">
                <label for="input_price"><b>Price</b></label>
                <input type="text" class="form-control" id="input_price" placeholder="" value="{{old('price')}}" name="price">
                @if($errors->first('price'))
                <p class="form-text text-danger">{{$errors->first('price')}}</p>
                @endif
            </div>

            <div class="form-group">
              <a href="{{route('portal.barcode.index')}}" class="btn btn-secondary">Go back to Barcode List</a>
              <button type="submit" class="btn  btn-primary">Submit</button>
            </div>
          </form>
        </div>
    </div>
  </div>
</div>
@stop