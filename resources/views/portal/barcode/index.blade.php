@extends('portal._layouts.main')

@section('breadcrumb')
<div class="breadcrumb">
  <h1>Barcodes Generator</h1>
  {{-- <ul>
      <li><a href="">Dashboard</a></li>
      <li>Version 1</li>
  </ul> --}}
</div>

<div class="separator-breadcrumb border-top"></div>
@stop

@section('content')
<div class="row">
  <div class="col-md-8">
    <div class="card o-hidden mb-4">
        <div class="card-header">
            <h3 class="w-50 float-left card-title m-0">Record Data </h3>
            <span class="float-right">
              <a href="{{route('portal.barcode.create')}}" class="btn btn-primary">Generate Barcode</a>
            </span>
        </div>
        <div class="card-body">
            @include('portal.components.notifications')
          
            <div class="table-responsive">
              <table class="table table-bordered text-center">
                <thead class="thead-dark">
                    <tr>
                        @if(session()->get('is_admin','no') == "yes")
                        <th scope="col">Partner Name</th>
                        @endif
                        <th scope="col">Product Name</th>
                        <th scope="col">Product Code/SKU</th>
                        <th scope="col"># of Copies</th>
                        <th scope="col">Price</th>

                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($barcodes as $index => $barcode)
                    <tr>
                        @if(session()->get('is_admin','no') == "yes")
                        <td>{{$barcode->partner?$barcode->partner->name:'n/a'}}</td>
                        @endif
                        <td>{{$barcode->product_name}}</td>
                        <td>{{$barcode->code}}</td>
                        <td><b>{{Helper::nice_number($barcode->qty)}}</b></td>
                        <td><b>₱ {{Helper::money_format($barcode->price)}}</b></td>
                        <td>
                            <div class="btn-group">
                              <button type="button" data-toggle="dropdown" class="btn btn-sm btn-secondary dropdown-toggle" aria-expanded="false">Actions <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                              <div class="dropdown-menu">
                                  <a class="dropdown-item" href="{{route('portal.barcode.edit',[$barcode->id])}}">Edit Details</a>
                                  <a class="dropdown-item" target="_blank" href="{{route('portal.barcode.download',[$barcode->id])}}">Download Barcodes</a>
                              </div>
                            </div>
                        </td>
                    </tr>
                    @empty
                    <tr>
                      <td colspan="6"><div class="text-center"><i>No records found yet.</i></div></td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
            </div>
        </div>
    </div>
  </div>
</div>
@stop