<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <style>
        * {
            font-family: Arial, Helvetica, sans-serif;
        }
        .gray {
            color: #909497;
        }

        .text-center {
            text-align: center;
        }

        .text-right {
            text-align: right;
        }

        .small {
            font-size: 10px;
        }

        .big {
            font-size: 18px;
        }

        .qr {
            width: 80px;
            height: 80px;
        }

        .mb-0 {
            margin-bottom: 0;
        }

        .mt-0 {
            margin-top: 0;
        }

        p {
            font-size: 12px;
            margin: 5px 0;
        }

        .table-container {
            background: #EAF2F8;
            padding: 10px 15px;
        }

        .table-container table td {
            padding: .2rem;
            font-size: 12px;
        }

        h3 {
            font-size: 12px;
        }

        hr {
            border-top: 1px solid #BDC3C7;
        }

        table {
            width: 100%;
        }

        .border-top {
            border-top: 1px solid #BDC3C7;
        }

        .underline {
            text-decoration: underline;
        }

        label {
            font-size: 12px;
        }

        input {
            vertical-align: middle;
            position: relative;
            bottom: 1px;
        }

        .container {
            width: 100%;
        }
    </style>
</head>
<body>
    <div class="container">
        <p class="text-center small gray">*THIS ELECTRONIC OFFICIAL RECEIPT SHALL BE VALID FOR FIVE(5) YEARS FROM THE DATE OF ATP*</p>

        <table>
            <tr>
                <td>
                    <img class="qr" src="https://i.pinimg.com/originals/60/c1/4a/60c14a43fb4745795b3b358868517e79.png" alt="">
                </td>
                <td class="text-right">
                    <p>No.: <b><span class="big">00012</span></b></p>
                    <p>Date: 05/07/2020</p>
                    <p>Reference No: 3234145232</p>
                    <p>Merchant No: 547863223</p>
                    <p>Teller No: 563453232</p>
                </td>
            </tr>
        </table>

        <table>
            <tr>
                <td>
                    <h3 class="mb-0">ABC COMPANY</h3>
                    <p>Zia, 123 Berry Street, Suite 550,</p>
                    <p>Ortigas, Metro Manila</p>
                    <p>VAT Reg. TIN 001-212-657-000</p>

                    <br>

                    <p class="mb-0"><b>RECEIVED FROM:</b></p>
                    <h3 class="mb-0 mt-0">XYZ Inc.</h3>
                    <p>890 Berry Street, Summit Center</p>
                    <p>Makati, Metro Manila</p>
                    <p>VAT Reg. TIN 009-955-344-000</p>

                    <br>

                    <p class="gray">PAYMENT METHOD</p>

                    <table>
                        <tr>
                            <td class="text-center checkboxes">
                                <label><input type="checkbox">CASH</label>
                            </td>
                            <td class="text-center checkboxes">
                                <label><input type="checkbox">CHECK</label>
                            </td>
                            <td class="text-center checkboxes">
                                <label><input type="checkbox">ON US</label>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center checkboxes">
                                <label><input type="checkbox">DEBIT</label>
                            </td>
                            <td class="text-center checkboxes">
                                <label><input type="checkbox">CREDIT</label>
                            </td>
                            <td class="text-center checkboxes">
                                <label><input type="checkbox">WALLET</label>
                            </td>
                        </tr>
                    </table>

                    <br>

                    <p class="small">200 BKLTS (50x2) 5001 - 15000</p>
                    <p class="small">BIR AUTHORITY TO PRINT NO 9A3233455439203</p>
                    <p class="small">DATE ISSUED 04/04/2020; VALID UNTIL: 04/02/2025</p>
                    <p class="small">(ASB PRINTING) - RONALDO B. BASSIG</p>
                    <p class="small">PRINTER'S ACCREDITATION NO. 0234MP234102000030203</p>
                    <p class="small">DATE OF ACCREDITATION: 04/03/2020</p>
                    <p class="small">3234 NATIONAL RD, MUNTINLUPA CITY,</p>
                    <p class="small">VAT REG TIN. 152-234-435-000</p>
                </td>
                <td>
                    <p class="small gray">IN PAYMENT OF THE FOLLOWING</p>
                    <div class="table-container">
                        <table>
                            <tr>
                                <td><span class="item">AB 20012394</span></td>
                                <td class="text-right"><span class="item">PHP 72,800.00</span></td>
                            </tr>
                            <tr>
                                <td><span class="item">VATABLE SALES</span></td>
                                <td class="text-right"><span class="item">PHP 65,000.00</span></td>
                            </tr>
                            <tr>
                                <td><span class="item">VAT AMOUNT</span></td>
                                <td class="text-right"><span class="item">PHP 7,800.00</span></td>
                            </tr>
                            <tr>
                                <td colspan="2"><hr></td>
                            </tr>
                            <tr>
                                <td><span class="item"><b>TOTAL</b></span></td>
                                <td class="text-right"><span class="item"><b>PHP 72,800.00</b></span></td>
                            </tr>
                        </table>
                    </div>
                    <br>
                    <div class="table-container">
                        <table>
                            <tr>
                                <td><span class="item">VATABLE</span></td>
                                <td class="text-right"><span class="item">PHP 1,318.75</span></td>
                            </tr>
                            <tr>
                                <td><span class="item">EXEMPT</span></td>
                                <td class="text-right"><span class="item">PHP 0.00</span></td>
                            </tr>
                            <tr>
                                <td><span class="item">ZERO-R</span></td>
                                <td class="text-right"><span class="item">PHP 0.00</span></td>
                            </tr>
                        </table>
                    </div>
                    <br>
                    <div class="table-container">
                        <p class="gray">AMOUNT IN WORDS</p>
                        <p class="underline">SEVENTY THOUSAND TWO THOUSAND EIGHT HUNDRED PESOS AND 0/100</p>
                    </div>
                    <br><br>
                    <p class="text-center gray border-top">CASHIER / AUTHORIZED SIGNATURED</p>
                </td>
            </tr>
        </table>

        <br>

        <p class="text-center small gray">*THIS ELECTRONIC OFFICIAL RECEIPT SHALL BE VALID FOR FIVE(5) YEARS FROM THE DATE OF ATP*</p>
    </div>
</body>
</html>