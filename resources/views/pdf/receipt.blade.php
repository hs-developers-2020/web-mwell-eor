<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Official Receipt</title>
	<style>
		body{
			font-family: 'Helvetica';
			font-size: 8px;
			text-transform: uppercase;
		}
		.header{
			font-size: 6px;
			line-height: 1px;
		}
		.text-left{
			text-align: left;
		}
		.line3{
			line-height: 5px;
		}
		.fs10{
			font-size: 10px;
		}
		.mb-1{
			margin-bottom: .25rem;
		}
		.mb-2{
			margin-bottom: .5rem;
		}
		.mb-3{
			margin-bottom: 1rem;
		}
		.mb-4{
			margin-bottom: 2rem;
		}
		.mb-5{
			margin-bottom: 3rem;
		}

		.text-bold{
			font-weight: 800;
		}
		
	</style>
</head>
<body>
	<table width="100%" border="0">
		<tbody>
			<tr>
				<td align="center" width="50%" style="vertical-align: text-top;">
					<img src="{{ public_path('assets/images/rep_logo.png') }}" width="40" height="40">
					<p class="header">Republic of the Philippines</p>
					<h3>OFFICIAL RECEIPT</h3>
					<p class="header">Accountable Form No.51</p>
					<p class="header mb-4 text-bold">Revised January 1992</p>
					<p class="line3 text-bold">{{ $transaction->items ? $transaction->items[0]->office_code : '---'}}</p>
					<p class="line3 mb-4 text-bold">{{ $transaction->items ? $transaction->items[0]->office_name : '---'}}</p>
					<p class="text-left line3">OR_code #: {{ $transaction->items ? $transaction->items[0]->or_code : '---'}}</p>
					<p class="text-left line3">Transaction #: {{ $transaction->transaction_code }}</p>
					<p class="text-left line3">Date: {{ $transaction->items ? Helper::date_format($transaction->items[0]->created_at, 'Y/m/d h:i:s A') : '---'}}</p>
					<p class="text-left line3 mb-4">Valid Until: {{ $transaction->items ? Helper::date_format($transaction->items[0]->date_expiry, 'Y/m/d') : ''}} 5:00 PM</p>
					<p class="text-left line3">AGENCY</p>
					<p class="text-left line3 text-bold" style="padding-left: 10px;">{{ $transaction->items ? $transaction->items[0]->merchant_name : ''}}</p>
					<p class="text-left line3">PAYOR</p>
					<p class="text-left line3 text-bold" style="padding-left: 10px">{{ $transaction->name }}</p>
					{{-- <p class="text-left line3">FUND</p>
					<p class="text-left line3 mb-3" style="padding-left: 10px">010010100101 (Regular Agency)</p> --}}
					<table width="100%">
						<tr>
							<td>payment details</td>
						</tr>
						@foreach($transaction->items[0]->items as $index => $value)
						<tr>
							<td style="padding-top: {{ $index == 0 ? '5px' : '0px'}};" class="text-bold">{{ $value->name}}</td>
							<td style="padding-top: {{ $index == 0 ? '5px' : '0px'}}">P {{ Helper::money_format($value->price) }}</td>
						</tr>
						@if($value->descrption)
						<tr>
							<td style="padding-left: 20px;">{{ $value->descrption }}</td>
						</tr>
						@endif
						@endforeach

						<tr>
							<td colspan="2" style="border-bottom:solid .5px black; padding-top: 10px;">
							</td>
						</tr>
						<tr>
							<td colspan="2" style="border-bottom:solid .5px black; padding-top: -1px;">
							</td>
						</tr>
						
						@if($transaction->dst_fee AND $transaction->dst_fee > 0 AND count($transaction->items) >= 2)
							<tr>
								<td style="padding-top: 10px;">
									<p class="text-left line3">AGENCY: <strong>{{ $transaction->items[1]->merchant_name }}</strong></p>
									<p class="text-left line3">OR_code #: {{ $transaction->items[1]->or_code }}</p>
									<p class="text-left line3">Date: {{ Helper::date_format($transaction->items[1]->created_at, 'Y/m/d h:i:s A') }}</p>
									<p class="text-left line3 mb-4">Valid Until: {{ Helper::date_format($transaction->items[1]->date_expiry, 'Y/m/d') }} 5:00 PM</p>
								</td>
							</tr>

							@foreach($transaction->items[1]->items as $index => $value)
							<tr>
								<td class="text-bold">DST</td>
								<td>P {{ Helper::money_format($value->price) }}</td>
							</tr>
							@endforeach
						@endif

						<tr>
							<td style="padding-top: 20px;" class="text-bold">TOTAL</td>
							<td style="padding-top: 20px;" class="text-bold">P {{ Helper::money_format($transaction->sub_total + $transaction->dst_fee) }}</td>
						</tr>
						<tr>
							<td align="center" colspan="2" style="padding-top: 15px;">
								<img src="data:image/png;base64,,{{DNS2D::getBarcodePNG($transaction->payment_reference_code, "QRCODE",4.5,4.5)}}" alt="barcode">
							</td>
						</tr>
						<tr>
							<td align="center" colspan="2">{{ $transaction->payment_reference_code}}
							</td>
						</tr>
					</table>
				</td>
				
				<td align="center" width="50%" style="vertical-align: text-top;">
					@if($flag == true)
						@if($transaction->items[$index]->merchant_code == 'ZIAPLEX')
							<img src="{{ public_path('assets/images/ziaplex.png') }}" width="40" height="40">
						@else
							<img src="{{ public_path('assets/images/paymaya.png') }}" width="40" height="40">
						@endif
						<p style="font-size: 10px;">{{ $transaction->items[$index]->merchant_name  }}</p>
						<p class="header mb-4">{{ $transaction->items[$index]->merchant_address }}</p>
						<p class="line3 mb-4 text-bold">OFFICIAL RECEIPT</p>
						<p class="text-left line3">TIN #: {{  $transaction->items[$index]->tin }}</p>
						<p class="text-left line3">Address: {{ $transaction->items[$index]->merchant_address }}</p>
						<p class="text-left line3">Tel. #: {{  $transaction->items[$index]->tel_no }}</p>
						<p class="text-left line3 mb-4">BIR #: --</p>

						<p class="text-left line3">OR_code #: {{  $transaction->items[$index]->or_code }}</p>
						<p class="text-left line3">Transaction #: {{  $transaction->transaction_code }}</p>
						<p class="text-left line3">Date: {{  Helper::date_format($transaction->items[$index]->created_at, 'Y/m/d h:i:s A')  }}</p>
						<p class="text-left line3 mb-4">Valid Until: {{  Helper::date_format($transaction->items[$index]->date_expiry, 'Y/m/d') }} 5:00 PM</p>
						
						<table width="100%">
							@foreach($transaction->items[$index]->items as $index => $value)
								<tr>
									<td class="text-bold">{{ $value->name }}</td>
									<td>P {{ Helper::money_format($value->price) }}</td>
								</tr>
							@endforeach

							<tr>
								<td colspan="2" style="border-bottom:solid .5px black; padding-top: 10px;">
								</td>
							</tr>
							<tr>
								<td colspan="2" style="border-bottom:solid .5px black; padding-top: -1px;">
								</td>
							</tr>

							<tr>
								<td style="padding-top: 20px;" class="text-bold">TOTAL</td>
								<td style="padding-top: 20px;" class="text-bold">P {{  Helper::money_format($transaction->items[$index]->amount)  }}</td>
							</tr>

							<tr>
								<td style="padding-top: 10px;">VATable Sales</td>
								<td style="padding-top: 10px;">P {{ Helper::money_format($transaction->vat_sales) }}</td>
							</tr>
							<tr>
								<td>VAT Amount</td>
								<td>P {{ Helper::money_format($transaction->vat_amount) }}<</td>
							</tr>
							<tr>
								<td>VAT Excempt Sales</td>
								<td>P 0.00</td>
							</tr>
							<tr>
								<td>Zero-Rates Sales</td>
								<td>P 0.00</td>
							</tr>

							<tr>
								<td style="padding-top: 20px;" class="text-bold">Buyer Details:</td>
							</tr>
							<tr>
								<td style="padding-top: 5px;">Customer Name: ________________________</td>
							</tr>
							<tr>
								<td>Address: ______________________________</td>
							</tr>
							<tr>
								<td>TIN #: ___________________________________</td>
							</tr>

							<tr>
								<td align="center" colspan="2" style="padding-top: 15px;">
									<img src="data:image/png;base64,,{{DNS2D::getBarcodePNG( $transaction->transaction_code, "QRCODE",4.5,4.5)}}" alt="barcode">
								</td>
							</tr>
							<tr>
								<td align="center" colspan="2">{{  $transaction->transaction_code }}
								</td>
							</tr>
						</table>
					@endif
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>