<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <style>
        * {
            font-family: Arial, Helvetica, sans-serif;
        }
        .gray {
            color: #909497;
        }

        .text-center {
            text-align: center;
        }

        .text-right {
            text-align: right;
        }

        .small {
            font-size: 4px;
        }

        .big {
            font-size: 8px;
        }

        .qr {
            width: 80px;
            height: 80px;
        }

        .mb-0 {
            margin-bottom: 0;
        }

        .mt-0 {
            margin-top: 0;
        }

        p {
            font-size: 6px;
            margin: 5px 0;
        }

        .table-container {
            background: #EAF2F8;
            padding: 10px 15px;
        }

        .table-container table td {
            padding: .1rem;
            font-size: 6px;
        }

        h3 {
            font-size: 12px;
        }

        hr {
            border-top: 1px solid #BDC3C7;
        }

        table {
            width: 100%;
        }

        .border-top {
            border-top: 1px solid #BDC3C7;
        }

        .underline {
            text-decoration: underline;
        }

        label {
            font-size: 6px;
        }

        input {
            vertical-align: middle;
            position: relative;
            bottom: 1px;
            width: 10px;
            height: 10px;
        }

        .container {
            width: 50%;
        }
    </style>
</head>
<body>
    @foreach($series as $index => $value)
    <div class="container" style="page-break-after: always;">
        <table>
            <tr>
                <td>
                    <img class="qr" src="https://i.pinimg.com/originals/60/c1/4a/60c14a43fb4745795b3b358868517e79.png" alt="">
                </td>
                <td class="text-right">
                    <p>No.: <b><span class="big">{{ $value->series }}</span></b></p>
                    <p>Date: </p>
                    <p>Reference No: </p>
                    <p>Merchant No: {{ strtoupper($value->merchant_code) }}</p>
                    <p>Teller No: </p>
                </td>
            </tr>
        </table>
    
        <h3 class="mb-0">{{ strtoupper($value->merchant->merchant_name) }}</h3>
        <p>{{ strtoupper($value->merchant->address) }}</p>
        <p>VAT Reg. TIN {{ strtoupper($value->merchant->tin) }}</p>
    
        <p class="small gray">IN PAYMENT OF THE FOLLOWING</p>
        <div class="table-container">
            <table>
                <tr>
                    <td><span class="item"></span></td>
                    <td class="text-right"><span class="item"></span></td>
                </tr>
                <tr>
                    <td><span class="item"></span></td>
                    <td class="text-right"><span class="item"></span></td>
                </tr>
                <tr>
                    <td><span class="item"></span></td>
                    <td class="text-right"><span class="item"></span></td>
                </tr>
                <tr>
                    <td><span class="item"></span></td>
                    <td class="text-right"><span class="item"></span></td>
                </tr>
                <tr>
                    <td><span class="item"></span></td>
                    <td class="text-right"><span class="item"></span></td>
                </tr>
                <tr>
                    <td><span class="item">VATABLE SALES</span></td>
                    <td class="text-right"><span class="item">0</span></td>
                </tr>
                <tr>
                    <td><span class="item">VAT AMOUNT</span></td>
                    <td class="text-right"><span class="item">0</span></td>
                </tr>
                <tr>
                    <td><span class="item"><b>TOTAL</b></span></td>
                    <td class="text-right"><span class="item">0</span></td>
                </tr>
            </table>
        </div>
    
        <br>

        <div class="table-container">
            <table>
                <tr>
                    <td><span class="item">VATABLE</span></td>
                    <td class="text-right"><span class="item">0</span></td>
                </tr>
                <tr>
                    <td><span class="item">EXEMPT</span></td>
                    <td class="text-right"><span class="item">0</span></td>
                </tr>
                <tr>
                    <td><span class="item">ZERO-R</span></td>
                    <td class="text-right"><span class="item">0</span></td>
                </tr>
            </table>
        </div>
    
        <br>
        
        <div class="table-container">
            <p class="gray">AMOUNT IN WORDS</p>
            <p class="underline">0</p>
        </div>
    
        <br>
        
        <p class="mb-0"><b>RECEIVED FROM:</b></p>
        <h3 class="mb-0 mt-0"></h3>
        <p></p>
        <p></p>
    
        <br>
    
        
        <p class="gray">PAYMENT METHOD</p>
        <table>
            <tr>
                <td class="text-center">
                    <p class="checkboxes"><input type="checkbox"><span>CASH</span></p>
                </td>
                <td class="text-center">
                    <p class="checkboxes"><input type="checkbox"><span>CHECK</span></p>
                </td>
                <td class="text-center">
                    <p class="checkboxes"><input type="checkbox"><span>ON US</span></p>
                </td>
            </tr>
            <tr>
                <td class="text-center">
                    <p class="checkboxes"><input type="checkbox"><span>DEBIT</span></p>
                </td>
                <td class="text-center">
                    <p class="checkboxes"><input type="checkbox"><span>CREDIT</span></p>
                </td>
                <td class="text-center">
                    <p class="checkboxes"><input type="checkbox"><span>WALLET</span></p>
                </td>
            </tr>
        </table>
    
        <br>
        
        <p class="text-center gray border-top">CASHIER / AUTHORIZED SIGNATURED</p>
        
        <br>
    
        <p class="text-center small">{{ count($series) }} BKLTS (50x{{ $transaction->eor_booklet_qty}}) {{ $series[0]->series }} - {{ $series[count($series) - 1]->series }}</p>
        <p class="text-center small">BIR AUTHORITY TO PRINT NO {{ $transaction->atp_no }}</p>
        <p class="text-center small">DATE ISSUED {{ Carbon::now()->format('m-d-Y') }}; {{ Carbon::now()->addYears(5)->format('m-d-Y') }}</p>
        <p class="text-center small">(ASB PRINTING) - RONALDO B. BASSIG</p>
        <p class="text-center small">PRINTER'S ACCREDITATION NO. {{ $transaction->printer_accreditation }}</p>
        {{-- <p class="text-center small">DATE OF ACCREDITATION: 04/03/2020</p>
        <p class="text-center small">3234 NATIONAL RD, MUNTINLUPA CITY,</p>
        <p class="text-center small">VAT REG TIN. 152-234-435-000</p> --}}
    
        <br>
    
        <p class="text-center small gray">*THIS ELECTRONIC OFFICIAL RECEIPT SHALL BE VALID FOR FIVE(5) YEARS FROM THE DATE OF ATP*</p>
    </div>
    @endforeach
</body>
</html>