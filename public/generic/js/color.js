var settings_block = '<div class="color_pick_wrap">\
		<div id="color_setting" class="color_settings">\
			<h2>Pick Your Color</h2>\
			<div>\
				<span class="light_red" title="Light Red" data-color="#F44234" />\
				<span class="green" title="Green" data-color="#43923E" />\
				<span class="light_purple" title="Light Purple" data-color="#B73DBD" />\
				<span class="orange" title="Orange" data-color="#E67E22" />\
				<span class="dark_purple" title="Dark Purple" data-color="#5E345E" />\
				<span class="light_blue" title="Light Blue" data-color="#3183d7" />\
			</div>\
			<a id="color_open_close"><i class="fa fa-cog fa-spin fa-fw"></i></a>\
		</div>\
	</div>';

//Init color buttons
function initColor() {
    $('.color_pick_wrap div span').on('click',function() {	
        var cls = $(this).attr('class');
        var color = $(this).data("color");
        //CSS
        $("link.colors").attr('href', 'resources/css/'+cls+'.css');
    });
}

//Init open/close button	
function initClose() {
    $('#color_open_close').on('click',function(e) {
        $('body').toggleClass('opened-settings');
        e.preventDefault();	
    });
}


$(document).ready(function() {
    initColor();	
    initClose();
});